#
# This distribution was designed to facilitate the refurbishing of
# computers given to humanitarian organizations, especially Emmaüs
# communities, where the name comes from, and to promote the discovery
# of GNU/Linux by beginners, but also to extend the life of the equipment
# and to reduce waste caused by over- consumption of raw materials.
#
# It was validate Debian 12 XFCE and LXQt
#
# Home web site : https://emmabuntus.org/
#
# Copyright (C) 2010-25 Collectif Emmabuntüs (contact@emmabuntus.org).
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

---------------------------------------------------------------------------------------------------------------------------------
ChangeLog.txt
---------------------------------------------------------------------------------------------------------------------------------

Changes of the 1.04 version compared to the 1.03 of 2025/02/18:
- Based on the Debian 12.9
- Update: Firefox 128.7.0esr, Thunderbird 128.7.0esr, deb-get 0.4.4-1, Ventoy 1.1.00, Warpinator 1.8.8, Debian beginners handbook 12.9


Changes of the 1.03 version compared to the 1.02 of 2024/11/23:
- Based on the Debian 12.8
- Added accessibility feature to write with 6 points via keyboard like perkins method: 40
- Fixed ISO usage with Etcher Bootable USB Creator and USB Creator : 48
- Update: Firefox 128.4.0esr, Thunderbird 128.4.3esr, Warpinator 1.8.6, Debian beginners handbook 12.7, deb-get 0.4.3-1, TurboPrint 2.57-1


Changes of the 1.02 version compared to the 1.01 of 2024/07/27:
- Based on the Debian 12.6
- Replacement of Syslinux by GRUB-PC: 39
- Added accessibility functions in Emmabuntüs: 40
- Added manuals for shortcuts, first steps with Orca and for accessibility functions: 45, 47
- Replacement of qpdfview by evince for accessibility: 40
- Replacement of Galculator by gnome-calculator for accessibility: 40
- Removed Tuxguitar and DarkTable in 64 bits and SuperTux2 to reduce ISO size below 4 GB : 46
- Update: Firefox 115.13.0esr, Thunderbird 115.13.0,  Warpinator 1.8.3, FreeTube 0.21.1, Ventoy 1.0.99, CTparental  5.1.15-1m, TurboPrint 2.56-1, Ancestris 12-20240609, deb-get 0.4.1-1, Boot-Repair 4ppa2075, libdvdcss2 1.4.3-dmo2


Changes of the 1.01 version compared to the 1.00 of 2023/12/30:
- Based on the Debian 12.4
- Fixed live repository problem: 31
- Included FileZilla 32-bit version
- Replacement of VeraCrypt by ZuluCrypt: 33
- Updated Emmabuntüs Debian Edition 5 installation tutorial in English
- Fixed activation of automatic donation of Lilo drops: 32
- Update: Firefox 115.6.0esr, Thunderbird 115.6.0, Warpinator 1.6.4, FreeTube 0.19.1, Ventoy 1.0.96, Deb-get 0.4.0, Ancestris 11-20231109


Changes of the 1.00 version compared to the RC 1 of 2023/08/26:
- Based on the Debian 12.1
- Added a script to launch OS-prober: ticket 25
- Improved startup time in live mode and the installation of Calamares under WMware: ticket 27
- Replaced zram-tools by systemd-zram-generator: ticket 28
- Removed PlayOnLinux and Wine to save space following the activation of a new ISO compression mode: ticket 27
- Disable info message on non-free-firmware repository: ticket 24
- Updated from Scratch 3.29.1 to 64-bit version: ticket 29
- Updated Emmabuntüs Debian Edition 5 installation tutorial in French
- Update: Firefox 102.14.0esr, Thunderbird 102.14.0, Ventoy 1.0.95


Changes of the RC 1  version compared to the Alpha 1 of 2023/06/18:
- Based on the Debian 12.0
- Added additional languages: ticket 21
- Added Jami from the Debian repository: ticket 22
- Added wallpapers based on the Emerald theme of Debian 12: ticket 23
- Improved display of GRUB in Secure Boot
- Fixed installation of RadioTray-NG in 32 bits
- Updated The Debian Bookworm 12.0 beginner’s handbook
- Updated Calamares slideshow
- Update: Firefox 102.12.0esr, Thunderbird 102.12.0, Ventoy 1.0.95, Pulseaudio Equalizer Ladspa 2022.07, Deb-get 0.3.9-1, CTparental 5.1.10-1


Changes of the Alpha 1 version compared to the Alpha 0 of 2023/04/06:
- Based on the Debian 12 RC 1
- Added Deb-get and Deborah utilities : ticket 14
- Added parental control CTparental: ticket 16
- Fixed apt cache creation
- Used gnome-packagekit from the repository
- GRUB improvement to 1024x768 and message about installing Calamares
- Filezilla in 64-bit version only present on Debian 12
- Removed Childsplay, Omnitux-ligth, Wammu, System-config-samba due to deprecated Python 2: ticket 15
- Removed Samba, bind9
- Disabled HTTPS in normal mode in Firefox
- Disabled insecure default umask (World Readable Files)
- Update: Firefox 102.9.0esr, Thunderbird 102.9.0, Ventoy 1.0.90, Warpinator 1.4.5, The Debian Bullseye 11.4 beginner’s handbook, TurboPrint 2.54-1, Boot-Repair 4ppa200


Changes of the Alpha 0 version compared to the EmmaDE4 1.01 of 2022/07/17:
- Based on the Debian 12 development release
- Added apt-cacher-ng config
- Added 64-bit UEFI boot with a 32-bit ISO and vice versa
- Added installation of boot in UEFI 64 bits with a 32 bits ISO and vice versa
- Added MenTest86+ launch in UEFI mode
- Fixed activation not launching keyboard selection in SysLinux and GRUB
- Fixed Zenity crash when installing new language packs
- Fixed language change script due to Calamares
- Updated Debian Beginner's Notebooks 11.3
- Updated Calamares slideshow by adding a slide on the reuse campaign
- Updated a new version of DWService agent and implemented a package
- Updated gSpeech 0.11.0
- Removed MultiSystem which has become obsolete: ticket 5
- Removed Disconnect, HTTPS Everywhere extensions and use of Firefox functions: ticket 6
- Removed MintLocale become not compatible with Debian 12
- Removed Hexchat and Empathy: ticket 9
- Removed Soundconverter and Winff: ticket 10
- Removed Imagination and Hugin: ticket 11
- Removed Stellarium to save space on the ISO: ticket 11
- Removed deprecated ndiswrapper and ndisgtk
- Removed Dominos which no longer works
- Used default Liberation Sans font in LXQt: ticket 6
- Used gnome-packagekit version Bullseye (TODO)
- Activation of boot-repair and os-uninstaller only live
- Update: Firefox ESR 91.11.1, Thunderbird 91.10.1, Ventoy 1.0.78, Warpinator 1.2.9, Radiotray-NG 0.2.8, TurboPrint 2.53-1, boot-repair 4ppa200, Veracrypt 1.25.9, MintStick 1.4 .9, Beginner's Notebooks Debian 11.3

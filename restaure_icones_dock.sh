#!/bin/bash


#find config/includes.chroot/etc/skel/.config/cairo-dock-language -type f -a -name '*.desktop' -exec grep -l steam {} \;
#find config/includes.chroot/etc/skel/.config/cairo-dock-language -type f -a -name '*.desktop' -exec grep -l GOG.com {} \;
#find config/includes.chroot/etc/skel/.config/cairo-dock-language -type f -a -name '*.desktop' -exec grep -l play.it {} \;
#find config/includes.chroot/etc/skel/.config/cairo-dock-language -type f -a -name '*.desktop' -exec grep -l skype {} \;
#find config/includes.chroot/etc/skel/.config/cairo-dock-language -type f -a -name '*.desktop' -exec grep -l supertuxkart {} \;
#find config/includes.chroot/etc/skel/.config/cairo-dock-language -type f -a -name '*.desktop' -exec grep -l hedgewars {} \;
#find config/includes.chroot/etc/skel/.config/cairo-dock-language -type f -a -name '*.desktop' -exec grep -l belooted {} \;

# Config

git checkout --  config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-fr/current_theme/cairo-dock-simple.conf
git checkout --  config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-fr-simple/current_theme/cairo-dock-simple.conf
git checkout --  config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-fr-kids/current_theme/cairo-dock-simple.conf
git checkout --  config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-en/current_theme/cairo-dock-simple.conf
git checkout --  config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-en-simple/current_theme/cairo-dock-simple.conf
git checkout --  config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-en-kids/current_theme/cairo-dock-simple.conf
git checkout --  config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-es/current_theme/cairo-dock-simple.conf
git checkout --  config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-es-simple/current_theme/cairo-dock-simple.conf
git checkout --  config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-es-kids/current_theme/cairo-dock-simple.conf
git checkout --  config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-it/current_theme/cairo-dock-simple.conf
git checkout --  config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-it-simple/current_theme/cairo-dock-simple.conf
git checkout --  config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-it-kids/current_theme/cairo-dock-simple.conf
git checkout --  config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-pt/current_theme/cairo-dock-simple.conf
git checkout --  config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-pt-simple/current_theme/cairo-dock-simple.conf
git checkout --  config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-pt-kids/current_theme/cairo-dock-simple.conf
git checkout --  config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-de/current_theme/cairo-dock-simple.conf
git checkout --  config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-de-simple/current_theme/cairo-dock-simple.conf
git checkout --  config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-de-kids/current_theme/cairo-dock-simple.conf
git checkout --  config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-ar/current_theme/cairo-dock-simple.conf
git checkout --  config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-ar-simple/current_theme/cairo-dock-simple.conf
git checkout --  config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-ar-kids/current_theme/cairo-dock-simple.conf


git checkout --  config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-fr/current_theme/cairo-dock.conf
git checkout --  config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-fr-simple/current_theme/cairo-dock.conf
git checkout --  config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-fr-kids/current_theme/cairo-dock.conf
git checkout --  config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-en/current_theme/cairo-dock.conf
git checkout --  config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-en-simple/current_theme/cairo-dock.conf
git checkout --  config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-en-kids/current_theme/cairo-dock.conf
git checkout --  config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-es/current_theme/cairo-dock.conf
git checkout --  config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-es-simple/current_theme/cairo-dock.conf
git checkout --  config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-es-kids/current_theme/cairo-dock.conf
git checkout --  config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-it/current_theme/cairo-dock.conf
git checkout --  config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-it-simple/current_theme/cairo-dock.conf
git checkout --  config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-it-kids/current_theme/cairo-dock.conf
git checkout --  config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-pt/current_theme/cairo-dock.conf
git checkout --  config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-pt-simple/current_theme/cairo-dock.conf
git checkout --  config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-pt-kids/current_theme/cairo-dock.conf
git checkout --  config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-de/current_theme/cairo-dock.conf
git checkout --  config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-de-simple/current_theme/cairo-dock.conf
git checkout --  config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-de-kids/current_theme/cairo-dock.conf
git checkout --  config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-ar/current_theme/cairo-dock.conf
git checkout --  config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-ar-simple/current_theme/cairo-dock.conf
git checkout --  config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-ar-kids/current_theme/cairo-dock.conf

# Steam

git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-fr/current_theme/launchers/01steam.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-es-simple/current_theme/launchers/01steam.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-ar/current_theme/launchers/01steam.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-de/current_theme/launchers/01steam.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-ar-simple/current_theme/launchers/01steam.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-en-simple/current_theme/launchers/01steam.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-fr-simple/current_theme/launchers/01steam.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-de-simple/current_theme/launchers/01steam.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-es/current_theme/launchers/01steam.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-pt/current_theme/launchers/01steam.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-en/current_theme/launchers/01steam.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-it-simple/current_theme/launchers/01steam.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-pt-simple/current_theme/launchers/01steam.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-it/current_theme/launchers/01steam.desktop

# GOG.com

git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-fr/current_theme/launchers/38launcher.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-de/current_theme/launchers/02launcher.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-en-simple/current_theme/launchers/02launcher.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-fr-simple/current_theme/launchers/30launcher.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-de-simple/current_theme/launchers/02launcher.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-pt/current_theme/launchers/02launcher.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-en/current_theme/launchers/08launcher.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-pt-simple/current_theme/launchers/02launcher.desktop

# play.it

git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-fr/current_theme/launchers/49launcher.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-fr-simple/current_theme/launchers/38launcher.desktop

# Skype

git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-fr/current_theme/launchers/34launcher.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-es-simple/current_theme/launchers/34launcher.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-ar/current_theme/launchers/34launcher.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-de/current_theme/launchers/34launcher.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-ar-simple/current_theme/launchers/34launcher.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-en-simple/current_theme/launchers/34launcher.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-fr-simple/current_theme/launchers/34launcher.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-de-simple/current_theme/launchers/34launcher.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-es/current_theme/launchers/34launcher.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-pt/current_theme/launchers/34launcher.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-en/current_theme/launchers/34launcher.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-it-simple/current_theme/launchers/34launcher.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-pt-simple/current_theme/launchers/34launcher.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-it/current_theme/launchers/34launcher.desktop

# supertuxkart

git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-fr/current_theme/launchers/01supertuxkart.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-es-simple/current_theme/launchers/01supertuxkart.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-ar-kids/current_theme/launchers/01supertuxkart.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-ar/current_theme/launchers/01supertuxkart.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-de/current_theme/launchers/01supertuxkart.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-ar-simple/current_theme/launchers/01supertuxkart.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-en-simple/current_theme/launchers/01supertuxkart.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-de-kids/current_theme/launchers/01supertuxkart.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-fr-simple/current_theme/launchers/01supertuxkart.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-pt-kids/current_theme/launchers/01supertuxkart.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-de-simple/current_theme/launchers/01supertuxkart.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-es/current_theme/launchers/01supertuxkart.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-es-kids/current_theme/launchers/01supertuxkart.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-pt/current_theme/launchers/01supertuxkart.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-en/current_theme/launchers/01supertuxkart.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-fr-kids/current_theme/launchers/01supertuxkart.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-it-simple/current_theme/launchers/01supertuxkart.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-en-kids/current_theme/launchers/01supertuxkart.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-it-kids/current_theme/launchers/01supertuxkart.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-pt-simple/current_theme/launchers/01supertuxkart.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-it/current_theme/launchers/01supertuxkart.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-da/current_theme/launchers/01supertuxkart.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-da-simple/current_theme/launchers/01supertuxkart.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-da-kids/current_theme/launchers/01supertuxkart.desktop


# hedgewars

git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-fr/current_theme/launchers/01hedgewars.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-es-simple/current_theme/launchers/01hedgewars.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-ar-kids/current_theme/launchers/01hedgewars.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-ar/current_theme/launchers/01hedgewars.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-de/current_theme/launchers/01hedgewars.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-ar-simple/current_theme/launchers/01hedgewars.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-en-simple/current_theme/launchers/01hedgewars.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-de-kids/current_theme/launchers/01hedgewars.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-fr-simple/current_theme/launchers/01hedgewars.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-pt-kids/current_theme/launchers/01hedgewars.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-de-simple/current_theme/launchers/01hedgewars.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-es/current_theme/launchers/01hedgewars.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-es-kids/current_theme/launchers/01hedgewars.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-pt/current_theme/launchers/01hedgewars.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-en/current_theme/launchers/01hedgewars.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-fr-kids/current_theme/launchers/01hedgewars.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-it-simple/current_theme/launchers/01hedgewars.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-en-kids/current_theme/launchers/01hedgewars.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-it-kids/current_theme/launchers/01hedgewars.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-pt-simple/current_theme/launchers/01hedgewars.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-it/current_theme/launchers/01hedgewars.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-da/current_theme/launchers/01hedgewars.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-da-simple/current_theme/launchers/01hedgewars.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-da-kids/current_theme/launchers/01hedgewars.desktop

# belooted

git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-fr/current_theme/launchers/01belooted.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-es-simple/current_theme/launchers/01belooted.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-ar/current_theme/launchers/01belooted.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-de/current_theme/launchers/01belooted.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-ar-simple/current_theme/launchers/01belooted.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-en-simple/current_theme/launchers/01belooted.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-fr-simple/current_theme/launchers/01belooted.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-de-simple/current_theme/launchers/01belooted.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-es/current_theme/launchers/01belooted.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-pt/current_theme/launchers/01belooted.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-en/current_theme/launchers/01belooted.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-it-simple/current_theme/launchers/01belooted.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-pt-simple/current_theme/launchers/01belooted.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-it/current_theme/launchers/01belooted.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-da/current_theme/launchers/01belooted.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-da-simple/current_theme/launchers/01belooted.desktop

# flatpak

git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-ar/current_theme/launchers/01flatpak.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-de/current_theme/launchers/01flatpak.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-en/current_theme/launchers/01flatpak.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-es/current_theme/launchers/01flatpak.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-fr/current_theme/launchers/01flatpak.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-it/current_theme/launchers/01flatpak.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-pt/current_theme/launchers/01flatpak.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-da/current_theme/launchers/01flatpak.desktop

# Darktable

git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-ar/current_theme/launchers/01darktable.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-de/current_theme/launchers/01darktable.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-en/current_theme/launchers/01darktable.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-es/current_theme/launchers/01darktable.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-fr/current_theme/launchers/01darktable.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-it/current_theme/launchers/01darktable.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-pt/current_theme/launchers/01darktable.desktop


# Paquets obsolètes
#find config/includes.chroot/etc/skel/.config/cairo-dock-language -type f -a -name '*.desktop' -exec grep -l freetuxtv {} \;

git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-ar/current_theme/launchers/01hexchat.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-de/current_theme/launchers/01hexchat.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-en/current_theme/launchers/01hexchat.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-es/current_theme/launchers/01hexchat.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-fr/current_theme/launchers/01hexchat.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-it/current_theme/launchers/01hexchat.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-pt/current_theme/launchers/01hexchat.desktop


git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-ar/current_theme/launchers/01evolution.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-de/current_theme/launchers/01evolution.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-en/current_theme/launchers/01evolution.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-es/current_theme/launchers/01evolution.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-fr/current_theme/launchers/01evolution.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-it/current_theme/launchers/01evolution.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-pt/current_theme/launchers/01evolution.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-da/current_theme/launchers/01evolution.desktop

git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-ar/current_theme/launchers/01kazam.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-de/current_theme/launchers/01kazam.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-en/current_theme/launchers/01kazam.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-es/current_theme/launchers/01kazam.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-fr/current_theme/launchers/01kazam.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-it/current_theme/launchers/01kazam.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-pt/current_theme/launchers/01kazam.desktop

git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-ar/current_theme/launchers/01shutter.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-de/current_theme/launchers/01shutter.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-en/current_theme/launchers/01shutter.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-es/current_theme/launchers/01shutter.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-fr/current_theme/launchers/01shutter.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-it/current_theme/launchers/01shutter.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-pt/current_theme/launchers/01shutter.desktop

git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-fr-kids/current_theme/launchers/01chromium.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-fr-simple/current_theme/launchers/01chromium.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-fr/current_theme/launchers/01chromium.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-it/current_theme/launchers/01chromium.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-en-kids/current_theme/launchers/01chromium.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-es/current_theme/launchers/01chromium.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-en/current_theme/launchers/01chromium.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-de-simple/current_theme/launchers/01chromium.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-es-kids/current_theme/launchers/01chromium.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-en-simple/current_theme/launchers/01chromium.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-de/current_theme/launchers/01chromium.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-es-simple/current_theme/launchers/01chromium.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-pt/current_theme/launchers/01chromium.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-ar/current_theme/launchers/01chromium.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-ar-simple/current_theme/launchers/01chromium.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-it-simple/current_theme/launchers/01chromium.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-it-kids/current_theme/launchers/01chromium.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-de-kids/current_theme/launchers/01chromium.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-pt-simple/current_theme/launchers/01chromium.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-pt-kids/current_theme/launchers/01chromium.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-ar-kids/current_theme/launchers/01chromium.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-da-kids/current_theme/launchers/01chromium.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-da-simple/current_theme/launchers/01chromium.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-da/current_theme/launchers/01chromium.desktop

git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-fr/current_theme/launchers/01org.gnome.Evolution.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-it/current_theme/launchers/01evolution.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-es/current_theme/launchers/01evolution.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-en/current_theme/launchers/01evolution.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-de/current_theme/launchers/01evolution.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-pt/current_theme/launchers/01evolution.desktop
git checkout -- config/includes.chroot/etc/skel/.config/cairo-dock-language/cairo-dock-ar/current_theme/launchers/01evolution.desktop

Emmabuntüs
==========

**Emmabuntüs** a été conçue par le [collectif Emmabuntüs](https://collectif.emmabuntus.org/) pour faciliter le reconditionnement des ordinateurs donnés aux associations humanitaires, en particulier les communautés **[Emmaüs](http://fr.wikipedia.org/wiki/Mouvement_Emma%C3%BCs)** (d'où son nom) et favoriser la découverte de [GNU](http://fr.wikipedia.org/wiki/Gnu)/[Linux](http://fr.wikipedia.org/wiki/Linux) par les débutants, mais aussi prolonger la durée de vie du matériel pour limiter le gaspillage entraîné par la surconsommation de matières premières. Cette Millième distribution Linux se veut épurée, accessible, équitable, voir les articles publiés dans [Next INpact en septembre 2020](https://next.ink/5743/emmabuntus-distribution-linux-specialisee-dans-materiel-reconditionne-et-sa-cle-magique/) et [Linux Format en mars 2022](https://emmabuntus.org/linux-format-287-fr/).

Emmabuntüs contient tous les logiciels nécessaires à une prise en main rapide et agréable de GNU/Linux.

Afin de simplifier la navigation, un [Dock](http://fr.wikipedia.org/wiki/Dock_(Informatique)) a été ajouté, il donne accès à différents logiciels de la distribution en fonction des 3 niveaux d'utilisateurs : enfant, débutant, expert.


----------

Caractéristiques de ces distributions
----------

- Distributions basées sur des variantes de [Debian](http://fr.wikipedia.org/wiki/Debian) 8 Jessie, 9 Stretch, 10 Buster, 11 Bullseye et maintenant **12 Bookworm**
- Mode d'installation rapide grâce à des scripts d'automatisation (preseed)
- Mode Live sur DVD, Clé USB avec [Ventoy](https://www.ventoy.net/)
- Installation autonome sans Internet, tout est dans la distribution
- Installation ou pas des logiciels non libres comme les fontes Microsoft
- Plus 60 applications supplémentaires
- Un dock permettant l'accès à l'ensemble des applications
- 7 langues (Fr, En, Es, Pt, It, De, Da) prises en charge dans les distributions

----------

Sites
----------

- [Site officiel](https://emmabuntus.org/)
- [Téléchargement](http://download.emmabuntus.org)
- [Tutoriels](http://tutos.emmabuntus.org)
- [Avis techniques sur DistroWatch](http://distrowatch.com/emmabuntus)
- [Wikipédia](http://fr.wikipedia.org/wiki/Emmabunt%C3%BCs)
- [Revues de presse](http://newspapers.emmabuntus.org/)
- [Forum](http://forum.emmabuntus.org/)
- [Blog](http://blog.emmabuntus.org/)
- [Framapiaf](https://framapiaf.org/@Emmabuntus), [Twitter](https://twitter.com/Emmabuntus), [Facebook](http://www.facebook.com/Emmabuntus)


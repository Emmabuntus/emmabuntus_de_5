#! /bin/bash
chemin="$(cd "$(dirname "$0")";pwd)/$(basename "$0")";
dossier="$(dirname "$chemin")"
cd "${dossier}"



###Pour exporter la librairie de gettext.
set -a
source gettext.sh
set +a
export TEXTDOMAIN=emmabuntus-accessibility
export TEXTDOMAINDIR="${dossier}/locale"
. gettext.sh

#_________________________________________________
#░▒▓███████████████████ADMIN███████████████████▓▒░

#si dans /usr/local/share/emmabuntus
if [[ ! "$SUDO_USER" && "${dossier}" == "/usr/local/share/emmabuntus" ]]; then
#gksudo -k "$chemin" #supprimé plus dispo ...
exit 0
fi

mkdir /tmp/emmabuntus-accessibility 2>/dev/null


#liste des langues
#VARLANGUES="en fr es de it pt da"
#cat /usr/share/language-selector/data/languagelist
selang="$(cat "${dossier}/lang_list.txt")"
VARLANGUES="$(echo -e "$selang" | awk -F"|" '{print $2}' | xargs)"

##Voir fichier ==> .../lang_list.txt
##see fille ==> .../lang_list.txt

#liste des fichiers a prendre en compte pour traduction des .po
list_po="
../emmabuntus-accessibility/emmabuntus-accessibility/usr/bin/emmabuntus_accessibility_audio.sh
../emmabuntus-accessibility/emmabuntus-accessibility/usr/bin/emmabuntus_accessibility.sh
../emmabuntus-accessibility/emmabuntus-accessibility/usr/bin/emmabuntus_accessibility_window_audio.sh
../emmabuntus-accessibility/emmabuntus-accessibility/usr/bin/emmabuntus_accessibility_window.sh
../emmabuntus-accessibility/emmabuntus-accessibility/usr/bin/emmabuntus_accessibility_internet.sh
../emmabuntus-accessibility/emmabuntus-accessibility/usr/bin/emmabuntus_accessibility_help.sh
../emmabuntus-accessibility/emmabuntus-accessibility/usr/bin/emmabuntus_accessibility_ebooks.sh
../emmabuntus-accessibility/emmabuntus-accessibility/usr/bin/emmabuntus_accessibility_update.sh
../emmabuntus-accessibility/emmabuntus-accessibility/usr/bin/emmabuntus_accessibility_capture_picture.sh
../emmabuntus-accessibility/emmabuntus-accessibility/usr/bin/emmabuntus_accessibility_dwservice.sh
../emmabuntus-accessibility/emmabuntus-accessibility/usr/bin/emmabuntus_accessibility_state_keys.sh
../emmabuntus-accessibility/emmabuntus-accessibility/usr/bin/emmabuntus_accessibility_system_update.sh
../emmabuntus-accessibility/emmabuntus-accessibility/usr/bin/emmabuntus_accessibility_gnome_calculator.sh
../emmabuntus-accessibility/emmabuntus-accessibility/usr/bin/emmabuntus_accessibility_ventoy.sh
../emmabuntus-accessibility/emmabuntus-accessibility/usr/bin/emmabuntus_accessibility_daisy_player.sh
../emmabuntus-accessibility-kiwix/emmabuntus-accessibility-kiwix/usr/bin/emmabuntus_accessibility_kiwix.sh
../elograf/elograf-emmabuntus-data/usr/bin/emmabuntus_elograf_on_off.sh
../elograf/elograf-emmabuntus-data/usr/bin/emmabuntus_elograf_window.sh
../elograf/elograf-emmabuntus-data/usr/bin/emmabuntus_elograf_window_audio.sh
"
export list_po

#../emmabuntus-accessibility/emmabuntus-accessibility/usr/bin/emmabuntus_accessibility_help.sh

#Attention pour que gettext ne se plante pas dans les traductions remplacer ' et \' et '\'' par son equivalent en octal \047
#pour les simple quote: ' ==> \\047
#et pour les double quote: " ==> \\042

#$(eval_gettext "Temps d\047execution: \$MIN Minutes et \$SEC Secondes")
#$(eval_gettext "")
#$(eval_gettext "Choisir l\047option \$var")
#$(eval_gettext '')
#
#syntaxe a adopter dans les fichiers en gtkdialog
#"'$(eval_gettext '')'"
#"'$(eval_gettext '')'"
#"'$(eval_gettext "Editer \$HOME/.md5_live_perso.txt")'"
#"'$(eval_gettext "Chemin de l'iso:")'"
#export LC_ALL=fr_FR.UTF-8
#export LC_ALL="en_US.UTF-8"


#Menage, virer les sauvegardes!
find "${dossier}" | egrep "~$" | perl -n -e 'system("rm $_");'


function internationalisation()
{
#Mettre en place internationalisation
cd "${dossier}"
#Créer les .po
for L in $VARLANGUES
do
if [ ! -e "${dossier}/locale/${L}/LC_MESSAGES/emmabuntus-accessibility.${L}.po" ]; then
echo -e "Créer: ${L}"
mkdir -p "${dossier}/locale/${L}/LC_MESSAGES"
xgettext --from-code=UTF-8 \
--no-location \
-L shell --no-wrap \
-o "${dossier}/locale/${L}/LC_MESSAGES/emmabuntus-accessibility.${L}.po" $(printf "$list_po" | xargs)
#Modifier CHARSET
sed -i "s%charset=CHARSET%charset=UTF-8%" "${dossier}"/locale/${L}/LC_MESSAGES/emmabuntus-accessibility.${L}.po

#Créer les binaires .mo
msgfmt -o "${dossier}"/locale/${L}/LC_MESSAGES/emmabuntus-accessibility.mo \
"${dossier}"/locale/${L}/LC_MESSAGES/emmabuntus-accessibility.${L}.po
fi
done
}
internationalisation


#Appliquer manuellement apres chaque modification du script!
function internationalisation_update()
{

#mise à jour. ,regenerer les .po avec en plus les nouveaux champs.
for L in $VARLANGUES
do
xgettext -join-existing \
--no-location \
--from-code=UTF-8 \
--package-name=emmabuntus-accessibility \
-L shell \
--no-wrap \
-o "${dossier}"/locale/${L}/LC_MESSAGES/emmabuntus-accessibility.${L}.po $(printf "$list_po" | xargs)
done

#Recréer les .mo
for L in $VARLANGUES
do
msgfmt \
-o "${dossier}"/locale/${L}/LC_MESSAGES/emmabuntus-accessibility.mo \
"${dossier}"/locale/${L}/LC_MESSAGES/emmabuntus-accessibility.${L}.po

#Modifier entête
translator=$(echo -e "$selang" | grep \|${L}\| | awk -F"|" '{print $4}')
email=$(echo -e "$selang" | grep \|${L}\| | awk -F"|" '{print $5}')

if [[ ${email} != "" ]] ; then sed -i "s%FULL NAME <EMAIL@ADDRESS>%FULL NAME <${email}>%" "${dossier}"/locale/${L}/LC_MESSAGES/emmabuntus-accessibility.${L}.po ; fi
if [[ ${translator} != "" ]] ; then sed -i "s%Last-Translator: FULL NAME%Last-Translator: ${translator}%" "${dossier}"/locale/${L}/LC_MESSAGES/emmabuntus-accessibility.${L}.po ; fi


done

#Retraduire les champs msgstr non traduits avec poedit ou gtranslator
#Editeurs poedit, gtranslator, ...

if [ ! "$(which poedit)" ]; then
sudo apt-get install -y poedit
fi

echo  'zenity \
--title="Live CD" \
--text="Veuillez sélectionner le fichier .po à traduire
dans la liste ci-dessous.
ATTENTION!
Veuillez respecter la longueur des phrases
ainsi que les saut de lignes sous peine
de provoquer des bogs d'\''affichage
ne traduisez pas les variables commençant par $
" \
--width=360 \
--height=400 \
--list \
--print-column="2" \
--radiolist \
--separator=" " \
--column="*" \
--column="Val" \' >/tmp/emmabuntus-accessibility/emmabuntus-accessibility-tr
echo -e "$(ls -A "${dossier}"/locale | tr " " "\n" | awk '{print "FALSE " $0 " \\"}')" >>/tmp/emmabuntus-accessibility/emmabuntus-accessibility-tr
RETOUR_TRANSLATE=$(. /tmp/emmabuntus-accessibility/emmabuntus-accessibility-tr)
if [ -e "${dossier}/locale/${RETOUR_TRANSLATE}/LC_MESSAGES/emmabuntus-accessibility.${RETOUR_TRANSLATE}.po" ]; then
poedit "${dossier}"/locale/${RETOUR_TRANSLATE}/LC_MESSAGES/emmabuntus-accessibility.${RETOUR_TRANSLATE}.po
msgfmt -o "${dossier}"/locale/${RETOUR_TRANSLATE}/LC_MESSAGES/emmabuntus-accessibility.mo \
"${dossier}"/locale/${RETOUR_TRANSLATE}/LC_MESSAGES/emmabuntus-accessibility.${RETOUR_TRANSLATE}.po
else
#Supprimer
rm -R /tmp/emmabuntus-accessibility
exit 0
fi

}
internationalisation_update
echo -e "\033[1;47;31m Attention, pensez a faire une copie en local du fichier:${dossier}/locale/*/LC_MESSAGES/emmabuntus-accessibility.*.po\nCar toute mise à jour n\047integrant pas ce fichier l\047ecrasera\!,\net vous perdrez votre travail... \033[0m"

#Supprimer
rm -R /tmp/emmabuntus-accessibility
rm ./locale/en/LC_MESSAGES/*.mo

    for L in $VARLANGUES
    do
    file="./locale/${L}/LC_MESSAGES/emmabuntus-accessibility.${L}.mo"
        if test -f ${file} ; then
        echo "Suppression fichier inutile : ${file}"
        rm ${file}
        fi
    done

exit 0


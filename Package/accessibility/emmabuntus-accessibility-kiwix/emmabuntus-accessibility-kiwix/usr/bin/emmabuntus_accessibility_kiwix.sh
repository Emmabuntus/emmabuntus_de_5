#! /bin/bash

# emmabuntus_kiwix_accessibility.sh --
#
#   This file permits to load zim on accessibility for the Emmabuntus Distrib.
#
#   Created on 2010-24 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 Xfce/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

###Pour exporter la librairie de gettext.
set -a
source gettext.sh
set +a
export TEXTDOMAIN=emmabuntus-accessibility
export TEXTDOMAINDIR="/usr/share/locale"
. gettext.sh

repertoire_emmabuntus=~/.config/emmabuntus
env_emmabuntus=${repertoire_emmabuntus}/env_emmabuntus
attente_lecture_message="1"

dir_zim="/usr/share/Documentation_emmabuntus/Documents/zim/"
kiwix_server_port=8080

message_load_server_ok="$(eval_gettext 'Kiwix server loaded OK')"
message_load_server_error="$(eval_gettext 'Kiwix server loading error!')"
message_file_not_found="$(eval_gettext 'No ZIM file in folder')"
message_one_file_found="$(eval_gettext 'ZIM file found in folder, Kiwix is open directly with this file')"
message_number_files_found="$(eval_gettext 'ZIM files found in folder, Kiwix is open in files selection window')"

. ${env_emmabuntus}

source /usr/bin/emmabuntus_accessibility_func.sh

langue_pico_accessibility_kiwix="$(eval_gettext '#langue_pico_accessibility_kiwix en-US')"
langue_pico_accessibility_kiwix=$(echo ${langue_pico_accessibility_kiwix} | cut -d " " -f 2)
if [[ ${langue_pico_accessibility_kiwix} == "en-US" && ${Language_pico} == "en-GB" ]] ; then langue_pico_accessibility_kiwix="en-GB" ; fi

escape_orca_read

# Si Orca d'activé attendre x secondes avant de lire le message d'information sur la modification du son
if [[ $(ps -A | grep orca) ]] ; then
    sleep ${attente_lecture_message}
fi

if [[ ${Library_zim} != "" ]] ; then
    if test -d ${Library_zim} ; then
        dir_zim=${Library_zim}
    fi
fi

WDIR=$(pwd)

# Teste si le dossier contient des fichiers ZIM

cd ${dir_zim}
file_ZIM=$(ls *.zim)
number_file_ZIM=`echo ${file_ZIM} | wc -w`
file_ZIM=`echo ${file_ZIM} | cut -d. -f1`

if [[ ${file_ZIM} != "" ]] ; then

    test_launch_server=$(ps -A | grep kiwix-serve)

    if [[ $test_launch_server != "" ]] ; then
        pkill kiwix-serve
    fi

    kiwix-serve -p ${kiwix_server_port} *.zim &

    test_launch_server=$(ps -A | grep kiwix-serve)

    if [[ ${test_launch_server} != "" ]] ; then

        message_audio_pico "${message_load_server_ok}" "${langue_pico_accessibility_kiwix}"

        if [ ${number_file_ZIM} -eq 1 ] ; then
            message_audio_pico "${number_file_ZIM} ${message_one_file_found}" "${langue_pico_accessibility_kiwix}"
            firefox http://127.0.0.1:${kiwix_server_port}/${file_ZIM}
        else
            message_audio_pico "${number_file_ZIM} ${message_number_files_found}" "${langue_pico_accessibility_kiwix}"
            firefox http://127.0.0.1:${kiwix_server_port}
        fi

    else

        message_audio_pico "${message_load_server_error}" "${langue_pico_accessibility_kiwix}"

    fi

else

    message_audio_pico "${message_file_not_found} ${dir_zim}" "${langue_pico_accessibility_kiwix}"

fi

cd ${WDIR}

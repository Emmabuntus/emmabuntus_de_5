#! /bin/bash

# emmabuntus_accessibility_dwservice.sh --
#
#   This file permit to use dwagent for DWService of accessibility for the Emmabuntus Distrib
#   More information on https://www.dwservice.net
#
#   Created on 2010-24 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 Xfce/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

###Pour exporter la librairie de gettext.
set -a
source gettext.sh
set +a
export TEXTDOMAIN=emmabuntus-accessibility
export TEXTDOMAINDIR="/usr/share/locale"
. gettext.sh

repertoire_emmabuntus=~/.config/emmabuntus
env_emmabuntus=${repertoire_emmabuntus}/env_emmabuntus
attente_lecture_message="1"

message_start_dwservice="$(eval_gettext 'Launching the D W Service utility, press key (2) then (Enter) to validate')"
message_start_capture_image="$(eval_gettext 'Then press the Alt Super Y shortcut to automatically send your computer connection code screenshot to our email. To exit the utility type Control C.')"

. ${env_emmabuntus}

source /usr/bin/emmabuntus_accessibility_func.sh

langue_pico_accessibility_dwservice="$(eval_gettext '#langue_pico_accessibility_dwservice en-US')"
langue_pico_accessibility_dwservice=$(echo ${langue_pico_accessibility_dwservice} | cut -d " " -f 2)
if [[ ${langue_pico_accessibility_dwservice} == "en-US" && ${Language_pico} == "en-GB" ]] ; then langue_pico_accessibility_dwservice="en-GB" ; fi

escape_orca_read

# Si Orca d'activé attendre x secondes avant de lire le message d'information sur la modification du son
if [[ $(ps -A | grep orca) ]] ; then
    sleep ${attente_lecture_message}
fi

message_audio_pico "${message_start_dwservice}" "${langue_pico_accessibility_dwservice}"

sleep 1

message_audio_pico "${message_start_capture_image}" "${langue_pico_accessibility_dwservice}"

xfce4-terminal --fullscreen -x /opt/DWService/dwagent.sh -console



#! /bin/bash

# emmabuntus_accessibility_system_update_exec.sh --
#
#   This file permit to update system of accessibility for the Emmabuntus Distrib
#
#   Created on 2010-24 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 Xfce/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

command=$1

if [[ ${command} == "update" ]] ; then
    xfce4-terminal -T "Update system" -x sudo apt update -q
    exit $?
elif [[ ${command} == "upgrade" ]] ; then
    xfce4-terminal -T "Upgrade system" -x sudo apt upgrade -qq -y
    exit $?
fi

exit 0




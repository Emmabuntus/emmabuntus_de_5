#! /bin/bash

# emmabuntus_accessibility_speech_dispatcher_update_exec.sh --
#
#   This file permits to test update speech-dispatcher for accessibilty
#   for the Emmabuntus Distrib.
#
#   Created on 2010-24 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 Xfce/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

# Commandes possibles : pico
command=${1}

restart_speech_dispatcher=0
mod_file=/etc/speech-dispatcher/speechd.conf

if test -f ${mod_file} ; then

    if [[ ${command} == "pico" ]] ; then

        if [[ $(cat ${mod_file} | grep "^#DefaultModule\ pico") ]] ; then
            sed s/"^#DefaultModule\ pico"/"DefaultModule pico"/ ${mod_file} > ${mod_file}.tmp
            sudo cp ${mod_file}.tmp ${mod_file}
            sudo rm ${mod_file}.tmp
            restart_speech_dispatcher=1
        fi

    else

        if [[ $(cat ${mod_file} | grep "^DefaultModule\ pico") ]] ; then
            sed s/"^DefaultModule\ pico"/"#DefaultModule pico"/ ${mod_file} > ${mod_file}.tmp
            sudo cp ${mod_file}.tmp ${mod_file}
            sudo rm ${mod_file}.tmp
            restart_speech_dispatcher=1
        fi

    fi

    if [[ ${restart_speech_dispatcher} == "1" ]] ; then

        if [[ $(ps -A | grep "speech-dispatch") ]] ; then
            pkill speech-dispatch
            sleep 1
        fi

        speech-dispatcher -d

    fi

fi


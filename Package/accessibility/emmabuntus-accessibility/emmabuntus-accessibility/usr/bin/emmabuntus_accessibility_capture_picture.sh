#! /bin/bash

# emmabuntus_accessibility_capture_picture.sh --
#
#   This file permit to capture a picture of accessibility for the Emmabuntus Distrib
#
#   Created on 2010-24 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 Xfce/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

###Pour exporter la librairie de gettext.
set -a
source gettext.sh
set +a
export TEXTDOMAIN=emmabuntus-accessibility
export TEXTDOMAINDIR="/usr/share/locale"
. gettext.sh

repertoire_emmabuntus=~/.config/emmabuntus
env_emmabuntus=${repertoire_emmabuntus}/env_emmabuntus
config_file=${repertoire_emmabuntus}/config_imgbb

emmabuntus_version=emmade5
file_extension=jpg
api_key="4b07b066e7f06eb32c6428e905f165e8"
expiration_delay="3600"
delay_capture="5"
attente_lecture_message="1"

DATE=`date +"%Y%m%d-%H%M%S"`

message_capture_image_delay="$(eval_gettext 'Screenshot in')"
message_capture_image_delay_time="$(eval_gettext 'seconds')"
message_capture_image="$(eval_gettext 'Screenshot now')"
message_upload_picture="$(eval_gettext 'Uploading screenshot to server')"
message_upload_picture_ok="$(eval_gettext 'Image sent to server successfully')"
message_upload_picture_ko="$(eval_gettext 'Uploading image to server failed')"

. ${env_emmabuntus}

source /usr/bin/emmabuntus_accessibility_func.sh

langue_pico_accessibility_capture_picture="$(eval_gettext '#langue_pico_accessibility_capture_picture en-US')"
langue_pico_accessibility_capture_picture=$(echo ${langue_pico_accessibility_capture_picture} | cut -d " " -f 2)
if [[ ${langue_pico_accessibility_capture_picture} == "en-US" && ${Language_pico} == "en-GB" ]] ; then langue_pico_accessibility_capture_picture="en-GB" ; fi

erase=$1

escape_orca_read

# Si Orca d'activé attendre x secondes avant de lire le message d'information sur la modification du son
if [[ $(ps -A | grep orca) ]] ; then
    sleep ${attente_lecture_message}
fi

# Test si l'effacement ne doit pas être activé
if [[ ${erase} == "no" ]] ; then

    expiration_delay=""

fi

# Determinations : BOIS, CPU et environnement

if [ $(uname -m) = "x86_64" ] ; then
    cpu=amd64

    # Determination si UEFI ou SecureBoot
    if test -d /sys/firmware/efi/ ; then
        if [[ $(dpkg --get-selections mokutil 2> /dev/null | grep -e "install") ]] ; then
            if [ "$(mokutil --sb-state | grep "SecureBoot enabled")" != "" ] ; then
                bios="uefi_sb"
            else
                bios="uefi"
            fi
        else
            bios="uefi"
        fi
    else
            bios="legacy"
    fi

else
    cpu=i686
    bios="legacy"
fi

if [[ $(ps -A | grep "xfce4-session") ]] ; then
    session=xfce
elif [[ $(ps -A | grep "lxqt-session") ]] ; then
    session=lxqt
else
    session=other
fi

file_name="./${emmabuntus_version}-${bios}-${cpu}-${session}-${DATE}.${file_extension}"

# Test si un fichier de config local existe
if test -f ${config_file} ; then

    . ${config_file}

fi

cd /tmp

if [[ ${delay_capture} != "" ]] ; then

    message_audio_pico "${message_capture_image_delay} ${delay_capture} ${message_capture_image_delay_time}" "${langue_pico_accessibility_capture_picture}"

    sleep ${delay_capture}

fi

message_audio_pico "${message_capture_image}" "${langue_pico_accessibility_capture_picture}"

xfce4-screenshooter --fullscreen --mouse --save ${file_name}

message_audio_pico "${message_upload_picture}" "${langue_pico_accessibility_capture_picture}"

if [[ ${expiration_delay} != "" ]] ; then

    return_server=$(curl -s --location --request POST "https://api.imgbb.com/1/upload?expiration=${expiration_delay}&key=${api_key}" --form "image=@${file_name}")

else

    return_server=$(curl -s --location --request POST "https://api.imgbb.com/1/upload?key=${api_key}" --form "image=@${file_name}")

fi

if [[ $(echo ${return_server} | grep "\"status\":200") ]] ; then

    message_audio_pico "${message_upload_picture_ok}" "${langue_pico_accessibility_capture_picture}"

else

    message_audio_pico "${message_upload_picture_ko}" "${langue_pico_accessibility_capture_picture}"

fi

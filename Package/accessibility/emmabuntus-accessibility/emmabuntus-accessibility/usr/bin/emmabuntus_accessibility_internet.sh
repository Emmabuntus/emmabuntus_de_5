#! /bin/bash

# emmabuntus_accessibility_internet.sh --
#
#   This file permits to test connexion Internet for accessibilty for the Emmabuntus Distrib.
#
#   Created on 2010-24 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 Xfce/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

# Command valeurs admises : start ou test
command=$1

###Pour exporter la librairie de gettext.
set -a
source gettext.sh
set +a
export TEXTDOMAIN=emmabuntus-accessibility
export TEXTDOMAINDIR="/usr/share/locale"
. gettext.sh


repertoire_emmabuntus=~/.config/emmabuntus
env_emmabuntus=${repertoire_emmabuntus}/env_emmabuntus
attente_lecture_message="1"

#  chargement des variables d'environnement
. ${env_emmabuntus}

source /usr/bin/emmabuntus_accessibility_func.sh

langue_pico_accessibility_internet="$(eval_gettext '#langue_pico_accessibility_internet en-US')"
langue_pico_accessibility_internet=$(echo ${langue_pico_accessibility_internet} | cut -d " " -f 2)
if [[ ${langue_pico_accessibility_internet} == "en-US" && ${Language_pico} == "en-GB" ]] ; then langue_pico_accessibility_internet="en-GB" ; fi

message_internet_ok="$(eval_gettext 'Connected to Internet.')"
message_internet_ko="$(eval_gettext 'Disconnected from Internet.')"
message_device_found_ethernet_wifi="$(eval_gettext 'Ethernet and Wireless devices found.')"
message_device_found_ethernet="$(eval_gettext 'Ethernet device found.')"
message_device_found_wifi="$(eval_gettext 'Wireless device found.')"
message_device_not_found="$(eval_gettext 'Ethernet and Wireless devices not found.')"
message_advice="$(eval_gettext 'To configure Internet, launch the nmtui application via Alt Super N')"

if [[ ${command} == "test" ]] ; then

    if [[ $(ping -w 1 sourceforge.net | grep "1 received") ]] ; then

        message="${message_internet_ok}"

    else

        device_ethernet=$(inxi -nz | grep "Device" | grep "Ethernet")
        device_wifi=$(inxi -nz | grep "Device" | grep "Wireless")

        if [[ ${device_ethernet} != "" && ${device_wifi} != "" ]] ; then
            message_device=${message_device_found_ethernet_wifi}
        elif [[ ${device_ethernet} != "" ]] ; then
            message_device=${message_device_found_ethernet}
        elif [[ ${device_wifi} != "" ]] ; then
            message_device=${message_device_found_wifi}
        else
            message_device=${message_device_not_found}
        fi

        message="${message_internet_ko}
        ${message_device}
        ${message_advice}"

    fi

    escape_orca_read

    # Si Orca d'activé attendre x secondes avant de lire le message d'information sur la modification du son
    if [[ $(ps -A | grep orca) ]] ; then
        sleep ${attente_lecture_message}
    fi

    message_audio_pico "${message}" "${langue_pico_accessibility_internet}"

elif [[ $(cat /proc/cmdline | grep -i accessibility) || $(gsettings get org.gnome.desktop.a11y.applications screen-reader-enabled) == "true" || ( ${Internet_info} == "1" &&  ${Accessibility} == "1" &&  ${command} == "start" ) ]] ; then

    if [[ $(ping -w 1 sourceforge.net | grep "1 received") ]] ; then

        message="${message_internet_ok}"

    else

        message="${message_internet_ko}"
    fi

    # Si Orca d'activé attendre x secondes avant de lire le message d'information sur la modification du son
    if [[ $(ps -A | grep orca) ]] ; then
        sleep ${attente_lecture_message}
    fi

    message_audio_pico "${message}" "${langue_pico_accessibility_internet}"

fi

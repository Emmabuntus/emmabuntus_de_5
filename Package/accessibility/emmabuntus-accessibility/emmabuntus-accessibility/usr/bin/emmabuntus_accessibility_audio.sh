#! /bin/bash

# emmabuntus_accessibility_audio.sh --
#
#   This file permits to change de volume audio for the Emmabuntus Distrib.
#
#   Created on 2010-24 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 Xfce/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

###Pour exporter la librairie de gettext.
set -a
source gettext.sh
set +a
export TEXTDOMAIN=emmabuntus-accessibility
export TEXTDOMAINDIR="/usr/share/locale"
. gettext.sh


repertoire_emmabuntus=~/.config/emmabuntus
env_emmabuntus=${repertoire_emmabuntus}/env_emmabuntus
attente_lecture_message="1"

#Audio_info="1"

#  chargement des variables d'environnement
. ${env_emmabuntus}

commande_audio=${1}
delta_audio_volume=${2}

if [[ ${Audio_channel_number_sink} == "" ]] ; then
    channel_number_sink="@DEFAULT_SINK@"
else
    channel_number_sink=${Audio_channel_number_sink}
fi

if [[ ${Audio_delta_audio_volume_default} == "" ]] ; then
    delta_audio_volume_default="10"
else
    delta_audio_volume_default=${Audio_delta_audio_volume_default}
fi

if [[ ${delta_audio_volume} == "" ]] ; then
    delta_audio_volume=${delta_audio_volume_default}
fi


if [[ ${Audio_channel_number_source} == "" ]] ; then
    channel_number_source="@DEFAULT_SOURCE@"
else
    channel_number_source=${Audio_channel_number_source}
fi

if [[ ${Audio_delta_micro_volume_default} == "" ]] ; then
    delta_micro_volume_default="10"
else
    delta_micro_volume_default=${Audio_delta_micro_volume_default}
fi

if [[ ${delta_audio_volume} == "" && $(echo ${commande_audio} | grep "micro") ]] ; then
    delta_audio_volume=${delta_micro_volume_default}
fi


# Détermination si le volume est coupé ou pas
mem_language=${LANGUAGE}
mem_lang=${LANG}
LANGUAGE="en_US"
LANG="en_US.UTF-8"

audio_volume=$(pactl get-sink-volume ${channel_number_sink} | grep Volume | cut -d % -f 1 | cut -d / -f 2)
audio_mute=$(pactl get-sink-mute ${channel_number_sink})

if [[ $(echo ${audio_mute} | grep "yes") ]] ; then

    message_audio_tooggle_on=${message_audio_on}
    message_audio_tooggle_off=""

else

    message_audio_tooggle_on=""
    message_audio_tooggle_off=${message_audio_off}

fi

LANGUAGE=${mem_language}
LANG=${mem_lang}

langue_pico_volume_audio="$(eval_gettext '#langue_pico_volume_audio en-US')"
langue_pico_volume_audio=$(echo ${langue_pico_volume_audio} | cut -d " " -f 2)

# Détermination si commande Micro
if [[ $(echo ${commande_audio} | grep "audio") ]] ; then
    message_device1="$(eval_gettext 'sound')"
    message_device2="$(eval_gettext 'microphone')"
elif [[ $(echo ${commande_audio} | grep "micro") ]] ; then
    message_device1="$(eval_gettext 'microphone')"
    message_device2=""
else
    message_device1="$(eval_gettext 'sound')"
    message_device2=""
fi

message_audio_on="$(eval_gettext 'Volume activation') ${message_device1}"

message_audio_off="$(eval_gettext 'Volume mute') ${message_device1}"

message_audio_up="$(eval_gettext 'Volume increase') ${message_device1} $(eval_gettext 'of') ${delta_audio_volume} %"

message_audio_down="$(eval_gettext 'Volume decrease') ${message_device1} $(eval_gettext 'of') ${delta_audio_volume} %"

message_audio_test="$(eval_gettext 'Sound volume test. This one is from') ${audio_volume} %"

message_audio_on_micro_off="$(eval_gettext 'Volume activation') ${message_device1} $(eval_gettext 'Volume mute') ${message_device2}"

message_audio_off_micro_on="$(eval_gettext 'Volume mute') ${message_device1} $(eval_gettext 'Volume activation') ${message_device2}"

message_audio_on_micro_on="$(eval_gettext 'Volume activation') ${message_device1} $(eval_gettext 'Volume activation') ${message_device2}"

message_audio_non_dispo="$(eval_gettext 'Unknown command')"

if [[ $(ps -A | grep "xfce4-session") ]] ; then
    xfce4-terminal --minimize -x xdotool key "Escape"
fi

#if [[ $(ps -A | grep "lxqt-session") ]] ; then
#    qterminal -e xdotool key "Escape"
#fi

# Si Orca d'activé attendre x secondes avant de lire le message d'information sur la modification du son
if [[ $(ps -A | grep orca) ]] ; then
    sleep ${attente_lecture_message}
fi

audio_message() {

if [[ ${Audio_info} == "1" && "${1}" != "" ]] ; then

    message="${1}"
    #echo ${message}

    echo ${message} | pico2wave -w /tmp/message_audio_son_accessibility.wav -l ${langue_pico_volume_audio}
    aplay -q /tmp/message_audio_son_accessibility.wav > /dev/null
    rm /tmp/message_audio_son_accessibility.wav

fi

}

case $commande_audio in

                   "") audio_message "${message_audio_tooggle_off}"; pactl set-sink-mute ${channel_number_sink} toggle; audio_message "${message_audio_tooggle_on}";;
                  "+") pactl set-sink-volume ${channel_number_sink} +${delta_audio_volume}%; pactl set-sink-mute ${channel_number_sink} false; audio_message "${message_audio_up}";;
                  "-") pactl set-sink-volume ${channel_number_sink} -${delta_audio_volume}%; pactl set-sink-mute ${channel_number_sink} false; audio_message "${message_audio_down}";;
                 "on") pactl set-sink-mute ${channel_number_sink} false; audio_message "${message_audio_on}";;
                "off") audio_message "${message_audio_off}"; pactl set-sink-mute ${channel_number_sink} true;;
               "test") pactl set-sink-mute ${channel_number_sink} false; audio_message "${message_audio_test}";;

            "micro_+") pactl set-source-volume ${channel_number_source} +${delta_audio_volume}%; pactl set-source-mute ${channel_number_source} false; audio_message "${message_audio_up}";;
            "micro_-") pactl set-source-volume ${channel_number_source} -${delta_audio_volume}%; pactl set-source-mute ${channel_number_source} false; audio_message "${message_audio_down}";;
           "micro_on") pactl set-source-mute ${channel_number_source} false; audio_message "${message_audio_on}";;
          "micro_off") audio_message "${message_audio_off}"; pactl set-source-mute ${channel_number_source} true;;

 "audio_on_micro_off") pactl set-sink-mute ${channel_number_sink} false; pactl set-source-mute ${channel_number_source} true; audio_message "${message_audio_on_micro_off}";;
 "audio_off_micro_on") audio_message "${message_audio_off_micro_on}"; pactl set-sink-mute ${channel_number_sink} true; pactl set-source-mute ${channel_number_source} false;;
  "audio_on_micro_on") audio_message "${message_audio_on_micro_on}"; pactl set-sink-mute ${channel_number_sink} false; pactl set-source-mute ${channel_number_source} false;;

                    *) echo "${message_audio_non_dispo}"; audio_message "${message_audio_non_dispo}";;

esac



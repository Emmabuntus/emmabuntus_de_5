#! /bin/bash

# emmabuntus_accessibility_window_desktop.sh --
#
#   This file permits to prepare Accessibilty for the Emmabuntus Distrib.
#
#   Created on 2010-24 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 Xfce/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

# Info pour lister les config
# xfconf-query -c xsettings -l -v

repertoire_emmabuntus=~/.config/emmabuntus
env_emmabuntus=${repertoire_emmabuntus}/env_emmabuntus

source /usr/bin/emmabuntus_accessibility_func.sh

channel="xsettings"
channel_base_gtk="/Gtk/"
channel_base_net="/Net/"
cursorthemesize_value_min="16"
cursorthemesize_value_max="72"
fontthemesize_value_min="11"

# Config ColorTheme
file_gtk2=~/.gtkrc-2.0
file_gtk3=~/.config/gtk-3.0/settings.ini
dir_theme=/usr/share/themes

source /usr/bin/emmabuntus_found_theme.sh
name_dark_theme=$(FOUND_THEME_DARK)
name_light_theme=$(FOUND_THEME_LIGHT)

if [[ ${language_pico_enum} == "" ]] ; then
    language_pico_enum=( "de-DE" "en-GB" "en-US" "es-ES" "fr-FR" "it-IT" )
fi

if [[ ${cursorthemename_value_enum} == "" ]] ; then
    cursorthemename_value_enum=( "Adwaita" "ComixCursors-Opaque-Black" "ComixCursors-Opaque-Blue"  "ComixCursors-Opaque-Green" "ComixCursors-Opaque-Orange"  "ComixCursors-Opaque-Red" "ComixCursors-Opaque-White" "Maya-Orange" "Maya-Black" "Maya-White" "Bgreat-Black" "Bgreat-White" )
fi

if [[ ${fontthemename_value_enum} == "" ]] ; then
    fontthemename_value_enum=( "Liberation Sans" "Eido" "Luciole" "OpenDyslexic" "Tiresias Infofont" )
fi

if [[ ${fontthemesize_value_max_enum} == "" ]] ; then
    fontthemesize_value_max_enum=( "20" "16" "17" "18" "18" )
fi

if [[ ${themename_value_enum} == "" ]] ; then
    themename_value_enum=( "Arc-Dark-largerborders" "BlackMATE" "HighContrast" "Arc-Darker-largerborders" "Qogir-dark-largerborders" "Qogir-largerborders" "Qogir-light-largerborders" )
fi

if [[ ${iconthemename_value_enum} == "" ]] ; then
    iconthemename_value_enum=( "revival-blue" "ContrastHigh" "HighContrast" "Papirus" "Papirus-Dark" "Papirus-Light" )
fi

re_is_integer='^[0-9]+$'

command=$1
value=$2
value_size=$3

#echo "command=$1 : value=$2 : value_size=$3"

if [[ $(ps -A | grep "xfce4-session") ]] ; then

    if [[ ${command} == "CursorThemeSize" && ${value} -ge ${cursorthemesize_value_min} && ${value} =~ ${re_is_integer} ]] ; then

        xfconf-query --verbose --create --channel "${channel}" --property "${channel_base_gtk}${command}" --type int --set "${value}"

    elif [[ ${command} == "CursorThemeName" &&  $(printf '%s\n' "${cursorthemename_value_enum[@]}" | grep -x "${value}" ) ]] ; then

        xfconf-query --verbose --create --channel "${channel}" --property "${channel_base_gtk}${command}" --set "${value}"

    elif [[ ${command} == "ThemeName" &&  $(printf '%s\n' "${themename_value_enum[@]}" | grep -x "${value}" ) ]] ; then

        xfconf-query --verbose --create --channel "${channel}" --property "${channel_base_net}${command}" --set "${value}"


    elif [[ ${command} == "IconThemeName" &&  $(printf '%s\n' "${iconthemename_value_enum[@]}" | grep -x "${value}" ) ]] ; then

        xfconf-query --verbose --create --channel "${channel}" --property "${channel_base_net}${command}" --set "${value}"


    elif [[ ${command} == "FontName" && ${value_size} -ge ${fontthemesize_value_min} && ${value_size} =~ ${re_is_integer} && $(printf '%s\n' "${fontthemename_value_enum[@]}" | grep -x "${value}" )  ]] ; then

        for i in "${!fontthemename_value_enum[@]}"; do
        if [[ "${fontthemename_value_enum[$i]}" = "${value}" ]]; then
            fontthemesize_value_max=${fontthemesize_value_max_enum[$i]};
        fi
        done

        if [[ ${value_size} -le ${fontthemesize_value_max} ]] ; then
            xfconf-query --verbose --create --channel "${channel}" --property "${channel_base_gtk}${command}" --type string --set "${value} ${value_size}"
            # Modif Libre Office
        fi


    elif [[ ${command} == "CorlorTheme" && ( ${value} == "true" || ${value} == "false" ) ]] ; then

        if [ ${value} == "true" ] ; then
            name_theme=${name_dark_theme}
        else
            name_theme=${name_light_theme}
        fi

        # Config pour LXQt
        sed s/"^gtk-theme-name[^$]*"/"gtk-theme-name = \"${name_theme}\""/ ${file_gtk2} > ${file_gtk2}.tmp
        cp ${file_gtk2}.tmp ${file_gtk2}
        rm ${file_gtk2}.tmp

        sed s/"^gtk-theme-name[^$]*"/"gtk-theme-name=${name_theme}"/ ${file_gtk3} > ${file_gtk3}.tmp
        cp ${file_gtk3}.tmp ${file_gtk3}
        rm ${file_gtk3}.tmp

        # Config pour Xfce
        if test -d ${dir_theme}/${name_theme}/xfwm4 ; then
            xfconf-query --verbose --create --channel xfwm4 --property /general/theme --set "${name_theme}"
        fi

        xfconf-query --verbose --create --channel xsettings -p /Net/ThemeName --set "${name_theme}"

        # Test si l'utilisateur fait parti du groupe sudo
        if [[ $(groups | grep sudo) != "" && $(cat /proc/cmdline | grep -i boot=live) == "" ]] ; then
            pkexec /usr/bin/lightdm_gtk_greeter_exec.sh ${value} ${name_dark_theme} ${name_light_theme} > /dev/null
        fi
    fi
fi

if [[ ${command} == "LxKeyMap" ]] ; then

    /usr/bin/lxkeymap
    KEYBOARD=`setxkbmap -v | awk -F "+" '/symbols/ {print $2}'`

    pkill gtkdialog > /dev/null
    /usr/bin/emmabuntus_accessibility_window.sh

fi


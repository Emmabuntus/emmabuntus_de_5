#! /bin/bash

# emmabuntus_accessibility.sh --
#
#   This file permits to prepare Accessibilty for the Emmabuntus Distrib.
#
#   Created on 2010-24 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 Xfce/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

. "$HOME/.config/user-dirs.dirs"

repertoire_emmabuntus=~/.config/emmabuntus
env_emmabuntus=${repertoire_emmabuntus}/env_emmabuntus
fichier_init_config=${repertoire_emmabuntus}/init_emmabuntus_config_accessibility.txt

# Fichiers config autologin_lightdm
file_lightdm=/etc/lightdm/lightdm.conf
if [[ ${USER} == root ]] ; then
tmp_postinstall=/root/.cache/postinstall
else
tmp_postinstall=/home/${USER}/.cache/postinstall
fi


Init=""
Init=$1

# Lancement des fonctions d'accessibilité
if [[ $(cat /proc/cmdline | grep -i accessibility) || $(gsettings get org.gnome.desktop.a11y.applications screen-reader-enabled) == "true" || ${Init} != "" ]] ; then

if [[ ${Init} != ""  || ! ( -f  ${fichier_init_config} ) ]] ; then

###Pour exporter la librairie de gettext.
set -a
source gettext.sh
set +a
export TEXTDOMAIN=emmabuntus-accessibility
export TEXTDOMAINDIR="/usr/share/locale"
. gettext.sh

# Lancement de cette commande pour corriger la non mise à jour du curseur lors du changement de celui-ci
if [[ $(ps -A | grep "xfce4-session") ]] ; then
    xfsettingsd --replace &
fi

source /usr/bin/emmabuntus_accessibility_func.sh

audio_min_level=50

langue_pico_default="en-US"
langue_espeak_default="en-us"

langue_pico_message_accueil_accessibility_audio="$(eval_gettext '#langue_pico_message_accueil_accessibility_audio en-US')"
langue_pico_message_accueil_accessibility_audio=$(echo ${langue_pico_message_accueil_accessibility_audio} | cut -d " " -f 2)

ebook_speaker_visual="$(eval_gettext 'eBook-speaker')"
ebook_speaker_audio="$(eval_gettext 'eBookspeaker')"
espeak_visual="$(eval_gettext 'eSpeak')"
espeak_audio="$(eval_gettext 'espeak')"

message_accueil_accessibility_1="$(eval_gettext 'Accessibility activation panel of ')"

message_accueil_accessibility_1a="$(eval_gettext 'for the blind or visually impaired people.')"

message_accueil_accessibility_2="$(eval_gettext 'The \042Escape\042 key allows you to stop reading this information, and the \042R\042 key to read it back.')"

message_accueil_accessibility_3="$(eval_gettext 'To start audio aid or info shortcuts via \042')"
message_accueil_accessibility_3a="$(eval_gettext '\042, press the \042I\042 or \042H\042 key. To quit \042')"
message_accueil_accessibility_3b="$(eval_gettext '\042 press \042Q\042.')"

message_accueil_accessibility_4="$(eval_gettext 'For the visually impaired, configure the functions located in the \042Visual\042 frame to suit your visual acuity.')"
message_accueil_accessibility_4a="$(eval_gettext 'To help you, you can start Compiz zoom by pressing Alt Super C, then Control up or down arrow to zoom in or out.')"

message_accueil_accessibility_5="$(eval_gettext 'For the blind, if you wish to use these functions, press their corresponding keys listed bellow to toggle them on or off :')"

message_accueil_accessibility_6="$(eval_gettext '\042O\042 for Orca, \042C\042 for Orca\047s options, \042T\042 for laptop shortcuts, \042A\042 for audio info, \042W\042 to find out the Internet state,')"

message_accueil_accessibility_7="$(eval_gettext '\042U\042 for update state, \042L\042 for autologin, \042E\042 to use ')"
message_accueil_accessibility_7a="$(eval_gettext 'instead of Pico, \042P\042 and \042K\042 to find out the synthesis voices for Pico and ')"

message_accueil_accessibility_8="$(eval_gettext 'Use \042S\042 to know the config state, then \042Enter\042 to validate your choices. Otherwise, press \042Q\042 to ignore these functions.')"

message_accueil_accessibility_audio="${message_accueil_accessibility_1}Emmabountous ${message_accueil_accessibility_1a}
${message_accueil_accessibility_2}
${message_accueil_accessibility_3}${ebook_speaker_audio}${message_accueil_accessibility_3a}${ebook_speaker_audio}${message_accueil_accessibility_3b}
${message_accueil_accessibility_4}
${message_accueil_accessibility_4a}
${message_accueil_accessibility_5}
${message_accueil_accessibility_6}
${message_accueil_accessibility_7} ${espeak_audio} ${message_accueil_accessibility_7a}${espeak_audio}.
${message_accueil_accessibility_8}"

message_accueil_accessibility_visuel="\n\
${message_accueil_accessibility_1}Emmabuntüs ${message_accueil_accessibility_1a}\n\
\n\
${message_accueil_accessibility_2}\n\
${message_accueil_accessibility_3}${ebook_speaker_visual}${message_accueil_accessibility_3a}${ebook_speaker_visual}${message_accueil_accessibility_3b}\n\
\n\
${message_accueil_accessibility_4}\n\
${message_accueil_accessibility_4a}\n\
\n\
${message_accueil_accessibility_5}\n\
${message_accueil_accessibility_6}\n\
${message_accueil_accessibility_7} ${espeak_visual} ${message_accueil_accessibility_7a}${espeak_visual}.\n\
${message_accueil_accessibility_8}"

export message_accueil_accessibility_audio
export message_accueil_accessibility_visuel

msg_orca=$(eval_gettext 'Orca')
comment_orca=$(eval_gettext 'Screen audio reader')

msg_compiz=$(eval_gettext 'Compiz')
comment_compiz=$(eval_gettext 'Zoom management utility, cursor settings')

#  chargement des variables d'environnement
. ${env_emmabuntus}


if [[ $(ps -A | grep orca) ]] ; then
    ORCA="true"
else
    ORCA="false"
fi

update_config_emmabuntus Orca ${ORCA} ${env_emmabuntus}

if [[ ${Audio_info} == "1" ]] ; then
    AUDIO_INFO="true"
else
    AUDIO_INFO="false"
fi

if ! test -f  ${fichier_init_config} ; then

    INTERNET_INFO="true"

else

    if [[ ${Internet_info} == "1" ]] ; then
        INTERNET_INFO="true"
    else
        INTERNET_INFO="false"
    fi

fi

if ! test -f  ${fichier_init_config} ; then

    UPDATE_INFO="true"

else

    if [[ ${Update_info} == "1" ]] ; then
        UPDATE_INFO="true"
    else
        UPDATE_INFO="false"
    fi

fi

if [[ $(ps -A | grep compiz) ]] ; then
    COMPIZ="true"
else
    COMPIZ="false"
fi

update_config_emmabuntus Compiz ${COMPIZ} ${env_emmabuntus}

if [[ ${Postinstall} == "1" ]] ; then
    POSTINSTALL="true"
else
    POSTINSTALL="false"
fi

if [[ ${Autologin} == "1" ]] ; then
    AUTOLOGIN="true"
else
    AUTOLOGIN="false"
fi

if [[ ${Enable_espeak} == "1" ]] ; then
    ENABLE_ESPEAK="true"
else
    ENABLE_ESPEAK="false"
fi

if [[ ${Orca_enable_cairodock} == "1" ]] ; then
    ORCA_ENABLE_CAIRODOCK="true"
else
    ORCA_ENABLE_CAIRODOCK="false"
fi

if [[ ${Compiz_enable_cairodock} == "1" ]] ; then
    COMPIZ_ENABLE_CAIRODOCK="true"
else
    COMPIZ_ENABLE_CAIRODOCK="false"
fi

EMULATION_MOUSE=${Emulation_mouse}

if ! test -f  ${fichier_init_config} ; then

    # Detection Laptop
    /usr/bin/laptop-detect
    status=$?
    echo "status Laptop = ${status}" >> ${fichier_init_config}

    if [ ${status} -eq 0 ] ; then
        # Laptop
        LAPTOP_ORCA="true"
    else
        LAPTOP_ORCA="false"
    fi

else

    if [[ ${Laptop_orca} == "1" ]] ; then
        LAPTOP_ORCA="true"
    else
        LAPTOP_ORCA="false"
    fi

fi


update_config_emmabuntus Laptop_orca ${LAPTOP_ORCA} ${env_emmabuntus}
update_laptop_orca ${LAPTOP_ORCA}

export ORCA
export AUDIO_INFO
export INTERNET_INFO
export UPDATE_INFO
export COMPIZ
export POSTINSTALL
export AUTOLOGIN
export LAPTOP_ORCA
export EMULATION_MOUSE
export ORCA_ENABLE_CAIRODOCK
export COMPIZ_ENABLE_CAIRODOCK

langue=$(echo $LANG | cut -d . -f 1)
langue_code=$(echo $langue | cut -d _ -f 1)
langue_country=$(echo $langue | cut -d _ -f 2)

# Détermination langue de Sox Pico
if ! test -f  ${fichier_init_config} ; then

    case ${langue_code} in
        "de") LANGUE_PICO="de-DE";;
        "es") LANGUE_PICO="es-ES";;
        "it") LANGUE_PICO="it-IT";;
        "fr") LANGUE_PICO="fr-FR";;
        "en") if [[ ${langue_country} == "GB" ]] ; then LANGUE_PICO="en-GB" ; else LANGUE_PICO="en-US" ; fi ;;
           *) LANGUE_PICO=${langue_pico_default} ; ENABLE_ESPEAK="true" ;;
    esac

else

    LANGUE_PICO=${Language_pico}

fi

export LANGUE_PICO

update_config_emmabuntus Language_pico ${LANGUE_PICO} ${env_emmabuntus}
update_config_orca pico ${LANGUE_PICO}

# Détermination langue de eSpeak
if ! test -f  ${fichier_init_config} ; then

    espeak --voices | cut -d " " -f 4 > ${repertoire_emmabuntus}/voices_espeak.txt

    LANGUE_ESPEAK=""
    langue_country_lower=$(echo "${langue_country,,}")

    while read voice ; do

        if [[ ${langue_code} == ${voice} || "${langue_code}-${langue_country_lower}" == ${voice} ]];  then

            LANGUE_ESPEAK="${voice}"
            #echo "LANGUE_ESPEAK = ${LANGUE_ESPEAK}"

        fi
    done < "${repertoire_emmabuntus}/voices_espeak.txt"

    if [[ ${LANGUE_ESPEAK} == "" ]]; then
        LANGUE_ESPEAK=${langue_espeak_default}
        ENABLE_ESPEAK="false"
    fi

else

    LANGUE_ESPEAK=${Language_espeak}

fi

export LANGUE_ESPEAK
export ENABLE_ESPEAK

update_config_emmabuntus Language_espeak ${LANGUE_ESPEAK} ${env_emmabuntus}

if  [[ ${ENABLE_ESPEAK} == "true" ]]; then
    update_config_orca espeak ${LANGUE_ESPEAK}
else
    update_config_orca pico ${LANGUE_PICO}
fi

KEYBOARD=`setxkbmap -v | awk -F "+" '/symbols/ {print $2}'`

export KEYBOARD

# Patch level audio
if ! test -f  ${fichier_init_config} ; then

    audio_volume=$(pactl -- get-sink-volume ${channel_number} | grep Volume | cut -d % -f 1 | cut -d / -f 2)
    if [[ ${audio_volume} -lt ${audio_min_level} ]]; then
        pactl -- set-sink-volume 0 ${Audio_default_level}%
    fi

fi

if [[ ${langue_pico_message_accueil_accessibility_audio} == "en-US" && ${Language_pico} == "en-GB" ]] ; then langue_pico_message_accueil_accessibility_audio="en-GB" ; fi

escape_orca_read

# Lancement de la lecture audio en parallèle
message_audio_pico "${message_accueil_accessibility_audio}" "${langue_pico_message_accueil_accessibility_audio}" &

Language_pico_old=${LANGUE_PICO}
Language_espeak_old=${LANGUE_ESPEAK}

if [[ ${ENABLE_ESPEAK} == "true" ]] ; then Enable_espeak_old="1" ; else Enable_espeak_old="0" ; fi
if [[ ${LAPTOP_ORCA} == "true" ]] ; then Laptop_orca_old="1" ; else Laptop_orca_old="0" ; fi

/usr/bin/emmabuntus_accessibility_window.sh ${Init}
status_exit=$?

if [[ $(ps -A | grep aplay) ]] ; then pkill aplay > /dev/null ; fi

# Deuxième pkill car cela n'est pas stoppé au premier
if [[ $(ps -A | grep aplay) ]] ; then pkill aplay > /dev/null ; fi

if [[ ${status_exit} == "1" ]] ; then
    exit 1
fi

#  chargement des variables d'environnement
. ${env_emmabuntus}

DATE=`date +"%d:%m:%Y - %H:%M:%S"`
echo "DATE = $DATE" >> ${fichier_init_config}

echo "Accessibility = ${Accessibility} : Orca =  ${Orca} : Compiz = ${Compiz} : Dark_theme = ${Dark_theme} : Postinstall = ${Postinstall}" >> ${fichier_init_config}
echo "Language_pico = ${Language_pico} : Enable_espeak = ${Enable_espeak} : Language_espeak = ${Language_espeak} : Laptop_orca = ${Laptop_orca} : Audio_info =  ${Audio_info} : Autologin =  ${Autologin}" >> ${fichier_init_config}
echo "Language_pico_old = ${Language_pico_old} : Enable_espeak_old = ${Enable_espeak_old} : Language_espeak_old = ${Language_espeak_old} : Laptop_orca_old = ${Laptop_orca_old}" >> ${fichier_init_config}
echo "Emulation_mouse = ${Emulation_mouse} : Orca_enable_cairodock = ${Orca_enable_cairodock} : Compiz_enable_cairodock = ${Compiz_enable_cairodock}" >> ${fichier_init_config}

# Lancement ou arrêt de Cairo-dock
if [[ ( ${Dock_XFCE} == "1" && ${Orca} == "0" && ${Compiz} == "0" ) || ( ${Orca} == "1" && ${Orca_enable_cairodock} == "1" )  || ( ${Compiz} == "1" && ${Compiz_enable_cairodock} == "1" ) ]] ; then

    if [[ $(ps -A | grep cairo-dock) ]] ; then
        echo "Cairo-dock ready on" >> ${fichier_init_config}
    else
        if test -L ~/.config/cairo-dock ; then
            /usr/bin/start_cairo_dock.sh
            echo "Cairo-dock on" >> ${fichier_init_config}
        else
            /usr/bin/start_cairo_dock.sh
            echo "Cairo-dock never started" >> ${fichier_init_config}
        fi
    fi

else

    if [[ $(ps -A | grep cairo-dock) ]] ; then
        killall cairo-dock
        echo "Cairo-dock off" >> ${fichier_init_config}
    else
        echo "Cairo-dock ready off" >> ${fichier_init_config}
    fi

fi

if [[ ${Accessibility} == "0" ]] ; then

    if [[ ${Orca} == "0" ]] ; then
        update_config_autostart orca-autostart off
    fi

    if [[ ${Compiz} == "0" ]] ; then
        update_config_autostart compiz-autostart off
    fi

    if [[ $(ps -A | grep orca) ]] ; then killall orca > /dev/null ; fi

    if [[ $(ps -A | grep compiz) ]] ; then
        xfwm4 --replace &
    fi

    update_config_menus off

    # Configuration de l'émulation de la souris

    echo "Config emulation mouse ${Emulation_mouse}" >> ${fichier_init_config}
    update_emulation_mouse ${Emulation_mouse} ${Orca}

    exit 0

else

    # Configuration du menu Wisker

    if [[ ${Orca} == "1" || ${Compiz} == "1" ]] ; then
        echo "Config menus on" >> ${fichier_init_config}
        update_config_menus on
    else
        echo "Config menus off" >> ${fichier_init_config}
        update_config_menus off
    fi

    # Configuration Autologin

    if [[ ${Autologin} == "1" && $(groups | grep sudo) != "" && $(cat /proc/cmdline | grep -i boot=live) == "" ]] ; then
        if [[ $(cat ${file_lightdm} | grep "^autologin-user=${USER}") ]]
        then
            echo "Already active ${USER} autologin" >> ${fichier_init_config}
        else
            sudo /usr/bin/autologin_lightdm_exec.sh
            echo "Activated autologin LightDM" >> ${fichier_init_config}
        fi
    fi

    # Lancement des utilitaires

    if [[ ${Orca} == "1" ]] ; then
        if [[ $(ps -A | grep orca) && ${Language_pico_old} == ${Language_pico} && ${Enable_espeak_old} == ${Enable_espeak} && ${Language_espeak_old} == ${Language_espeak} && ${Laptop_orca_old} == ${Laptop_orca} ]] ; then
            echo "Orca ready on" >> ${fichier_init_config}
        else
            echo "Orca on" >> ${fichier_init_config}
            numlockx off
            orca --replace &
        fi
        update_config_autostart orca-autostart on
    else
        echo "Orca off" >> ${fichier_init_config}
        if [[ $(ps -A | grep orca) ]] ; then killall orca > /dev/null ; fi
        update_config_autostart orca-autostart off
    fi

    if [[ ${Compiz} == "1" ]] ; then
        if [[ $(ps -A | grep compiz) ]] ; then
            echo "Compiz ready on" >> ${fichier_init_config}
        else
            echo "Compiz on" >> ${fichier_init_config}
            compiz ccp --replace &
        fi
        update_config_autostart compiz-autostart on
    else
        echo "Compiz off" >> ${fichier_init_config}
        if [[ $(ps -A | grep compiz) ]] ; then
            xfwm4 --replace &
        fi
        sleep 1
        # Deuxième relance car cela n'est pas passé au premier coup
        if [[ ! $(ps -A | grep xfwm4) ]] ; then
            xfwm4 --replace &
        fi
        update_config_autostart compiz-autostart off
    fi

    # Configuration de l'émulation de la souris

    echo "Config emulation mouse ${Emulation_mouse}" >> ${fichier_init_config}
    update_emulation_mouse ${Emulation_mouse} ${Orca}

    if [[ ${Compiz} == "1" && ${Orca} == "0" ]] ; then
        exit 0
    else
        exit 1
    fi

fi

else

    #  chargement des variables d'environnement
    . ${env_emmabuntus}

    if [[ ${Postinstall} == "1" ]] ; then
        exit 0
    elif [[ ${Accessibility} == "1" ]] ; then
        exit 2
    else
        exit 0
    fi

fi

fi


#! /bin/bash

# emmabuntus_accessibility_gnome_calculator.sh --
#
#   This file permit to use gnome-calculator for accessibility on the Emmabuntus Distrib.
#
#   Created on 2010-24 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 Xfce/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

###Pour exporter la librairie de gettext.
set -a
source gettext.sh
set +a
export TEXTDOMAIN=emmabuntus-accessibility
export TEXTDOMAINDIR="/usr/share/locale"
. gettext.sh

repertoire_emmabuntus=~/.config/emmabuntus
env_emmabuntus=${repertoire_emmabuntus}/env_emmabuntus
update_numlockx_on="0"

#  chargement des variables d'environnement
. ${env_emmabuntus}

source /usr/bin/emmabuntus_accessibility_func.sh

message_keypad_on="$(eval_gettext 'Numeric keypad on')"
message_keypad_off="$(eval_gettext 'Numeric keypad off')"

langue_pico_accessibility_gnome_calculator="$(eval_gettext '#langue_pico_accessibility_gnome_calculator en-US')"
langue_pico_accessibility_gnome_calculator=$(echo ${langue_pico_accessibility_gnome_calculator} | cut -d " " -f 2)
if [[ ${langue_pico_accessibility_gnome_calculator} == "en-US" && ${Language_pico} == "en-GB" ]] ; then langue_pico_accessibility_gnome_calculator="en-GB" ; fi

if [[ $(numlockx status | grep off) ]] ; then
    numlockx on
    message_audio_pico "${message_keypad_on}" "${langue_pico_accessibility_gnome_calculator}"
    update_numlockx_on="1"
fi

export LC_NUMERIC=C.utf8; gnome-calculator

if [[ ${update_numlockx_on} == "1" ]] ; then
    numlockx off
    message_audio_pico "${message_keypad_off}" "${langue_pico_accessibility_gnome_calculator}"

fi

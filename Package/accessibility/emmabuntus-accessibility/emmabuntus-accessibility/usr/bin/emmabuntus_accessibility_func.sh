#! /bin/bash

# emmabuntus_accessibility_func.sh --
#
#   This file for functions of accessibility for the Emmabuntus Distrib.
#
#   Created on 2010-24 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 Xfce/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

escape_orca_read () {

    dwagent_actif=$(ps -A | grep "dwagent.sh")

    # Lancement si dwagent n'est pas actif pour éviter l'envoi des caratères ^[ dans le terminal
    if [[ ${dwagent_actif} == "" ]] ; then

        if [[ $(ps -A | grep "xfce4-session") ]] ; then
            xfce4-terminal --minimize -x xdotool key --clearmodifiers Escape
            sleep 1
            xfce4-terminal --minimize -x xdotool key --clearmodifiers Escape
        fi

        if [[ $(ps -A | grep "lxqt-session") ]] ; then
            #qterminal -e xdotool key --clearmodifiers Escape
            sleep 1
            #qterminal -e xdotool key --clearmodifiers Escape
        fi

    fi

}

message_audio_pico () {

message=${1}
langue_pico_message=${2}

# Suppression du caractère \\042 correspondant à "
message=$(echo "'${message}'" | sed "s%\\\042%%g")

# Remplacement du caractère \\047 correspondant à ' par '
message=$(echo "'${message}'" | sed "s%\\\047%\'%g" | sed "s%\\\%%g")

#echo ${message}

if [[ ${message} != "" && ${langue_pico_message} != "" ]] ; then

    echo ${message} | pico2wave -w /tmp/message_accessibility.wav -l ${langue_pico_message}
    aplay -q /tmp/message_accessibility.wav
    rm /tmp/message_accessibility.wav

fi

}


function update_config_emmabuntus () {

config=${1}
varible=${2}
file_config=${3}

if test -f ${file_config} ; then

    if [[ ${varible} == "true" ]] ; then

        sed s/"^${config}=\"[0-9]*\""/"${config}=\"1\""/ ${file_config} > ${file_config}.tmp

    elif [[ ${varible} == "false" ]] ; then

        sed s/"^${config}=\"[0-9]*\""/"${config}=\"0\""/ ${file_config} > ${file_config}.tmp

    elif [[ ${varible} != "" ]] ; then

         sed s/"^${config}=[^$]*"/"${config}=\"${varible}\""/ ${file_config} > ${file_config}.tmp

    fi

    cp ${file_config}.tmp ${file_config}
    rm ${file_config}.tmp

fi

}


function update_config_autostart () {

file_config=~/.config/autostart

file=${1}
varible=${2}

file_config=${file_config}/${file}.desktop

if test -f ${file_config} ; then

    if [[ ${varible} == "on" ]] ; then

        sed s/"^Hidden=[^$]*"/"Hidden=false"/ ${file_config} > ${file_config}.tmp

    else

        sed s/"^Hidden=[^$]*"/"Hidden=true"/ ${file_config} > ${file_config}.tmp

    fi

    cp ${file_config}.tmp ${file_config}
    rm ${file_config}.tmp

fi

}

function update_config_menus () {

dir_config=~/.local/share/desktop-directories
update_panel="0"

files=("Accessibility-utilities" "Accessibility-help" "Accessibility-audio")

varible=${1}

for file in "${files[@]}"
do

    file_config=${dir_config}/${file}.directory

    if test -f ${file_config} ; then

        if [[ ${varible} == "on" && $(cat ${file_config} | grep NoDisplay=true) ]] ; then

            sed s/"^NoDisplay=[^$]*"/"NoDisplay=false"/ ${file_config} > ${file_config}.tmp
            cp ${file_config}.tmp ${file_config}
            rm ${file_config}.tmp

            update_panel="1"

        elif [[ ${varible} == "off" && $(cat ${file_config} | grep NoDisplay=false) ]] ; then

            sed s/"^NoDisplay=[^$]*"/"NoDisplay=true"/ ${file_config} > ${file_config}.tmp
            cp ${file_config}.tmp ${file_config}
            rm ${file_config}.tmp

            update_panel="1"

        fi

    fi

done

if [[ ${update_panel} == "1" ]] ; then
    if [[ $(ps -A | grep "xfce4-session") ]] ; then
        xfce4-panel --restart
    fi
fi

}


function update_config_orca () {

file_config=~/.local/share/orca/user-settings.conf

if test -f ${file_config} ; then

    synthesis="${1}"
    langue="${2}"
    lang=$(echo ${langue} | cut -d "-" -f 1)
    dialect=$(echo ${langue} | cut -d "-" -f 2)

    if [[ ${synthesis} != "" ]] ; then

        sed s/"\"pico\"\|\"espeak\""/"\"${synthesis}\""/ ${file_config} > ${file_config}.tmp
        cp ${file_config}.tmp ${file_config}
        rm ${file_config}.tmp

    fi

    if [[ ${synthesis} != "" ]] ; then

        sed s/"\ pico\"\|\ espeak\""/"\ ${synthesis}\""/ ${file_config} > ${file_config}.tmp
        cp ${file_config}.tmp ${file_config}
        rm ${file_config}.tmp

    fi

    if [[ ${synthesis} != "" ]] ; then

        sed s/"\"pico\ \|\"espeak\ "/"\"${synthesis}\ "/ ${file_config} > ${file_config}.tmp
        cp ${file_config}.tmp ${file_config}
        rm ${file_config}.tmp

    fi

    if [[ ${lang} != "" ]] ; then

        sed s/"\"lang\":[^,]*"/"\"lang\":\ \"${lang}\""/ ${file_config} > ${file_config}.tmp
        cp ${file_config}.tmp ${file_config}
        rm ${file_config}.tmp

    fi

    if [[ ${dialect} != "" ]] ; then

        sed s/"\"dialect\":[^,]*"/"\"dialect\":\ \"${dialect}\""/ ${file_config} > ${file_config}.tmp
        cp ${file_config}.tmp ${file_config}
        rm ${file_config}.tmp

    fi

    if [[ ${synthesis} == "pico" ]] ; then
        sudo /usr/bin/emmabuntus_accessibility_speech_dispatcher_update_exec.sh "pico"
    else
        sudo /usr/bin/emmabuntus_accessibility_speech_dispatcher_update_exec.sh
    fi

fi

}


function update_laptop_orca () {

file_config_orca=~/.local/share/orca/user-settings.conf
repertoire_emmabuntus=~/.config/emmabuntus
env_emmabuntus=${repertoire_emmabuntus}/env_emmabuntus

#  chargement des variables d'environnement
. ${env_emmabuntus}

if test -f ${file_config_orca} ; then

    laptop="${1}"

    if  [[ ${laptop} == "true" ]] ; then
        orcaModifierKeys_1="Caps_Lock"
        orcaModifierKeys_2="Shift_Lock"
        keyboardLayout="2"
        update_config_emmabuntus Key_orca Caps_Lock ${env_emmabuntus}
    elif [[ ${laptop} == "false" ]] ; then
        orcaModifierKeys_1="Insert"
        orcaModifierKeys_2="KP_Insert"
        keyboardLayout="1"
        update_config_emmabuntus Key_orca Insert ${env_emmabuntus}
    else
        exit 1
    fi

    sed -e "/profiles/,/],/ s/Shift_Lock\|KP_Insert/${orcaModifierKeys_2}/g" ${file_config_orca} > ${file_config_orca}.tmp
    cp ${file_config_orca}.tmp ${file_config_orca}
    rm ${file_config_orca}.tmp

    sed -e "/profiles/,/],/ s/Caps_Lock\|Insert/${orcaModifierKeys_1}/g" ${file_config_orca} > ${file_config_orca}.tmp
    cp ${file_config_orca}.tmp ${file_config_orca}
    rm ${file_config_orca}.tmp

    sed -e "/profiles/,/keyboardLayout/ s/keyboardLayout[^,]*/keyboardLayout\":\ ${keyboardLayout}/g" ${file_config_orca} > ${file_config_orca}.tmp
    cp ${file_config_orca}.tmp ${file_config_orca}
    rm ${file_config_orca}.tmp

    nb_keyboardLayout=$(cat ${file_config_orca} | grep keyboardLayout | wc -l)

    if [ ${nb_keyboardLayout} -eq 1 ] ; then

        sed -e "/profiles/,/brailleContractionTable/ s/\"profile\"/\"keyboardLayout\":\ ${keyboardLayout},\\n            \"profile\"/g" ${file_config_orca} > ${file_config_orca}.tmp
        cp ${file_config_orca}.tmp ${file_config_orca}
        rm ${file_config_orca}.tmp

    fi

    nb_orcamodifierKeys=$(cat ${file_config_orca} | grep orcaModifierKeys | wc -l)

    if  [ ${nb_orcamodifierKeys} -eq 1 ] ; then

        sed -e "/default/,/speechServerFactory/ s/\"speechServerFactory/\"orcaModifierKeys\": \[\\n                \"${orcaModifierKeys_1}\",\\n                \"${orcaModifierKeys_2}\"\\n            \],\\n            \"speechServerFactory/g" ${file_config_orca} > ${file_config_orca}.tmp
        cp ${file_config_orca}.tmp ${file_config_orca}
        rm ${file_config_orca}.tmp

    fi

fi

}


function update_config_tuxtype () {

file_config=~/.config/tuxtype/settings.txt

if test -f ${file_config} ; then

    varible="${1}"
    lang_voice_espeak=$(echo ${varible} | cut -d "-" -f 1)

    case ${lang_voice_espeak} in
        "de") lang="deutsh";;
        "es") lang="espanol";;
        "it") lang="italian";;
        "fr") lang="french";;
        "en") lang="english";;
           *) lang="english";;
    esac

    if [[ ${lang} != "" ]] ; then

        sed /"lang="/d ${file_config} > ${file_config}.tmp
        cp ${file_config}.tmp ${file_config}
        rm ${file_config}.tmp

        sed s/"mus_volume"/"lang=${lang}\\nmus_volume"/ ${file_config} > ${file_config}.tmp
        cp ${file_config}.tmp ${file_config}
        rm ${file_config}.tmp

    fi

fi

}


function update_config_ebook_speaker () {

file_config=~/.eBook-speaker.xml

if test -f ${file_config} ; then

    varible_espeak="${1}"
    varible_pico="${2}"
    lang_voice_espeak=$(echo ${varible_espeak} | cut -d "-" -f 1)

    if [[ ${lang_voice_espeak} != "" ]] ; then

        sed s/"espeak[^$]*$"/"espeak -f eBook-speaker.txt -w eBook-speaker.wav -v ${lang_voice_espeak}"/ ${file_config} > ${file_config}.tmp
        cp ${file_config}.tmp ${file_config}
        rm ${file_config}.tmp

    fi

    if [[ ${varible_pico} != "" ]] ; then

        sed s/"pico2wave[^$]*$"/"pico2wave -w eBook-speaker.wav -l ${varible_pico}"/ ${file_config} > ${file_config}.tmp
        cp ${file_config}.tmp ${file_config}
        rm ${file_config}.tmp

    fi

fi

}

function update_emulation_mouse () {

    emulation_mouse="${1}"
    orca="${2}"

    if [[ $(ps -A | grep "mousetweaks") ]] ; then
        killall mousetweaks
    fi

    if [[ ${emulation_mouse} == "NumPad" ]] ; then

        if [[ $(ps -A | grep "xfce4-session") ]] ; then
            xfconf-query --verbose --create --channel accessibility --property /MouseKeys --type 'bool' --set 'true'
        fi

        update_config_emmabuntus Mousetweaks "false" ${env_emmabuntus}

    elif [[ ${emulation_mouse} == "MouseTweaks" ]] ; then

        if [[ $(ps -A | grep "xfce4-session") ]] ; then
            xfconf-query --verbose --create --channel accessibility --property /MouseKeys --type 'bool' --set 'false'
        fi

        update_config_emmabuntus Mousetweaks "true" ${env_emmabuntus}

        mousetweaks --dwell --dwell-time=${Mousetweaks_dwell_time} --threshold=${Mousetweaks_threshold} --geometry=${Mousetweaks_geometry} &

    else

        if [[ $(ps -A | grep "xfce4-session") ]] ; then
            xfconf-query --verbose --create --channel accessibility --property /MouseKeys --type 'bool' --set 'false'
        fi

        update_config_emmabuntus Mousetweaks "false" ${env_emmabuntus}

    fi
}

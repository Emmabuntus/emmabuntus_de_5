#! /bin/bash

# emmabuntus_accessibility_state_keys.sh --
#
#   This file permits to test state of keys for accessibilty for the Emmabuntus Distrib.
#
#   Created on 2010-24 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 Xfce/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

# Command valeurs admises : start ou test
command=$1

###Pour exporter la librairie de gettext.
set -a
source gettext.sh
set +a
export TEXTDOMAIN=emmabuntus-accessibility
export TEXTDOMAINDIR="/usr/share/locale"
. gettext.sh


repertoire_emmabuntus=~/.config/emmabuntus
env_emmabuntus=${repertoire_emmabuntus}/env_emmabuntus
attente_lecture_message="1"

#  chargement des variables d'environnement
. ${env_emmabuntus}

source /usr/bin/emmabuntus_accessibility_func.sh

langue_pico_accessibility_state_keys="$(eval_gettext '#langue_pico_accessibility_state_keys en-US')"
langue_pico_accessibility_state_keys=$(echo ${langue_pico_accessibility_state_keys} | cut -d " " -f 2)
if [[ ${langue_pico_accessibility_state_keys} == "en-US" && ${Language_pico} == "en-GB" ]] ; then langue_pico_accessibility_state_keys="en-GB" ; fi

message_state_on="$(eval_gettext 'on')"
message_state_off="$(eval_gettext 'off')"
message_state_unknown="$(eval_gettext 'unknown')"
message_key_caps_lock="$(eval_gettext 'Caps Lock')"
message_key_num_lock="$(eval_gettext 'Num Lock')"
message_key_scroll_lock="$(eval_gettext 'Scroll Lock')"
message_key_num_lock_on_orca="$(eval_gettext 'Warning: If you launch Orca learning mode, remember to unlock the numeric keypad beforehand, otherwise you wont be able to use its help.')"
num_lock_on="false"

escape_orca_read

# Si Orca d'activé attendre x secondes avant de lire le message d'information sur la modification du son
if [[ $(ps -A | grep orca) ]] ; then
    sleep ${attente_lecture_message}
fi

state_on=$(xset q | grep "Caps Lock:   on")
state_off=$(xset q | grep "Caps Lock:   off")

if [[ ${state_on} != "" ]] then
    state_key_caps_lock="${message_state_on}"
elif [[ ${state_off} != "" ]] then
    state_key_caps_lock="${message_state_off}"
else
    state_key_caps_lock="${message_state_unknown}"
fi

state_on=$(xset q | grep "Num Lock:    on")
state_off=$(xset q | grep "Num Lock:    off")

if [[ ${state_on} != "" ]] then
    state_key_num_lock="${message_state_on}"
    num_lock_on="true"
elif [[ ${state_off} != "" ]] then
    state_key_num_lock="${message_state_off}"
else
    state_key_num_lock="${message_state_unknown}"
fi

state_on=$(xset q | grep "Scroll Lock: on")
state_off=$(xset q | grep "Scroll Lock: off")

if [[ ${state_on} != "" ]] then
    state_key_scroll_lock="${message_state_on}"
elif [[ ${state_off} != "" ]] then
    state_key_scroll_lock="${message_state_off}"
else
    state_key_scroll_lock="${message_state_unknown}"
fi

message="${message_key_caps_lock} ${state_key_caps_lock}"
message_audio_pico "${message}" "${langue_pico_accessibility_state_keys}"

message="${message_key_num_lock} ${state_key_num_lock}"
message_audio_pico "${message}" "${langue_pico_accessibility_state_keys}"

message="${message_key_scroll_lock} ${state_key_scroll_lock}"
message_audio_pico "${message}" "${langue_pico_accessibility_state_keys}"

if [[ ${num_lock_on} == "true" ]] then
    message_audio_pico "${message_key_num_lock_on_orca}" "${langue_pico_accessibility_state_keys}"
fi


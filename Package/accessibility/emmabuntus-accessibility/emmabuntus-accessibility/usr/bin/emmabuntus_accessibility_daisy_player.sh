#! /bin/bash

# emmabuntus_accessibility_daisy_player.sh --
#
#   This file permits to load Daisy files on accessibility for the Emmabuntus Distrib.
#
#   Created on 2010-24 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 Xfce/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

###Pour exporter la librairie de gettext.
set -a
source gettext.sh
set +a
export TEXTDOMAIN=emmabuntus-accessibility
export TEXTDOMAINDIR="/usr/share/locale"
. gettext.sh

mode=$1

repertoire_emmabuntus=~/.config/emmabuntus
env_emmabuntus=${repertoire_emmabuntus}/env_emmabuntus
attente_lecture_message="1"

dir_daisy="/usr/share/Documentation_emmabuntus/Documents/daisy/"

message_start_daisy_player_script_cdrom="$(eval_gettext 'Started Daisy player script to use the Daisy CD ROM')"
message_start_daisy_player="$(eval_gettext 'Start Daisy player. To quit Daisy player type the letter Q')"
message_cdrom_daisy="$(eval_gettext 'Disk inserted in the CD ROM player is in Daisy format')"
message_cdrom_not_daisy="$(eval_gettext 'Disk inserted in the CD ROM player is not in Daisy format')"
message_cdrom_not_mounted="$(eval_gettext 'Failed to mount the CD ROM disk')"
message_cdrom_not_found="$(eval_gettext 'No disk found on CD ROM')"

message_start_daisy_player_script_explorer="$(eval_gettext 'Started Daisy player script to read Daisy files on disk')"
message_file_not_found="$(eval_gettext 'No ZIP file in folder ')"
message_one_file_found="$(eval_gettext 'One ZIP file found in folder, Daisy player is open directly with this file. To quit Daisy player type letter Q')"
message_number_files_found="$(eval_gettext 'ZIP files found in folder, File explorer is open in files selection window')"
message_select_files="$(eval_gettext 'Select one file, and type O to open it with Daisy Player. When the Daisy player is launched, type the letter Q to exit')"

. ${env_emmabuntus}

source /usr/bin/emmabuntus_accessibility_func.sh

langue_pico_accessibility_daisy="$(eval_gettext '#langue_pico_accessibility_daisy en-US')"
langue_pico_accessibility_daisy=$(echo ${langue_pico_accessibility_daisy} | cut -d " " -f 2)
if [[ ${langue_pico_accessibility_daisy} == "en-US" && ${Language_pico} == "en-GB" ]] ; then langue_pico_accessibility_daisy="en-GB" ; fi

escape_orca_read

# Si Orca d'activé attendre x secondes avant de lire le message d'information sur la modification du son
if [[ $(ps -A | grep orca) ]] ; then
    sleep ${attente_lecture_message}
fi

if [[ ${mode} == "CDROM" ]] ; then

    message_audio_pico "${message_start_daisy_player_script_cdrom}" "${langue_pico_accessibility_daisy}"

    # Test présense CDROM
    msg_setcd_info=$(setcd -i)
    if [[ $(echo ${msg_setcd_info} | grep "Disc found in drive") ]] ; then

        # Test CDROM monté
        mount_dir=$(mount | grep /dev/sr0 | cut -d " " -f3)
        #echo "mount_dir=${mount_dir}"

        if [[ ${mount_dir} != "" ]] ; then

            # Test format Daisy
            if [[ $(ls ${mount_dir} | grep "ncc.html") ]] ; then

                message_audio_pico "${message_cdrom_daisy}" "${langue_pico_accessibility_daisy}"
                message_audio_pico "${message_start_daisy_player}" "${langue_pico_accessibility_daisy}"

                xfce4-terminal -x daisy-player

            else
                message_audio_pico "${message_cdrom_not_daisy}" "${langue_pico_accessibility_daisy}"
                # Lancement de Daisy-Player au cas ou il puisse ouvrir le CDROM
                # xfce4-terminal -x daisy-player
            fi

        else

            # Monte CDROM
            msg_mount_cdrom=$(udisksctl mount -b /dev/cdrom)
            #echo "msg_mount_cdrom=${msg_mount_cdrom}"

            if [[ $(echo ${msg_mount_cdrom} | grep "Mounted") ]] ; then

                mount_dir=$(echo ${msg_mount_cdrom} | cut -d " " -f4)
                #echo "mount_dir=${mount_dir}"

                # Test format Daisy
                if [[ $(ls ${mount_dir} | grep "ncc.html") ]] ; then

                    message_audio_pico "${message_cdrom_daisy}" "${langue_pico_accessibility_daisy}"
                    message_audio_pico "${message_start_daisy_player}" "${langue_pico_accessibility_daisy}"

                    xfce4-terminal -x daisy-player

                else
                    message_audio_pico "${message_cdrom_not_daisy}" "${langue_pico_accessibility_daisy}"
                    # Lancement de Daisy-Player au cas ou il puisse ouvrir le CDROM
                    # xfce4-terminal -x daisy-player
                fi

            else
                message_audio_pico "${message_cdrom_not_mounted}" "${langue_pico_accessibility_daisy}"
                # Lancement de Daisy-Player au cas ou il puisse ouvrir le CDROM
                # xfce4-terminal -x daisy-player
            fi

        fi

    else
        message_audio_pico "${message_cdrom_not_found}" "${langue_pico_accessibility_daisy}"
        # Lancement de Daisy-Player au cas ou il puisse ouvrir le CDROM
        # xfce4-terminal -x daisy-player
    fi

else

# Mode explorateur de fichier

    message_audio_pico "${message_start_daisy_player_script_explorer}" "${langue_pico_accessibility_daisy}"

    if [[ ${Library_daisy} != "" ]] ; then
        if test -d ${Library_daisy} ; then
            dir_daisy=${Library_daisy}
        fi
    fi

    WDIR=$(pwd)

    # Teste si le dossier contient des fichiers ZIP


    cd ${dir_daisy}
    file_ZIP=$(ls *.zip)
    number_file_ZIP=`echo ${file_ZIP} | wc -w`
    file_ZIP=`echo ${file_ZIP} | cut -d. -f1`

    if [[ ${file_ZIP} != "" ]] ; then

        if [ ${number_file_ZIP} -eq 1 ] ; then
            message_audio_pico "${message_one_file_found}" "${langue_pico_accessibility_daisy}"
            xfce4-terminal -x daisy-player ${dir_daisy}${file_ZIP}.zip
        else
            message_audio_pico "${number_file_ZIP} ${message_number_files_found}" "${langue_pico_accessibility_daisy}"
            message_audio_pico "${message_select_files}" "${langue_pico_accessibility_daisy}"
            xfce4-terminal -x ranger ${dir_daisy}
        fi

    else

        message_audio_pico "${message_file_not_found} ${dir_daisy}" "${langue_pico_accessibility_daisy}"

    fi

    cd ${WDIR}

fi

#! /bin/bash

# emmabuntus_accessibility_help.sh --
#
#   This file for functions of accessibility for the Emmabuntus Distrib.
#
#   Created on 2010-24 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 Xfce/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

###Pour exporter la librairie de gettext.
set -a
source gettext.sh
set +a
export TEXTDOMAIN=emmabuntus-accessibility
export TEXTDOMAINDIR="/usr/share/locale"
. gettext.sh

#  chargement des variables d'environnement
repertoire_emmabuntus=~/.config/emmabuntus
env_emmabuntus=${repertoire_emmabuntus}/env_emmabuntus
attente_lecture_message="1"

dir_help_file="/usr/share/Documentation_emmabuntus/Documents/Emmabuntus/"
help_visual_command="firefox"
help_audio_command="ebook-speaker"
help_visual_file=${dir_help_file}/Emmabuntus_Debian_Edition_5_accessibility.pdf
help_audio_file=${dir_help_file}/Emmabuntus_Debian_Edition_5_accessibility.epub
help_visual_file_fr=${dir_help_file}/Emmabuntus_Debian_Edition_5_accessibilite.pdf
help_audio_file_fr=${dir_help_file}/Emmabuntus_Debian_Edition_5_accessibilite.epub

manual_orca_visual_file=${dir_help_file}/Manual_Orca.pdf
manual_orca_audio_file=${dir_help_file}/Manual_Orca.epub
manual_orca_visual_file_fr=${dir_help_file}/Manuel_Orca.pdf
manual_orca_audio_file_fr=${dir_help_file}/Manuel_Orca.epub
first_steps_orca_visual_file=${dir_help_file}/Orca_first_steps.pdf
first_steps_orca_audio_file=${dir_help_file}/Orca_first_steps.epub
first_steps_orca_visual_file_fr=${dir_help_file}/Premiers_pas_Orca.pdf
first_steps_orca_audio_file_fr=${dir_help_file}/Premiers_pas_Orca.epub

. ${env_emmabuntus}

source /usr/bin/emmabuntus_accessibility_func.sh

langue_pico_help_accessibility_audio="$(eval_gettext '#langue_pico_help_accessibility_audio en-US')"
langue_pico_help_accessibility_audio=$(echo ${langue_pico_help_accessibility_audio} | cut -d " " -f 2)
if [[ ${langue_pico_help_accessibility_audio} == "en-US" && ${Language_pico} == "en-GB" ]] ; then langue_pico_help_accessibility_audio="en-GB" ; fi

message_file_not_found="$(eval_gettext 'Mode or file not found.')"

mode=$1
manual=$2

if [[ ${Language_pico} == "fr-FR" ]] ; then

    if [[ ${manual} == "manual" ]] ; then
        help_visual_file=${manual_orca_visual_file_fr}
        help_audio_file=${manual_orca_audio_file_fr}
    elif [[ ${manual} == "first_steps" ]] ; then
        help_visual_file=${first_steps_orca_visual_file_fr}
        help_audio_file=${first_steps_orca_audio_file_fr}
    else
        help_visual_file=${help_visual_file_fr}
        help_audio_file=${help_audio_file_fr}
    fi

elif [[ ${manual} == "manual" ]] ; then
        help_visual_file=${manual_orca_visual_file}
        help_audio_file=${manual_orca_audio_file}
elif [[ ${manual} == "first_steps" ]] ; then
    help_visual_file=${first_steps_orca_visual_file}
    help_audio_file=${first_steps_orca_audio_file}
fi

help_audio_option_command="-t cat eBook-speaker.txt | pico2wave -w eBook-speaker.wav -l ${langue_pico_help_accessibility_audio}"

escape_orca_read

if [[ ${mode} == "visual" && ( -f  ${help_visual_file} ) ]] ; then

    ${help_visual_command} ${help_visual_file}

elif [[ ${mode} == "audio" && ( -f  ${help_audio_file} ) ]] ; then

    if [[ $(ps -A | grep aplay) ]] ; then pkill aplay ; fi

    # Si Orca d'activé attendre x secondes avant de lire le message d'information sur la modification du son
    if [[ $(ps -A | grep orca) ]] ; then
        sleep ${attente_lecture_message}
    fi

    xterm -e "${help_audio_command}" "${help_audio_file}" "${help_audio_option_command}" &

else

    # Si Orca d'activé attendre x secondes avant de lire le message d'information sur la modification du son
    if [[ $(ps -A | grep orca) ]] ; then
        sleep ${attente_lecture_message}
    fi

     message_audio_pico "${message_file_not_found}" "${langue_pico_help_accessibility_audio}"

fi

#! /bin/bash

# emmabuntus_accessibility_window_audio.sh --
#
#   This file permits to prepare Accessibilty for the Emmabuntus Distrib.
#
#   Created on 2010-24 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 Xfce/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

###Pour exporter la librairie de gettext.
set -a
source gettext.sh
set +a
export TEXTDOMAIN=emmabuntus-accessibility
export TEXTDOMAINDIR="/usr/share/locale"
. gettext.sh

repertoire_emmabuntus=~/.config/emmabuntus
env_emmabuntus=${repertoire_emmabuntus}/env_emmabuntus
fichier_init_config=${repertoire_emmabuntus}/emmabuntus_config_accessibility_window.txt
espeak_voices_language_file=/usr/share/emmabuntus/espeak_voices_language.txt
lxkeymap_keyboard_language_file=/usr/share/emmabuntus/lxkeymap_keyboard_language.txt

#  chargement des variables d'environnement
. ${env_emmabuntus}

source /usr/bin/emmabuntus_accessibility_func.sh

langue_pico_accessibility_window_audio="$(eval_gettext '#langue_pico_accessibility_window_audio en-US')"
langue_pico_accessibility_window_audio=$(echo ${langue_pico_accessibility_window_audio} | cut -d " " -f 2)
if [[ ${langue_pico_accessibility_window_audio} == "en-US" && ${Language_pico} == "en-GB" ]] ; then langue_pico_accessibility_window_audio="en-GB" ; fi

# Touche valides : o = Orca, c = Config Orca, p : Langue Svox Pico, e : eSpeak, k : Langue eSpeak,
# t : Laptop, a : Audio info, w : Internet info, u : Update info, l : Autologin, x : set LxKeyMap, y :  info LxKeyMap
# Control + lettre D, E, I, F, U, G : Changer la langue de Svox Pico, Alt + première lettre du code langue : Changer la langue de eSpeak
# s : Status options, i : Lecture audio du tutoriel, h : Lecture audio des raccourcis, r : Relecture de l'aide,
# Espace : Arrêt lecture audio, Enter : Valider la fenêtre, q : Quitter la fenêtre

key="$(eval_gettext 'Key')"
function_on="$(eval_gettext 'enabled')"
function_off="$(eval_gettext 'disabled')"
langue_pico="$(eval_gettext 'Svox Pico language is')"
langue_pico_set="$(eval_gettext 'Svox Pico language setting')"
msg_espeak="$(eval_gettext 'speech synthesis eSpeak')"
langue_espeak="$(eval_gettext 'eSpeak language is')"
langue_espeak_set="$(eval_gettext 'eSpeak language is')"
shortcut_laptop="$(eval_gettext 'laptop shortcuts')"
msg_audio_info="$(eval_gettext 'audio information')"
web_info="$(eval_gettext 'Internet state information')"
update_info="$(eval_gettext 'Updates state')"
msg_autologin="$(eval_gettext 'auto login')"
msg_keyboard="$(eval_gettext 'LXkeymap keyboard language is')"
option_not_available_live="$(eval_gettext 'option not available in live mode')"
function_keyboard="$(eval_gettext 'Starting Orca to help you configure LxKeyMap, with Tab and Alt F4 to quit')"
function_status="$(eval_gettext 'Configuration state')"
function_help_audio="$(eval_gettext 'opening aid in audio mode')"
function_help_audio_shortcuts="$(eval_gettext 'opening shortcuts aid in audio mode')"
function_validation="$(eval_gettext 'configuration validation')"
function_not_available="$(eval_gettext 'function not available')"
message=""

re_is_integer='^[0-9]+$'

key_symbol=${1}
key_mod=${2}
orca_value=${3}
languagepico_value=${4}
enable_espeak_value=${5}
languageespeak_value=${6}
laptop_orca_value=${7}
audio_info_value=${8}
internet_info_value=${9}
update_info_value=${10}
autologin_value=${11}
compiz_value=${12}
dark_theme_value=${13}
postinstall_value=${14}
keyboard_value=${15}

key_symbol_maj=$(echo ${key_symbol} | tr [a-z] [A-Z])

if [[ ${key_mod} == "20" || ${key_mod} == "22" || ${key_mod} == "4" || ${key_mod} == "6" ]] ; then
    key_mod_symbol="Control"
elif [[ ${key_mod} == "24" || ${key_mod} == "26" || ${key_mod} == "8" || ${key_mod} == "10" ]] ; then
    key_mod_symbol="Alt"
else
    key_mod_symbol=""
fi

echo "key_symbol=${key_symbol} : key_mod=${2} : orca_value=${3} : languagepico_value=${4} : enable_espeak_value=${5} : languageespeak_value=${6} : laptop_orca_value=${7} : audio_info_value=${8} : internet_info_value=${9} : update_info_value=${10} : autologin_value=${11} : compiz_value=${12} : dark_theme_value=${13} : postinstall_value=${14} : keyboard_value=${15}" >> ${fichier_init_config}

message_audio_key () {

    function=${1}
    value=${2}
    status=${3}

    if [ "${value}" == "true" ] ; then
        variable="false"
        if [[ ${status} != "" ]] ; then
            message="${function} ${function_on}"
        else
            message="${key} ${key_symbol_maj} ${function} ${function_off}"
        fi
    else
        variable="true"
        if [[ ${status} != "" ]] ; then
            message="${function} ${function_off}"
        else
            message="${key} ${key_symbol_maj} ${function} ${function_on}"
        fi
    fi

    message_audio_pico "${message}" "${langue_pico_accessibility_window_audio}"

    echo "${variable}"

}

message_audio_language () {

    function=${1}
    value=${2}
    status=${3}

    langue_code=$(echo ${value} | cut -d - -f 1)
    langue_country=$(echo ${value} | cut -d - -f 2)

    if [[ $(echo ${function} | grep "Pico") ]] ; then

        case ${langue_code} in
            "de") langue="$(eval_gettext 'German')";;
            "es") langue="$(eval_gettext 'Spanish')";;
            "it") langue="$(eval_gettext 'Italian')";;
            "fr") langue="$(eval_gettext 'French')";;
            "en")  if [[ ${langue_country} == "US" ]] ; then  langue="$(eval_gettext 'American')" ; else  langue="$(eval_gettext 'English')" ; fi ;;
            *) langue="$(eval_gettext 'others')";;
        esac

    elif [[ $(echo ${function} | grep "eSpeak\|spik") ]] ; then

        espeak_voices_language=$(cat ${espeak_voices_language_file} | grep "^${value}")

        if [[ ${espeak_voices_language} != "" ]] ; then
            langue=${espeak_voices_language}
        else
            langue="$(eval_gettext 'others')"
        fi

    elif [[ $(echo ${function} | grep "LXkeymap") ]] ; then

        value=$(echo ${value} | cut -d "(" -f 1)

        lxkeymap_keyboard_language=$(cat ${lxkeymap_keyboard_language_file} | grep "^${value}")

        if [[ ${lxkeymap_keyboard_language} != "" ]] ; then
            langue=${lxkeymap_keyboard_language}
        else
            langue="$(eval_gettext 'others')"
        fi

    fi

    if [[ ${status} != "" ]] ; then
        message="${function} ${langue}"
    else
        message="${key} ${key_symbol_maj} ${function} ${langue}"
    fi

    message_audio_pico "${message}" "${langue_pico_accessibility_window_audio}"

}

message_audio_set_language () {

    function=${1}
    value=${2}

    case ${key_symbol_maj} in
        "D") langue="$(eval_gettext 'German')" ; variable="de-DE";;
        "E") langue="$(eval_gettext 'Spanish')"; variable="es-ES";;
        "I") langue="$(eval_gettext 'Italian')"; variable="it-IT";;
        "F") langue="$(eval_gettext 'French')" ; variable="fr-FR";;
        "U") langue="$(eval_gettext 'English')"; variable="en-US";;
        "G") langue="$(eval_gettext 'English')"; variable="en-GB";;
          *) langue="$(eval_gettext 'others')" ; variable="${value}";;
    esac

    message="${key} ${key_symbol_maj} ${function} ${langue}"

    message_audio_pico "${message}" "${langue_pico_accessibility_window_audio}"

    if [[ ${key_mod_symbol} == "Alt" ]] ; then
        variable=$(echo ${variable} | cut -d - -f 1)
    fi

    echo "${variable}"

}

if [[ ${key_mod_symbol} == "Control" ]] ; then

    LANGUE_PICO=$(message_audio_set_language "${langue_pico_set}" ${languagepico_value})

    pkill gtkdialog > /dev/null
    /usr/bin/emmabuntus_accessibility_window.sh

elif [[ ${key_mod_symbol} == "Alt" ]] ; then

    LANGUE_ESPEAK=$(message_audio_set_language "${langue_espeak_set}" ${languageespeak_value})

    pkill gtkdialog > /dev/null
    /usr/bin/emmabuntus_accessibility_window.sh

elif [[ ${key_symbol_maj} == "O" ]] ; then

    ORCA=$(message_audio_key "Orca" ${orca_value})

    pkill gtkdialog > /dev/null
    /usr/bin/emmabuntus_accessibility_window.sh

elif [[ ${key_symbol_maj} == "C" ]] ; then

    xdotool key Super+Alt+s

elif [[ ${key_symbol_maj} == "P" ]] ; then

    message_audio_language "${langue_pico}" ${languagepico_value}

elif [[ ${key_symbol_maj} == "E" ]] ; then

    ENABLE_ESPEAK=$(message_audio_key "${msg_espeak}" ${enable_espeak_value})

    pkill gtkdialog > /dev/null
    /usr/bin/emmabuntus_accessibility_window.sh

elif [[ ${key_symbol_maj} == "K" ]] ; then

    message_audio_language "${langue_espeak}" ${languageespeak_value}

elif [[ ${key_symbol_maj} == "T" ]] ; then

    LAPTOP_ORCA=$(message_audio_key "${shortcut_laptop}" ${laptop_orca_value})

    pkill gtkdialog > /dev/null
    /usr/bin/emmabuntus_accessibility_window.sh

elif [[ ${key_symbol_maj} == "A" ]] ; then

    AUDIO_INFO=$(message_audio_key "${msg_audio_info}" ${audio_info_value})

    pkill gtkdialog > /dev/null
    /usr/bin/emmabuntus_accessibility_window.sh

elif [[ ${key_symbol_maj} == "W" ]] ; then

    INTERNET_INFO=$(message_audio_key "${web_info}" ${internet_info_value})

    pkill gtkdialog > /dev/null
    /usr/bin/emmabuntus_accessibility_window.sh

elif [[ ${key_symbol_maj} == "U" ]] ; then

    UPDATE_INFO=$(message_audio_key "${update_info}" ${update_info_value})

    pkill gtkdialog > /dev/null
    /usr/bin/emmabuntus_accessibility_window.sh

elif [[ ${key_symbol_maj} == "Y" ]] ; then

    message_audio_language "${msg_keyboard}" ${keyboard_value}

elif [[ ${key_symbol_maj} == "L" ]] ; then

    if [[ $(cat /proc/cmdline | grep -i boot=live) == "" ]] ; then
        AUTOLOGIN=$(message_audio_key "${msg_autologin}" ${autologin_value})

        pkill gtkdialog > /dev/null
        /usr/bin/emmabuntus_accessibility_window.sh
    else
        message="${key} ${key_symbol_maj} ${option_not_available_live}"
        message_audio_pico "${message}" "${langue_pico_accessibility_window_audio}"
    fi

elif [[ ${key_symbol_maj} == "X" ]] ; then

    message="${key} ${key_symbol_maj} ${function_keyboard}"
    message_audio_pico "${message}" "${langue_pico_accessibility_window_audio}"

    orca --replace &

    sleep 1

    /usr/bin/lxkeymap
    KEYBOARD=`setxkbmap -v | awk -F "+" '/symbols/ {print $2}'`

    killall orca > /dev/null

    pkill gtkdialog > /dev/null
    /usr/bin/emmabuntus_accessibility_window.sh

elif [[ ${key_symbol_maj} == "S" ]] ; then

    message="${key} ${key_symbol_maj} ${function_status}"
    message_audio_pico "${message}" "${langue_pico_accessibility_window_audio}"

    message_audio_key "Orca" ${orca_value} "1"
    message_audio_language "${langue_pico}" ${languagepico_value} "1"
    message_audio_key "${msg_espeak}" ${enable_espeak_value} "1"
    message_audio_language "${langue_espeak}" ${languageespeak_value} "1"
    message_audio_key "${shortcut_laptop}" ${laptop_orca_value} "1"
    message_audio_key "${msg_audio_info}" ${audio_info_value} "1"
    message_audio_key "${web_info}" ${internet_info_value} "1"
    message_audio_key "${update_info}" ${update_info_value} "1"
    message_audio_language "${msg_keyboard}" ${keyboard_value} "1"

    if [[ $(cat /proc/cmdline | grep -i boot=live) == "" ]] ; then
        message_audio_key "${msg_autologin}" ${autologin_value} "1"
    fi

elif [[ ${key_symbol_maj} == "I" ]] ; then

    message="${key} ${key_symbol_maj} ${function_help_audio}"

    message_audio_pico "${message}" "${langue_pico_accessibility_window_audio}"
    /usr/bin/emmabuntus_accessibility_help.sh audio &

elif [[ ${key_symbol_maj} == "H" ]] ; then

    message="${key} ${key_symbol_maj} ${function_help_audio_shortcuts}"

    message_audio_pico "${message}" "${langue_pico_accessibility_window_audio}"
    /usr/bin/emmabuntus_accessibility_help_shortcuts.sh audio compact &

elif [[ ${key_symbol_maj} == "R" ]] ; then

    message_audio_pico "${message_accueil_accessibility_audio}" "${langue_pico_accessibility_window_audio}" &

elif [[ ${key_symbol} == "Escape" ]] ; then

    if [[ $(ps -A | grep aplay) ]] ; then pkill aplay > /dev/null ; fi

elif [[ ${key_symbol_maj} == "Q" ]] ; then

    message="${key} ${key_symbol_maj} Orca, Compiz ${function_off}"
    message_audio_pico "${message}" "${langue_pico_accessibility_window_audio}"

    pkill gtkdialog > /dev/null
    /usr/bin/emmabuntus_accessibility_window.sh &

    while true; do sleep 0.2; if [[ $(ps axg | grep -vw grep | grep -w gtkdialog) ]] ; then pkill gtkdialog; break; fi; done

elif [[ ${key_symbol} == "Return" ]] ; then

    update_config_emmabuntus Orca ${orca_value} ${env_emmabuntus}
    update_config_emmabuntus Compiz ${compiz_value} ${env_emmabuntus}

    update_config_emmabuntus Audio_info ${audio_info_value} ${env_emmabuntus}
    update_config_emmabuntus Internet_info ${internet_info_value} ${env_emmabuntus}
    update_config_emmabuntus Update_info ${update_info_value} ${env_emmabuntus}
    update_config_emmabuntus Dark_theme ${dark_theme_value} ${env_emmabuntus}
    update_config_emmabuntus Postinstall ${postinstall_value} ${env_emmabuntus}
    update_config_emmabuntus Autologin ${autologin_value} ${env_emmabuntus}
    update_config_emmabuntus Enable_espeak ${enable_espeak_value} ${env_emmabuntus}
    update_config_emmabuntus Laptop_orca ${laptop_orca_value} ${env_emmabuntus}
    update_laptop_orca ${laptop_orca_value}
    update_config_tuxtype ${languageespeak_value}
    update_config_ebook_speaker ${languageespeak_value} ${languagepico_value}
    update_config_emmabuntus Language_pico ${languagepico_value} ${env_emmabuntus}
    update_config_emmabuntus Language_espeak ${languageespeak_value} ${env_emmabuntus}

    if [[ ${enable_espeak_value} == "true" ]] ; then
        update_config_orca espeak ${languageespeak_value}
    else
        update_config_orca pico ${languagepico_value}
    fi

    message="${key} ${key_symbol} ${function_validation}"
    message_audio_pico "${message}" "${langue_pico_accessibility_window_audio}"
    pkill gtkdialog > /dev/null

# Filtage des nombres et du back utiliser pour renseigner la taille du curseur ou de la police
elif [[ ${key_symbol} =~ ${re_is_integer} || $(echo ${key_symbol} | grep -E '^KP_[0-9]+$') || ${key_symbol} == "BackSpace" || ${key_symbol} == "Shift_L" || ${key_symbol} == "Caps_Lock" ]] ; then

    echo ""

# Filtage Super+Alt+S utilisé pour lancer la config d'Orca
elif [[ ${key_symbol} == "Super_L" || ${key_symbol} == "Alt_L" || ${key_symbol_maj} == "S" ]] ; then

    echo ""

# Filtage Ctrl et Alt utilisé pour changer les langues de Pico et eSpeak
elif [[ ${key_symbol} == "Control_L" || ${key_symbol} == "Alt_L" ]] ; then

    echo ""

else

    message="${key} ${key_symbol_maj} ${function_not_available}"
    message_audio_pico "${message}" "${langue_pico_accessibility_window_audio}"

fi


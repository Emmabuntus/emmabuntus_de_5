#!/bin/bash

# Emmabuntus_config.sh --
#
#    This file permits to automatically postinstall softwares for Emmabuntüs.
#
#   Created on 2010-24 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 Xfce/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

. "$HOME/.config/user-dirs.dirs"

###Pour exporter la librairie de gettext.
set -a
source gettext.sh
set +a
export TEXTDOMAIN=emmabuntus
export TEXTDOMAINDIR="/usr/share/locale"
. gettext.sh
emmabuntus=$0


nom_distribution="Emmabuntüs Debian Edition 5"
nom_distribution_fr="Emmabuntüs Debian Édition 5"
repertoire_emmabuntus=~/.config/emmabuntus
env_emmabuntus=${repertoire_emmabuntus}/env_emmabuntus
fichier_init_config=${repertoire_emmabuntus}/init_emmabuntus_config_tmp.txt
fichier_init_config_final=${repertoire_emmabuntus}/init_emmabuntus_config.txt
fichier_affichage_config=${repertoire_emmabuntus}/display_emmabuntus_config.txt
fichier_init_lxqt=${repertoire_emmabuntus}/init_emmabuntus_lxqt.txt
repertoire_documentation="/usr/share/Documentation_emmabuntus"
file_conf_autologin="/etc/lightdm/lightdm.conf"

fond_ecran_en="framasoft_wallpaper_emmabuntus_classic"
fond_ecran_fr="framasoft_fond_ecran_accueil"

install_xfce_visible="true"
install_lxqt_visible="false"

# Détection du mode d'installation OEM direct et désactivation du lancement du script Emmabuntus_config
if [[ $(cat /proc/cmdline | grep -i oem) ]] ; then
    exit 10
fi


if [[ $(ps -A | grep "lxqt-session") ]] ; then
    repertoire_wallpaper_emmabuntus="/usr/share/lxqt/themes/emmabuntus"
else
    repertoire_wallpaper_emmabuntus="/usr/share/xfce4/backdrops"
fi

source /usr/bin/emmabuntus_found_theme.sh
highlight_color=$(FOUND_HIGHLIGHT_COLOR)

if ! test -d  ${repertoire_emmabuntus}; then mkdir ${repertoire_emmabuntus}; fi


bureau=${XDG_DESKTOP_DIR}

delai_fenetre=20

CST_RATIO_5_4=125    ; # 1280x1024
CST_RATIO_4_3=133    ; # valeur réelle = 1.333 ; # 1280x960
CST_RATIO_16_10=166  ; # 1280x800
CST_RATIO_16_9=177   ; # valeur réelle = 1.777 ; # 1280x720


#  chargement des variables d'environnement
. ${env_emmabuntus}


if [[ $LANG == fr* ]] ; then
    nom_distribution_mise_forme="\<b>\<i>$nom_distribution_fr\</i>\</b>"
else
    nom_distribution_mise_forme="\<b>\<i>$nom_distribution\</i>\</b>"
fi


DATE=`date +"%d:%m:%Y - %H:%M:%S"`

echo "DATE = $DATE" >> ${fichier_affichage_config}
echo "##########################################################" >> ${fichier_affichage_config}


# Détermination de la résolution de l'écran

x=`xwininfo -root | grep Width: |cut -d: -f 2`
y=`xwininfo -root | grep Height: |cut -d: -f 2`

ratio_ecran=$(($(( $x * 100 ))/$y))

if test $ratio_ecran -le $(( $(( $CST_RATIO_4_3 + $CST_RATIO_5_4 )) / 2 ))
then
   ecran="5_4"

elif test $ratio_ecran -le $(( $(( $CST_RATIO_16_10 + $CST_RATIO_4_3 )) / 2 ))
then
   ecran="4_3"

elif test $ratio_ecran -le $(( $(( $CST_RATIO_16_9 + $CST_RATIO_16_10 )) / 2 ))
then
   ecran="16_10"

elif test $ratio_ecran -ge $(( $(( $CST_RATIO_16_9 + $CST_RATIO_16_10 )) / 2 ))
then
   ecran="16_9"

else
   echo "Ecran résolution inconnue !!!" >> ${fichier_affichage_config}
   ecran="4_3"

fi

echo "Format de l'ecran = $ecran" >> ${fichier_affichage_config}


# Définition par défaut des messages en Anglais

message_accueil_live="\n\
$(eval_gettext 'Welcome on') ${nom_distribution_mise_forme}.\n\
\n\
$(eval_gettext 'Thanks for trying our distribution. We hope it will meet your expectations.')\n\
\n\
$(eval_gettext 'Numerous of accessibility functions are now embedded in Emmabuntüs,')\n\
$(eval_gettext 'particularly aiming at the visually impaired persons.')\n\
\n\
\<span color=\'${highlight_color}\'>$(eval_gettext 'Note:')\</span> $(eval_gettext 'With old or resource limited computers, we suggest you use')\n\
$(eval_gettext 'the LXQt desktop environment which is lighter than XFCE, the one installed by default.')\n\
\n\
$(eval_gettext 'During login, you can select your environment, at any time,')\n\
$(eval_gettext 'by clicking on the gear icon (at the top right of the screen).')\n\
\n\
\<b>$(eval_gettext 'If you want to use LXQt right now, select the left button.')\</b>\n"

message_accueil_install_lxqt="\n\
$(eval_gettext 'Welcome on') ${nom_distribution_mise_forme}.\n\
\n\
$(eval_gettext 'Thanks for having installed our distribution. We hope it will meet your expectations.')\n\
\n\
$(eval_gettext 'Numerous of accessibility functions are now embedded in Emmabuntüs,')\n\
$(eval_gettext 'particularly aiming at the visually impaired persons.')\n\
\n\
\<span color=\'${highlight_color}\'>$(eval_gettext 'Note:')\</span> $(eval_gettext 'Several post-installation windows are going to pop-up.')\n\
$(eval_gettext 'They allow you to configure your brand-new system.')\n"

# Test si utilisateur n'appartient pas au groupe sudo
if [[ $(groups | grep sudo) == "" ]] ; then
    switch_lxqt=""
else
    switch_lxqt=$(eval_gettext 'If you want to use LXQt right now, select the left button.')
fi
message_accueil_install_xfce="\n\
$(eval_gettext 'Welcome on') ${nom_distribution_mise_forme}.\n\
\n\
$(eval_gettext 'Thanks for having installed our distribution. We hope it will meet your expectations.')\n\
\n\
\<span color=\'${highlight_color}\'>1\</span> - $(eval_gettext 'Several post-installation windows are going to pop-up.')\n\
     $(eval_gettext 'They allow you to configure your brand-new system.')\n\
\<span color=\'${highlight_color}\'>2\</span> - $(eval_gettext 'Numerous of accessibility functions are now embedded in Emmabuntüs,')\n\
     $(eval_gettext 'particularly aiming at the visually impaired persons.')\n\
\<span color=\'${highlight_color}\'>3\</span> - $(eval_gettext 'With old or resource limited computers, we suggest you use')\n\
     $(eval_gettext 'the LXQt desktop environment which is lighter than XFCE, the one installed by default.')\n\
\n\
$(eval_gettext 'During login, you can select your environment, at any time,')\n\
$(eval_gettext 'by clicking on the gear icon (at the top right of the screen).')\n\
\n\
\<b>${switch_lxqt}\</b>\n"


msg_installation_configuration=$(eval_gettext 'Installation configuration for  ')


if [[ $(ps -A | grep "lxqt-session") ]] ; then
    message_accueil="${message_accueil_install_lxqt}"
    install_xfce_visible="false"
    install_lxqt_visible="true"
    install_switch_lxqt_visible="false"
else
    message_accueil="${message_accueil_install_xfce}"
    install_xfce_visible="true"
    install_lxqt_visible="false"
    # Test si utilisateur n'appartient pas au groupe sudo
    if [[ $(groups | grep sudo) == "" ]] ; then
        install_switch_lxqt_visible="false"
    else
        install_switch_lxqt_visible="true"
    fi
fi


# Personnalisation du fond d'écran en fonction de la langue
fond_ecran=${fond_ecran_en}_${ecran}


if [[ $LANG == fr* ]]
then
    fond_ecran=${fond_ecran_fr}_${ecran}
fi

wallpaper_file=${repertoire_wallpaper_emmabuntus}/${fond_ecran}.jpg
wallpaper_file_fr=${repertoire_wallpaper_emmabuntus}/${fond_ecran_fr}.jpg

cd ~

# Désactivation du pavé numérique, afin d'éviter certains soucis pour renseigner
# les mots de passe sur les ordinateurs portables

/usr/bin/laptop-detect
status=$?
if [ $status -eq 0 ]
then
    numlockx off
    echo "Numeric keypad Off" >> ${fichier_affichage_config}
elif [ $status -eq 1 ]
then
    if [[ ${Orca} == "0" ]] ; then
        numlockx on
        echo "Numeric keypad On" >> ${fichier_affichage_config}
    else
        numlockx off
        echo "Numeric keypad Off because Orca is actived" >> ${fichier_affichage_config}
    fi
else
    numlockx off
    echo "Failed to detect keypad type" >> ${fichier_affichage_config}
    echo "Return code = $status" >> ${fichier_affichage_config}
fi


# Désactivation du beep lors de la fenêtre de déconnexion
xset b off

/usr/bin/emmabuntus_config_desktop.sh

# Patch des sources.list en live
if [[ $(cat /proc/cmdline | grep -i boot=live) ]] ; then

    mod_file=/etc/apt/sources.list

    if test -f ${mod_file} ; then

        if [[ $(cat ${mod_file} | grep "^deb \[trusted=yes] file:/run/live/medium") ]] ; then

            echo "Patch ${mod_file}" >> ${fichier_affichage_config}

            sed s/"^deb\ \[trusted=yes] file:\/run\/live\/medium"/"#deb [trusted=yes] file:\/run\/live\/medium"/ ${mod_file} | sudo tee ${mod_file}.tmp > /dev/null
            sudo cp ${mod_file}.tmp ${mod_file}
        sudo rm ${mod_file}.tmp

        fi

    fi

fi

# Patch du GRUB en activant OS-prober
if [[ $(cat /proc/cmdline | grep -i boot=live) ]] ; then

    mod_file=/etc/default/grub

    if test -f ${mod_file} ; then

        echo "Patch ${mod_file}" >> ${fichier_affichage_config}

        sed s/"#GRUB_DISABLE_OS_PROBER[^$]*"/"GRUB_DISABLE_OS_PROBER=false"/ ${mod_file} | sudo tee ${mod_file}.tmp > /dev/null
        sudo cp ${mod_file}.tmp ${mod_file}
        sudo rm ${mod_file}.tmp

    fi

fi

# Activation de l'affichage des logiciels boot-repair et os-uninstaller
if [[ $(cat /proc/cmdline | grep -i boot=live) ]] ; then

    rep_applications=~/.local/share/applications

    mod_file=${rep_applications}/boot-repair.desktop
    if test -f ${mod_file} ; then
        sed s/"NoDisplay=true"/"NoDisplay=false"/ ${mod_file} > ${mod_file}.tmp
        cp ${mod_file}.tmp ${mod_file}
        rm ${mod_file}.tmp
    fi

    mod_file=${rep_applications}/os-uninstaller.desktop
    if test -f ${mod_file} ; then
        sed s/"NoDisplay=true"/"NoDisplay=false"/ ${mod_file} > ${mod_file}.tmp
        cp ${mod_file}.tmp ${mod_file}
        rm ${mod_file}.tmp
    fi

    rep_applications="/usr/share/applications"

    mod_file=${rep_applications}/boot-repair.desktop
    if test -f ${mod_file} ; then
        sed s/"NoDisplay=true"/"NoDisplay=false"/ ${mod_file} | sudo tee ${mod_file}.tmp > /dev/null
        sudo cp ${mod_file}.tmp ${mod_file}
        sudo rm ${mod_file}.tmp
    fi

    mod_file=${rep_applications}/os-uninstaller.desktop
    if test -f ${mod_file} ; then
        sed s/"NoDisplay=true"/"NoDisplay=false"/ ${mod_file} | sudo tee ${mod_file}.tmp > /dev/null
        sudo cp ${mod_file}.tmp ${mod_file}
        sudo rm ${mod_file}.tmp
    fi

else

    rep_applications=~/.config/cairo-dock-language

    for langue in fr en es it pt de da ar
    do

        mod_file=${rep_applications}/cairo-dock-${langue}/current_theme/launchers/01boot-repair.desktop
        if test -f ${mod_file} ; then rm ${mod_file} ; fi

        mod_file=${rep_applications}/cairo-dock-${langue}/current_theme/launchers/01os-uninstaller.desktop
        if test -f ${mod_file} ; then rm ${mod_file} ; fi

    done

fi

# Lancement du script pour mettre à jour les langues des synthèses vocales
/usr/bin/emmabuntus_accessibility_language.sh

# Lancement du script pour tester la présence Internet
/usr/bin/emmabuntus_accessibility_internet.sh "start"

# Lancement du script pour tester la présence de mise à jour
/usr/bin/emmabuntus_accessibility_update.sh &

if [[ ${Mousetweaks} == "1" ]] ; then

    if [[ $(ps -A | grep "mousetweaks") ]] ; then
        killall mousetweaks
    fi

    mousetweaks --dwell --dwell-time=${Mousetweaks_dwell_time} --threshold=${Mousetweaks_threshold} --geometry=${Mousetweaks_geometry} &
fi

#####################################################################################################################################
# Fonction opérationnelle

nornal_start()
{


if [[ $(ps -A | grep "lxqt-session") ]] ; then

    if [[ $(cat ~/.config/pcmanfm-qt/lxqt/settings.conf | grep -E "${fond_ecran_fr}|${fond_ecran_en}") ]] ; then

    # Test si l'écran est déjà pas la bonne version configurée
    if [[ ! $(cat ~/.config/pcmanfm-qt/lxqt/settings.conf | grep -E "${fond_ecran_fr}|${fond_ecran_en}" | grep ${wallpaper_file}) ]] ; then

        if test -f ${wallpaper_file} ; then
            pcmanfm-qt --set-wallpaper ${wallpaper_file}
            pcmanfm-qt --wallpaper-mode stretch
        else
            pcmanfm-qt --set-wallpaper ${wallpaper_file_fr}
            pcmanfm-qt --wallpaper-mode stretch
        fi
    fi

    fi

else

    if [[ $(xfconf-query -v -c xfce4-desktop -p "/backdrop/screen0/monitor0/last-image" | grep -E "${fond_ecran_fr}|${fond_ecran_en}") &&
          $(xfconf-query -v -c xfce4-desktop -p "/backdrop/screen0/monitor0/workspace0/last-image" | grep -E "${fond_ecran_fr}|${fond_ecran_en}") &&
          $(xfconf-query -v -c xfce4-desktop -p "/backdrop/screen0/monitor0/workspace1/last-image" | grep -E "${fond_ecran_fr}|${fond_ecran_en}") &&
          $(xfconf-query -v -c xfce4-desktop -p "/backdrop/screen0/monitor0/workspace2/last-image" | grep -E "${fond_ecran_fr}|${fond_ecran_en}") &&
          $(xfconf-query -v -c xfce4-desktop -p "/backdrop/screen0/monitor0/workspace3/last-image" | grep -E "${fond_ecran_fr}|${fond_ecran_en}") ]]
    then

        if test -f ${wallpaper_file} ; then
            wallpaper_file=${wallpaper_file}
        else
            wallpaper_file=${wallpaper_file_fr}
        fi

        wallpaper_list=$(xfconf-query -c xfce4-desktop -l | grep "image-path\|last-image\|last-single-image")

        for wallpaper_mod in ${wallpaper_list};
        do
            echo ${wallpaper_mod} >> ${fichier_affichage_config}
            xfconf-query -v -c xfce4-desktop -p ${wallpaper_mod} -s "${wallpaper_file}"
        done

        wallpaper_list=$(xfconf-query -c xfce4-desktop -l | grep "image-style")

        for wallpaper_mod in ${wallpaper_list};
        do
            echo ${wallpaper_mod} >> ${fichier_affichage_config}
            xfconf-query -v -c xfce4-desktop -p ${wallpaper_mod} -s "5"
        done

    fi

fi

if  ! [[ $(cat /proc/cmdline | grep -i boot=live) ]]
then

    if ! test -d ~/.mozilla ; then

        echo "# $msg_installation_configuration config user" >> ${fichier_init_config}

        # Installation de la configuration pour Thunderbird et les configurations les plus lourdes
        # afin de réduire l'utilisation mémoire en mode Live, ces configurations ont été mises dans le dossier /opt/User_Emmabuntus/
        cp -f -r /opt/User_Emmabuntus/. ~

    fi

else

    if ! test -d ~/.mozilla ; then
        echo "# $msg_installation_configuration Firefox" >> ${fichier_init_config}

        # En mode live copie que du répertoire .mozilla, afin accéler la démarrage en mode live
        cp -f -r /opt/User_Emmabuntus/.mozilla ~

    fi
fi

# Correction de l'utilisation du swap
/usr/bin/emmabuntus_fix_swap_use.sh
/usr/bin/emmabuntus_create_cache_apt.sh

if [[ $(ps -A | grep "xfce4-session") ]]
then

    echo "xfce4-session" >> ${fichier_affichage_config}

else

    pkill xfce4-notifyd
    pkill xfconfd
    pkill gvfs-afc-volume
    pkill gvfs-gphoto2-vo
    pkill wineserver
    pkill deja-dup-monito
    pkill hp-systray
    pkill indicator-bluet

fi

/usr/bin/calamares_oem_prepare.sh
/usr/bin/emmabuntus_config_firefox.sh
/usr/bin/emmabuntus_stat.sh

if [[ ${Orca} == "0" ]] ; then
    /usr/bin/emmabuntus_config_postinstall.sh
    /usr/bin/picosvox_config.sh
    /usr/bin/emmabuntus_config_dock.sh
fi

if [[ $(ps -A | grep "xfce4-session") ]]
then
    if [[ ( ${Dock_XFCE} == "1" && ${Orca} == "0" && ${Compiz} == "0" ) || ( ${Orca} == "1" && ${Orca_enable_cairodock} == "1" ) || ( ${Compiz} == "1" && ${Compiz_enable_cairodock} == "1" ) ]]
    then
    /usr/bin/start_cairo_dock.sh
    fi

elif [[ $(ps -A | grep "lxsession") ]]
then
    if [[ $Dock_LXDE == "1" ]]
    then
    /usr/bin/start_cairo_dock.sh
    fi
elif [[ $(ps -A | grep "openbox") ]]
then
    if [[ $Dock_OpenBox == "1" ]]
    then
    /usr/bin/start_cairo_dock.sh
    fi
elif [[ $(ps -A | grep "lxqt-session") ]]
then
    if [[ $Dock_LXQT == "1" ]]
    then
    /usr/bin/start_cairo_dock.sh
    fi
fi


if [[ $(cat /proc/cmdline | grep -i boot=live) ]]
then

    # Detection de la présence de l'adapateur Bluetooth
    bluetooth_present=$(hciconfig |grep Type |cut -d ':' -f1)

    if [ "$bluetooth_present" = "" ]; then
        echo "Bluetooth not found" >> ${fichier_affichage_config}
    else
        echo "Bluetooth found" >> ${fichier_affichage_config}

        if [[ ! $(ps -A | grep blueman-applet) ]] ; then
            echo "blueman-applet not active" >> ${fichier_affichage_config}
            blueman-applet &
        fi

    fi

else

    if [[ $Bluetooth == "1" ]]
    then

        if [[ ! $(ps -A | grep blueman-applet) ]] ; then
            echo "blueman-applet not active" >> ${fichier_affichage_config}
            blueman-applet &
        fi

    fi

fi

if [[ ${Orca} == "0" ]] ; then

    if  ! [[ $(cat /proc/cmdline | grep -i boot=live) ]] ; then
        /usr/bin/acceptation_install_logiciel_non_libre.sh
    fi

    /usr/bin/remove_language_not_use.sh
    /opt/ctparental/install_ctparental.sh
    /usr/bin/emmabuntus_welcome.sh
fi

}

#####################################################################################################################################
# Fonction post-installation

post_install()
{

echo "Initialisation" > ${fichier_init_config}
echo "DATE = $DATE" >> ${fichier_init_config}
echo "##########################################################" >> ${fichier_init_config}

# Ne pas afficher le lanceur d'installation de Calamares avant le lancement des fonctions d'accessibilité
if [[ $(cat /proc/cmdline | grep -i accessibility) ]] ; then
    file_desktop=${bureau}/install-debian.desktop

    if [[ -f ${file_desktop} ]] ; then
        rm ${file_desktop}
    fi
fi

if [[ $(cat /proc/cmdline | grep -i boot=live) ]]
then

    pkill package-update-

fi

if  ! [[ $(cat /proc/cmdline | grep -i boot=live) ]]
then

    if ! test -d ~/.mozilla ; then

        echo "# $msg_installation_configuration config user" >> ${fichier_init_config}

        # Installation de la configuration pour Thunderbird et les configurations les plus lourdes
        # afin de réduire l'utilisation mémoire en mode Live, ces configurations ont été mises dans le dossier /opt/User_Emmabuntus/
        cp -f -r /opt/User_Emmabuntus/. ~

    fi

else

    if ! test -d ~/.mozilla ; then
        echo "# $msg_installation_configuration Firefox" >> ${fichier_init_config}

        # En mode live copie que du répertoire .mozilla, afin accéler la démarrage en mode live
        cp -f -r /opt/User_Emmabuntus/.mozilla ~

    fi
fi

# Lancement du script pour ajouter les raccourcis
/usr/bin/emmabuntus_accessibility_xfce_shortcuts.sh

# Lancement de la fonction accessibilité
if [[ $(cat /proc/cmdline | grep -i accessibility) || $(gsettings get org.gnome.desktop.a11y.applications screen-keyboard-enabled) ]] ; then

    /usr/bin/emmabuntus_accessibility.sh

    # Si la fonction ne retourne pas 0, on sort de ce script sinon on poursuit le script
    if [ "$?" -ne 0 ]; then

        /usr/bin/emmabuntus_config_firefox.sh
        /usr/bin/emmabuntus_stat.sh

        exit 20

    fi

fi


# Mise en place du lanceur de Calamares
if [[ $(cat /proc/cmdline | grep -i boot=live) ]]
then

    file_desktop=${bureau}/install-debian.desktop

    if [[ -f ${file_desktop} ]] ; then
        rm ${file_desktop}
    fi

    cp -f /opt/calamares/install-debian-classic.desktop ${file_desktop}
    chmod a+x ${file_desktop}

    # Pour autoriser le lancement de Calamares sur le bureau pour LXQt
    gio set ${file_desktop} -t string metadata::trust "true"

    # Pour autoriser le lancement de Calamares sur le bureau pour Xfce et LXQt
    gio set -t string ${file_desktop} metadata::xfce-exe-checksum "$(sha256sum ${file_desktop} | awk '{print $1}')"

    if [[ $(ps -A | grep "xfce4-session") ]] ; then

        if [[ $(ps -A | grep xfce4-panel) ]] ; then
            xfce4-panel --restart
        fi

        sleep 1

        if [[ $(ps -A | grep xfce4-panel) ]] ; then
            echo "" >> ${fichier_affichage_config}
        else
            xfce4-panel &
        fi

    fi

else

    /usr/bin/calamares_oem_prepare.sh

fi

if [[ $(cat /proc/cmdline | grep -i boot=live) ]]
then

    echo "Live mode" >> ${fichier_affichage_config}

    if ! test -f ${fichier_init_lxqt} ; then

    echo "Test basculement sous LXQt en Live" > ${fichier_init_lxqt}
    echo "DATE = $DATE" >> ${fichier_init_lxqt}

    export WINDOW_DIALOG_WELCOME='<window title="'${nom_distribution}'" icon-name="gtk-info" resizable="false">
    <vbox spacing="0">

    <text use-markup="true" wrap="false" xalign="0" justify="3">
    <input>echo "'$message_accueil_live'" | sed "s%\\\%%g"</input>
    </text>

    <hbox spacing="10" space-expand="false" space-fill="false">
    <button>
    <label>"'$(eval_gettext 'Switch to LXQt  ')'"</label>
    <input file stock="gtk-execute"></input>
    <action>exit:OK</action>
    </button>

    <button>
    <label>"'$(eval_gettext 'Setting up accessibility')'"</label>
    <input file stock="gtk-info"></input>
    <action>exit:Accessibility</action>
    </button>

    <button can-default="true" has-default="true" use-stock="true" is-focus="true">
    <label>"'$(eval_gettext 'Continue under XFCE  ')'"</label>
    <input file stock="gtk-ok"></input>
    <action>exit:Cancel</action>
    </button>

    </hbox>

    </vbox>
    </window>'

    MENU_DIALOG_WELCOME="$(gtkdialog --center --program=WINDOW_DIALOG_WELCOME)"
    eval ${MENU_DIALOG_WELCOME}
    echo "MENU_DIALOG_WELCOME=${MENU_DIALOG_WELCOME}" >> ${fichier_affichage_config}

        if [ ${EXIT} == "OK" ] ; then

            echo "Basculement sous LXQt en Live" >> ${fichier_init_lxqt}
            echo "DATE = $DATE" >> ${fichier_init_lxqt}

            pkexec /usr/bin/emmabuntus_switch_lxqt.sh "${USER}"

        elif [ ${EXIT} == "Accessibility" ] ; then

            /usr/bin/emmabuntus_accessibility.sh "Init"

            # Si la fonction ne retourne pas 0, on sort de ce script sinon on poursuit le script
            if [ "$?" -ne 0 ]; then

                /usr/bin/emmabuntus_config_firefox.sh
                /usr/bin/emmabuntus_stat.sh

                exit 20

            fi

        fi

    fi

else

    if ! test -f ${fichier_init_lxqt} ; then

    echo "Test basculement sous LXQt en Install" > ${fichier_init_lxqt}
    echo "DATE = $DATE" >> ${fichier_init_lxqt}

    export WINDOW_DIALOG_WELCOME='<window title="'${nom_distribution}'" icon-name="gtk-info" resizable="false">
    <vbox spacing="0">

    <text use-markup="true" wrap="false" xalign="0" justify="3">
    <input>echo "'$message_accueil'" | sed "s%\\\%%g"</input>
    </text>

    <hbox spacing="10" space-expand="false" space-fill="false">

    <button visible="'${install_switch_lxqt_visible}'">
    <label>"'$(eval_gettext 'Switch to LXQt  ')'"</label>
    <input file stock="gtk-execute"></input>
    <action>exit:OK</action>
    </button>
    <button>
    <label>"'$(eval_gettext 'Setting up accessibility')'"</label>
    <input file stock="gtk-info"></input>
    <action>exit:Accessibility</action>
    </button>
    <button can-default="true" has-default="true" use-stock="true" is-focus="true" visible="'${install_xfce_visible}'">
    <label>"'$(eval_gettext 'Continue under XFCE  ')'"</label>
    <input file stock="gtk-ok"></input>
    <action>exit:Cancel</action>
    </button>

    <button can-default="true" has-default="true" use-stock="true" is-focus="true" visible="'${install_lxqt_visible}'">
    <label>gtk-ok</label>
    <action>exit:Cancel</action>
    </button>
    </hbox>

    </vbox>
    </window>'

    MENU_DIALOG_WELCOME="$(gtkdialog --center --program=WINDOW_DIALOG_WELCOME)"
    eval ${MENU_DIALOG_WELCOME}

    echo "MENU_DIALOG_WELCOME=${MENU_DIALOG_WELCOME}" >> ${fichier_affichage_config}

        if [ ${EXIT} == "Accessibility" ] ; then

            /usr/bin/emmabuntus_accessibility.sh "Init"

            # Si la fonction ne retourne pas 0, on sort de ce script sinon on poursuit le script
            if [ "$?" -ne 0 ]; then

                /usr/bin/emmabuntus_config_firefox.sh
                /usr/bin/emmabuntus_stat.sh

                exit 20

            fi

        elif [ ${EXIT} == "OK" ] ; then

            echo "Basculement sous LXQt en Install" >> ${fichier_init_lxqt}
            echo "DATE = $DATE" >> ${fichier_init_lxqt}

            if [[ $(cat ${file_conf_autologin} | grep "^autologin-user=${USER}") ]] ; then

                echo "Autologin activé" >> ${fichier_init_lxqt}

            else

                echo "Autologin désactivé" >> ${fichier_init_lxqt}

                export WINDOW_DIALOG_WELCOME='<window title="'${nom_distribution}'" icon-name="gtk-info" resizable="false">
                <vbox spacing="0">

                <text use-markup="true" wrap="false" xalign="0" justify="3">
                <input>echo "\n'$(eval_gettext 'You will be redirected to the login screen.')'\n'$(eval_gettext 'Please enter your password to switch to LXQt.')'\n" | sed "s%\\\%%g"</input>
                </text>

                <hbox spacing="10" space-expand="false" space-fill="false">
                <button can-default="true" has-default="true" use-stock="true" is-focus="true">
                <label>gtk-ok</label>
                <action>exit:OK</action>
                </button>
                </hbox>

                </vbox>
                </window>'

                MENU_DIALOG_WELCOME="$(gtkdialog --center --program=WINDOW_DIALOG_WELCOME)"

            fi

            pkexec /usr/bin/emmabuntus_switch_lxqt.sh "${USER}"

        fi

    fi

fi

# Correction de l'utilisation du swap
/usr/bin/emmabuntus_fix_swap_use.sh
/usr/bin/emmabuntus_create_cache_apt.sh

# Mise en place des liens vers le répertoire de culture Libre (Wikipédia Off-line, Musique Libre, eBooks)
if /sbin/blkid -L FREE_CULTURE
then

    if [[ $(cat /proc/cmdline | grep -i boot=live) ]]
    then

        zenity --question --title="$nom_distribution" --no-wrap --width=500  --text="$(eval_gettext 'Do you want to access to Free Culture documents on your USB drive?')" \
        --ok-label="${zenity_ok_label}" --cancel-label="${zenity_cancel_label}"

        choix_install_free_culture=$?

    else

        zenity --question --title="$nom_distribution" --no-wrap --width=500  --text="$(eval_gettext 'Do you want to install the Free Culture documents from your USB drive to your hard drive ?')" \
        --ok-label="${zenity_ok_label}" --cancel-label="${zenity_cancel_label}"

        choix_install_free_culture=$?

    fi

    if [ $choix_install_free_culture = "0" ]
    then

        /usr/bin/start_install_free_culture.sh

    fi

fi


# Suppresion du lanceur Flatpak en mode live
# car il n'est pas possible d'installer ce type de paquet en mode live
if [[ $(cat /proc/cmdline | grep -i boot=live) ]]
then

    sudo rm -f /usr/share/applications/flatpak.desktop

    rm ~/.config/cairo-dock-language/cairo-dock-ar/current_theme/launchers/01flatpak.desktop
    rm ~/.config/cairo-dock-language/cairo-dock-de/current_theme/launchers/01flatpak.desktop
    rm ~/.config/cairo-dock-language/cairo-dock-en/current_theme/launchers/01flatpak.desktop
    rm ~/.config/cairo-dock-language/cairo-dock-es/current_theme/launchers/01flatpak.desktop
    rm ~/.config/cairo-dock-language/cairo-dock-fr/current_theme/launchers/01flatpak.desktop
    rm ~/.config/cairo-dock-language/cairo-dock-it/current_theme/launchers/01flatpak.desktop
    rm ~/.config/cairo-dock-language/cairo-dock-pt/current_theme/launchers/01flatpak.desktop

fi


echo "# $(eval_gettext 'Changing the wallpaper')"  >> ${fichier_init_config}

echo "# wallpaper_file = $(eval_gettext 'Changing the wallpaper')" >> ${fichier_init_config}

if [[ $(ps -A | grep "lxqt-session") ]] ; then

    if [[ $(cat ~/.config/pcmanfm-qt/lxqt/settings.conf | grep -E "${fond_ecran_fr}|${fond_ecran_en}") ]] ; then

    # Test si l'écran est déjà pas la bonne version configurée
    if [[ ! $(cat ~/.config/pcmanfm-qt/lxqt/settings.conf | grep -E "${fond_ecran_fr}|${fond_ecran_en}" | grep ${wallpaper_file}) ]] ; then

        if test -f ${wallpaper_file} ; then
            pcmanfm-qt --set-wallpaper ${wallpaper_file}
            pcmanfm-qt --wallpaper-mode stretch
        else
            pcmanfm-qt --set-wallpaper ${wallpaper_file_fr}
            pcmanfm-qt --wallpaper-mode stretch
        fi
    fi

    fi

else

    if test -f ${wallpaper_file} ; then
        wallpaper_file=${wallpaper_file}
    else
        wallpaper_file=${wallpaper_file_fr}
    fi

    wallpaper_list=$(xfconf-query -c xfce4-desktop -l | grep "image-path\|last-image\|last-single-image")

    for wallpaper_mod in ${wallpaper_list};
    do
        echo ${wallpaper_mod} >> ${fichier_affichage_config}
        xfconf-query -v -c xfce4-desktop -p ${wallpaper_mod} -s "${wallpaper_file}"
    done

    wallpaper_list=$(xfconf-query -c xfce4-desktop -l | grep "image-style")

    for wallpaper_mod in ${wallpaper_list};
    do
        echo ${wallpaper_mod} >> ${fichier_affichage_config}
        xfconf-query -v -c xfce4-desktop -p ${wallpaper_mod} -s "5"
    done

fi


if [[ $(cat /proc/cmdline | grep -i boot=live) ]]
then

    /usr/bin/picosvox_config.sh
    /usr/bin/emmabuntus_config_firefox.sh
    /usr/bin/emmabuntus_stat.sh
    /usr/bin/emmabuntus_choose_keyboard.sh
    /usr/bin/emmabuntus_config_dock.sh

    #  chargement des variables d'environnement
    . ${env_emmabuntus}

    if test -f ~/.config/cairo-dock-language/cairo-dock-choise-dock.conf
    then
        rm ~/.config/cairo-dock-language/cairo-dock-choise-dock.conf
    fi

    if [[ $(ps -A | grep "xfce4-session") ]]
    then
        if [[ ( ${Dock_XFCE} == "1" && ${Orca} == "0" && ${Compiz} == "0" ) || ( ${Orca} == "1" && ${Orca_enable_cairodock} == "1" ) || ( ${Compiz} == "1" && ${Compiz_enable_cairodock} == "1" ) ]]
        then
            /usr/bin/start_cairo_dock.sh
        else
            xfconf-query -v -c xfce4-panel -p /plugins/plugin-9 -s "tasklist"
            xfconf-query -v -c xfce4-panel -p /plugins/plugin-9/grouping -n -t bool -s true
            xfconf-query -v -c xfce4-panel -p /plugins/plugin-10 -s "showdesktop"
            xfconf-query -v -c xfce4-panel -p /plugins/plugin-11 -s "pager"

            if [[ $(ps -A | grep xfce4-panel) ]]
            then
                xfce4-panel --restart
            fi
            sleep 1
            if [[ $(ps -A | grep xfce4-panel) ]]
            then
                echo "" >> ${fichier_affichage_config}
            else
                xfce4-panel &
            fi
        fi

    elif [[ $(ps -A | grep "lxsession") ]]
    then
        if [[ $Dock_LXDE == "1" ]]
        then
        lxpanelctl exit
        lxpanel --profile LXDE_Dock &
        /usr/bin/start_cairo_dock.sh
        fi
    elif [[ $(ps -A | grep "openbox") ]]
    then
        if [[ $Dock_OpenBox == "1" ]]
        then
        /usr/bin/start_cairo_dock.sh
        fi
    elif [[ $(ps -A | grep "lxqt-session") ]]
    then
        if [[ $Dock_LXQT == "1" ]]
        then
        /usr/bin/start_cairo_dock.sh
        fi

    fi

    # Detection de la présence de l'adapateur Bluetooth
    bluetooth_present=$(hciconfig |grep Type |cut -d ':' -f1)

    if [ "$bluetooth_present" = "" ]; then
        echo "Bluetooth not found" >> ${fichier_affichage_config}
    else
        echo "Bluetooth found" >> ${fichier_affichage_config}

        if [[ ! $(ps -A | grep blueman-applet) ]] ; then
            echo "blueman-applet not active" >> ${fichier_affichage_config}
            blueman-applet &
        fi

    fi

    /usr/bin/emmabuntus_welcome.sh Init

else

    /usr/bin/emmabuntus_config_firefox.sh
    /usr/bin/emmabuntus_stat.sh

    if [[ ${Orca} == "0" ]] ; then
        /usr/bin/emmabuntus_config_postinstall.sh
        /usr/bin/picosvox_config.sh
        /usr/bin/emmabuntus_config_dock.sh
    fi

    #  chargement des variables d'environnement
    . ${env_emmabuntus}

    if test -f ~/.config/cairo-dock-language/cairo-dock-choise-dock.conf
    then
        rm ~/.config/cairo-dock-language/cairo-dock-choise-dock.conf
    fi

    if [[ $(ps -A | grep "xfce4-session") ]]
    then
        if [[ ( ${Dock_XFCE} == "1" && ${Orca} == "0" && ${Compiz} == "0" ) || ( ${Orca} == "1" && ${Orca_enable_cairodock} == "1" ) || ( ${Compiz} == "1" && ${Compiz_enable_cairodock} == "1" )  ]]
        then
            /usr/bin/start_cairo_dock.sh
        else
            xfconf-query -v -c xfce4-panel -p /plugins/plugin-9 -s "tasklist"
            xfconf-query -v -c xfce4-panel -p /plugins/plugin-9/grouping -n -t bool -s true
            xfconf-query -v -c xfce4-panel -p /plugins/plugin-10 -s "showdesktop"
            xfconf-query -v -c xfce4-panel -p /plugins/plugin-11 -s "pager"

            if [[ $(ps -A | grep xfce4-panel) ]]
            then
                xfce4-panel --restart
            fi
            sleep 1
            if [[ $(ps -A | grep xfce4-panel) ]]
            then
                echo "" >> ${fichier_affichage_config}
            else
                xfce4-panel &
            fi
        fi

    elif [[ $(ps -A | grep "lxsession") ]]
    then
        if [[ $Dock_LXDE == "1" ]]
        then
        lxpanelctl exit
        lxpanel --profile LXDE_Dock &
        /usr/bin/start_cairo_dock.sh
        fi
    elif [[ $(ps -A | grep "openbox") ]]
    then
        if [[ $Dock_OpenBox == "1" ]]
        then
        /usr/bin/start_cairo_dock.sh
        fi
    elif [[ $(ps -A | grep "lxqt-session") ]]
    then
        if [[ $Dock_LXQT == "1" ]]
        then
        /usr/bin/start_cairo_dock.sh
        fi
    fi

    if [[ ${Orca} == "0" ]] ; then
        /usr/bin/acceptation_install_logiciel_non_libre.sh
        /usr/bin/remove_language_not_use.sh
        /opt/ctparental/install_ctparental.sh
        /usr/bin/emmabuntus_welcome.sh Init
    fi

fi

cp -f ${fichier_init_config} ${fichier_init_config_final}

}

#####################################################################################################################################

# Corrige le fait que la base de données pour command-not-found n'est pas en lecture suite à la config non mise en lecture pour le groupe other
if [ ! -r /var/lib/command-not-found/commands.db ] || [ ! -r /var/lib/command-not-found/commands.db.metadata ]
then
    pkexec /usr/bin/emmabuntus_command_not_found_readable_exec.sh
fi


if test -f ${fichier_init_config_final}
then

    nornal_start

else

    post_install


fi



#! /bin/bash

# emmabuntus_accessibility_xfce_shortcuts.sh --
#
#   This file modify shortcuts for accessibility on the Emmabuntus Distrib.
#
#   Created on 2010-24 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 Xfce/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

# Affiche tous les raccourcis
# xfconf-query -c xfce4-keyboard-shortcuts -l -v

# Recherche si un raccourci est affecté
# shortcut="<Super>e"
# xfconf-query -c xfce4-keyboard-shortcuts -l -v | grep -i ${shortcut}

repertoire_emmabuntus=~/.config/emmabuntus
env_emmabuntus=${repertoire_emmabuntus}/env_emmabuntus
fichier_init_config=${repertoire_emmabuntus}/init_emmabuntus_accessibility_xfce_shortcuts.txt

# Test si session sous Xfce
if [[ $(ps -A | grep "xfce4-session") ]] ; then

# Commandes possibles : --force, --test
command=$1

if [[ ( ${command} == "--force" || ${command} == "--test" || ! ( -f  ${fichier_init_config} ) ) && $( ps -A | grep xfce4-panel ) ]] ; then

max_shortcut=58

shortcuts_key=(

                    [0]='<Alt><Shift>Home'                                                               [1]='<Alt><Shift>Up'
                    [2]='<Alt><Shift>Down'                                                               [3]='<Alt><Shift>Left'
                    [4]='<Alt><Shift>Right'                                                              [5]='<Alt><Shift>End'
                    [6]='<Alt><Super>o'                                                                  [7]='<Alt>b'
                    [8]='<Alt><Super>s'                                                                  [9]='<Shift>F2'

                   [10]='<Alt><Super>c'                                                                 [11]='<Alt><Super>a'
                   [12]='<Alt>r'                                                                        [13]='<Alt>s'
                   [14]='<Alt>m'                                                                        [15]='<Super>o'

                   [16]='<Super>c'                                                                      [17]='<Alt><Super>e'
                   [18]='<Alt><Super>t'                                                                 [19]='<Alt><Super>b'
                   [20]='<Super>v'                                                                      [21]='<Alt><Super>i'
                   [22]='<Alt><Super>g'                                                                 [23]='<Alt><Super>r'

                   [24]='<Alt><Super>Up'                                                                [25]='<Alt><Super>Down'
                   [26]='<Alt><Super>Right'                                                             [27]='<Alt><Super>Left'
                   [28]='<Primary><Super>Left'                                                          [29]='<Primary><Super>Right'
                   [30]='<Primary><Super>Page_Up'                                                       [31]='<Alt><Super>w'
                   [32]='<Alt><Super>n'                                                                 [33]='<Alt><Super>u'

                   [34]='<Alt><Super>x'                                                                 [35]='<Alt><Super>h'
                   [36]='<Alt><Super>v'                                                                 [37]='<Ctrl><Super>s'
                   [38]='<Ctrl><Super>v'                                                                [39]='<Alt><Super>l'

                   [40]='<Alt><Super>p'                                                                 [41]='<Alt><Super>k'
                   [42]='<Alt><Super>y'                                                                 [43]='<Ctrl><Alt><Super>s'
                   [44]='<Ctrl><Alt><Super>v'                                                           [45]='<Alt><Super>m'
                   [46]='<Ctrl><Alt><Super>y'                                                           [47]='<Alt><Super><Shift>h'
                   [48]='<Alt><Super><Shift>v'                                                          [49]='<Alt>k'
                   [50]='<Ctrl><Super>c'                                                                [51]='<Ctrl><Super>e'
                   [52]='<Ctrl><Super>k'                                                                [53]='<Ctrl><Super><Shift>h'
                   [54]='<Ctrl><Super><Shift>v'                                                         [55]='<Ctrl><Super>m'
                   [56]='<Ctrl><Super>d'                                                                [57]='<Ctrl><Alt><Super>d'

                   )


shortcuts_command=(

                    [0]='/usr/bin/emmabuntus_accessibility_audio.sh ""'                                  [1]='/usr/bin/emmabuntus_accessibility_audio.sh "+"'
                    [2]='/usr/bin/emmabuntus_accessibility_audio.sh "-"'                                 [3]='/usr/bin/emmabuntus_accessibility_audio.sh "off"'
                    [4]='/usr/bin/emmabuntus_accessibility_audio.sh "on"'                                [5]='/usr/bin/emmabuntus_accessibility_audio.sh "test"'
                    [6]='orca --replace'                                                                 [7]='play /usr/share/sounds/purple/alert.wav'
                    [8]='orca --replace --setup'                                                         [9]='orca --replace'

                   [10]='compiz ccp --replace'                                                          [11]='/usr/bin/emmabuntus_accessibility.sh "Init"'
                   [12]='xfce4-session-logout --reboot'                                                 [13]='xfce4-session-logout --halt'
                   [14]='wmctrl -k on'                                                                  [15]='libreoffice --writer'

                   [16]='calibre'                                                                       [17]='/usr/bin/emmabuntus_elograf_on_off.sh'
                   [18]='/usr/bin/tuxtype_fix_speech_dispatcher.sh'                                     [19]='xfce4-terminal --fullscreen -x ebook-speaker'
                   [20]='/usr/bin/virtual_magnifier_glass.sh'                                           [21]='/usr/bin/natbraille -gui'
                   [22]='geany'                                                                         [23]='/usr/bin/ocrizer.sh'

                   [24]='/usr/bin/emmabuntus_accessibility_audio.sh "micro_₊"'                          [25]='/usr/bin/emmabuntus_accessibility_audio.sh "micro_₋"'
                   [26]='/usr/bin/emmabuntus_accessibility_audio.sh "micro_on"'                         [27]='/usr/bin/emmabuntus_accessibility_audio.sh "micro_off"'
                   [28]='/usr/bin/emmabuntus_accessibility_audio.sh "audio_off_micro_on"'               [29]='/usr/bin/emmabuntus_accessibility_audio.sh "audio_on_micro_off"'
                   [30]='/usr/bin/emmabuntus_accessibility_audio.sh "audio_on_micro_on"'                [31]='/usr/bin/emmabuntus_accessibility_dwservice.sh'
                   [32]='xfce4-terminal --fullscreen -e nmtui'                                          [33]='/usr/bin/emmabuntus_accessibility_system_update.sh'

                   [34]='/usr/bin/emmabuntus_accessibility_internet.sh "test"'                          [35]='/usr/bin/emmabuntus_accessibility_help.sh "audio"'
                   [36]='/usr/bin/emmabuntus_accessibility_help.sh "visual"'                            [37]='/usr/bin/emmabuntus_accessibility_help_shortcuts.sh "audio" "compact"'
                   [38]='/usr/bin/emmabuntus_accessibility_help_shortcuts.sh "visual" "compact"'        [39]='/usr/bin/emmabuntus_accessibility_ebooks.sh "audio"'

                   [40]='/usr/bin/emmabuntus_accessibility_ebooks.sh "visual"'                          [41]='/usr/bin/emmabuntus_accessibility_kiwix.sh'
                   [42]='/usr/bin/emmabuntus_accessibility_capture_picture.sh'                          [43]='/usr/bin/emmabuntus_accessibility_help_shortcuts.sh "audio"'
                   [44]='/usr/bin/emmabuntus_accessibility_help_shortcuts.sh "visual"'                  [45]='/usr/bin/emmabuntus_accessibility_state_keys.sh'
                   [46]='/usr/bin/emmabuntus_accessibility_capture_picture.sh "no"'                     [47]='/usr/bin/emmabuntus_accessibility_help.sh "audio" "manual"'
                   [48]='/usr/bin/emmabuntus_accessibility_help.sh "visual" "manual"'                   [49]='xfce4-find-cursor'
                   [50]='/usr/bin/emmabuntus_accessibility_gnome_calculator.sh'                         [51]='evince'
                   [52]='xfce4-terminal --fullscreen -e /usr/bin/emmabuntus_accessibility_ventoy.sh'    [53]='/usr/bin/emmabuntus_accessibility_help.sh "audio" "first_steps"'
                   [54]='/usr/bin/emmabuntus_accessibility_help.sh "visual" "first_steps"'              [55]='clementine'
                   [56]='/usr/bin/emmabuntus_accessibility_daisy_player.sh'                             [57]='/usr/bin/emmabuntus_accessibility_daisy_player.sh "CDROM"'

                  )


shortcut_key_present=""
shortcut_key=""
shortcut_command=""
shortcut_base="/commands/custom/"
delay_test="5"


if [[ ${command} == "" ]] ; then

    DATE=`date +"%d:%m:%Y - %H:%M:%S"`
    echo "DATE = $DATE" >> ${fichier_init_config}

fi


for (( i=0; i<=${max_shortcut}-1; i++ ))
do

    if [[ ${shortcuts_key[$i]} != "" ]] ; then

        shortcut_key_present=$(xfconf-query -c xfce4-keyboard-shortcuts -l -v | grep -i "${shortcut_base}${shortcuts_key[$i]} ")

        if [[ ${shortcut_key_present} != "" ]] ; then
            shortcut_key_present_command=$(xfconf-query -c xfce4-keyboard-shortcuts -p "${shortcut_base}${shortcuts_key[$i]}" -v)
        else
            shortcut_key_present_command=""
        fi

        shortcut_key=${shortcuts_key[$i]}
        shortcut_command=${shortcuts_command[$i]}

        #echo "$i"
        #echo "${shortcut_key}"
        #echo "${shortcut_command}"
        #echo "Shortcut key present : ${shortcut_key_present}"

        if [[ ${command} == "--test" ]] ; then

            if [[ ${shortcut_key_present} != "" || ${shortcut_key_present_command} != "" ]] ; then

                echo "Shortcut key present :  $i : ${shortcut_key_present} : ${shortcut_key_present_command}"

            else

                shortcut_key_xdotool=${shortcut_key//</}
                shortcut_key_xdotool=${shortcut_key_xdotool//>/+}
                shortcut_key_xdotool=${shortcut_key_xdotool//Primary/Ctrl}

                if [[ ${shortcut_key} != "" ]] ; then

                    echo "Send shortcut key : $i : ${shortcut_key_xdotool}"

                    xdotool key --clearmodifiers ${shortcut_key_xdotool}

                    sleep ${delay_test}

                else

                    echo "Shortcut key empty :  $i !!!"

                fi

            fi

        else

            if [[ ${shortcut_key_present} == "" ]] ; then

                if [[ ${shortcut_key} != "" && ${shortcut_command} != "" ]] ; then

                    echo "Update shortcut key : $i : ${shortcut_key} : ${shortcut_command}" >> ${fichier_init_config}

                    xfconf-query --verbose --create --channel xfce4-keyboard-shortcuts --property "${shortcut_base}${shortcut_key}"  --type string  --set "${shortcut_command}"

                fi

            elif [[ ${shortcut_key_present_command} != ${shortcut_command} ]] ; then

                if [[ ${command} == "--force" ]] ; then

                    echo "Force shortcut key : $i : ${shortcut_key} : ${shortcut_command}"

                    xfconf-query --verbose --create --channel xfce4-keyboard-shortcuts --property "${shortcut_base}${shortcut_key}"  --type string  --set "${shortcut_command}"

                else

                    echo "Shortcut key present :  $i : ${shortcut_key_present}" >> ${fichier_init_config}

                fi
            fi
        fi

    else

        echo "Shortcut key empty :  $i !!!" >> ${fichier_init_config}

    fi

done

fi

fi

#! /bin/bash

# emmabuntus_accessibility_system_update.sh --
#
#   This file permit to update system of accessibility for the Emmabuntus Distrib
#
#   Created on 2010-24 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 Xfce/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

###Pour exporter la librairie de gettext.
set -a
source gettext.sh
set +a
export TEXTDOMAIN=emmabuntus-accessibility
export TEXTDOMAINDIR="/usr/share/locale"
. gettext.sh

repertoire_emmabuntus=~/.config/emmabuntus
env_emmabuntus=${repertoire_emmabuntus}/env_emmabuntus
attente_lecture_message="1"

message_system_update_no_start="$(eval_gettext 'System update not possible. Internet is not active')"
message_system_update_start="$(eval_gettext 'Start system update')"
message_system_update_no_upgrade="$(eval_gettext 'All packages are up to date')"
message_system_update_upgrade_start="$(eval_gettext 'Start system upgrade. Please wait until upgrade is complete')"
message_system_update_upgrade_ok="$(eval_gettext 'System upgrade successful')"
message_system_update_upgrade_ko="$(eval_gettext 'System upgrade failed')"

. ${env_emmabuntus}

source /usr/bin/emmabuntus_accessibility_func.sh

langue_pico_accessibility_system_update="$(eval_gettext '#emmabuntus_accessibility_system_update en-US')"
langue_pico_accessibility_system_update=$(echo ${langue_pico_accessibility_system_update} | cut -d " " -f 2)
if [[ ${langue_pico_accessibility_system_update} == "en-US" && ${Language_pico} == "en-GB" ]] ; then langue_pico_accessibility_system_update="en-GB" ; fi

escape_orca_read

# Si Orca d'activé attendre x secondes avant de lire le message d'information sur la modification du son
if [[ $(ps -A | grep orca) ]] ; then
    sleep ${attente_lecture_message}
fi

if [[ $(ping -w 1 sourceforge.net | grep "1 received") ]] ; then

    message_audio_pico "${message_system_update_start}" "${langue_pico_accessibility_system_update}"

    sudo /usr/bin/emmabuntus_accessibility_system_update_exec.sh "update"

    if [ $(apt list --upgradable 2>/dev/null | wc -l) -gt 1 ] ; then

        message_audio_pico "${message_system_update_upgrade_start}" "${langue_pico_accessibility_system_update}"

        sudo /usr/bin/emmabuntus_accessibility_system_update_exec.sh "upgrade"
        status=$?
        echo "status = ${status}"

        if [ ${status} -eq 0 ] ; then
            message_audio_pico "${message_system_update_upgrade_ok}" "${langue_pico_accessibility_system_update}"
        else
            message_audio_pico "${message_system_update_upgrade_ko}" "${langue_pico_accessibility_system_update}"
        fi
    else

        message_audio_pico "${message_system_update_no_upgrade}" "${langue_pico_accessibility_system_update}"

    fi

else

    message_audio_pico "${message_system_update_no_start}" "${langue_pico_accessibility_system_update}"

fi


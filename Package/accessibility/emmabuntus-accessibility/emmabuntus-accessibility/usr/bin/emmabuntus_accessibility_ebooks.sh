#! /bin/bash

# emmabuntus_accessibility_ebooks.sh --
#
#   This file for functions of accessibility for the Emmabuntus Distrib
#   to read eBooks library
#
#   Created on 2010-24 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 Xfce/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

###Pour exporter la librairie de gettext.
set -a
source gettext.sh
set +a
export TEXTDOMAIN=emmabuntus-accessibility
export TEXTDOMAINDIR="/usr/share/locale"
. gettext.sh

mode=$1

#  chargement des variables d'environnement
repertoire_emmabuntus=~/.config/emmabuntus
env_emmabuntus=${repertoire_emmabuntus}/env_emmabuntus

dir_ebooks="/usr/share/Documentation_emmabuntus/Documents/eBooks/"
ebooks_visual_command="calibre"
ebooks_audio_command="ebook-speaker"
message_dir_not_found="$(eval_gettext 'Directory not found.')"
message_mode_not_found="$(eval_gettext 'Mode not found.')"

. ${env_emmabuntus}

source /usr/bin/emmabuntus_accessibility_func.sh

if [[ ${Library_epub} != "" ]] ; then
    if test -d ${Library_epub} ; then
        dir_ebooks=${Library_epub}
    fi
fi

ebooks_visual_dir=${dir_ebooks}
ebooks_audio_dir=${dir_ebooks}
ebooks_visual_dir_fr=${dir_ebooks}
ebooks_audio_dir_fr=${dir_ebooks}

langue_pico_accessibility_ebooks="$(eval_gettext '#langue_pico_accessibility_ebooks en-US')"
langue_pico_accessibility_ebooks=$(echo ${langue_pico_accessibility_ebooks} | cut -d " " -f 2)
if [[ ${langue_pico_accessibility_ebooks} == "en-US" && ${Language_pico} == "en-GB" ]] ; then langue_pico_accessibility_ebooks="en-GB" ; fi

ebooks_audio_option_command="-t cat eBook-speaker.txt | pico2wave -w eBook-speaker.wav -l ${langue_pico_accessibility_ebooks}"

if [[ ${Language_pico} == "fr-FR" ]] ; then

    ebooks_visual_dir=${ebooks_visual_dir_fr}
    ebooks_audio_dir=${ebooks_audio_dir_fr}

fi

if [[ ! -d  ${ebooks_audio_dir} ]] ; then

     message_audio_pico "${message_dir_not_found}" "${langue_pico_ebooks_accessibility_ebooks}"

elif [[ ${mode} == "visual" ]] ; then

    ${ebooks_visual_command} ${ebooks_visual_dir}

elif [[ ${mode} == "audio" ]] ; then

    if [[ $(ps -A | grep aplay) ]] ; then pkill aplay ; fi

    xfce4-terminal --fullscreen -x "${ebooks_audio_command}" "${ebooks_audio_dir}" "${ebooks_audio_option_command}"

else

     message_audio_pico "${message_mode_not_found}" "${langue_pico_ebooks_accessibility_ebooks}"

fi

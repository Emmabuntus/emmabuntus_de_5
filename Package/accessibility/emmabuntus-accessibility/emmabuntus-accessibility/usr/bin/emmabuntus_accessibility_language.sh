#! /bin/bash

# emmabuntus_accessibility_language.sh --
#
#   This file modify language for accessibility on the Emmabuntus Distrib.
#
#   Created on 2010-24 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 Xfce/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################


repertoire_emmabuntus=~/.config/emmabuntus
env_emmabuntus=${repertoire_emmabuntus}/env_emmabuntus
fichier_init_config=${repertoire_emmabuntus}/init_emmabuntus_accessibility_language.txt

# Commandes possibles : --force
command=$1

#  chargement des variables d'environnement
. ${env_emmabuntus}

if [[ ( ${command} == "--force" || ! ( -f  ${fichier_init_config} ) || ${LANG} != ${Language} ) ]] ; then

source /usr/bin/emmabuntus_accessibility_func.sh

DATE=`date +"%d:%m:%Y - %H:%M:%S"`
echo "DATE = $DATE" >> ${fichier_init_config}

update_config_emmabuntus Language ${LANG} ${env_emmabuntus}

langue=$(echo $LANG | cut -d . -f 1)
langue_code=$(echo $langue | cut -d _ -f 1)
langue_country=$(echo $langue | cut -d _ -f 2)

# Détermination langue de Sox Pico

case ${langue_code} in
    "de") LANGUE_PICO="de-DE";;
    "es") LANGUE_PICO="es-ES";;
    "it") LANGUE_PICO="it-IT";;
    "fr") LANGUE_PICO="fr-FR";;
    "en") if [[ ${langue_country} == "GB" ]] ; then LANGUE_PICO="en-GB" ; else LANGUE_PICO="en-US" ; fi ;;
       *) LANGUE_PICO=${langue_pico_default} ; ENABLE_ESPEAK="true" ;;
esac

update_config_emmabuntus Language_pico ${LANGUE_PICO} ${env_emmabuntus}
update_config_orca pico ${LANGUE_PICO}

# Détermination langue de eSpeak

espeak --voices | cut -d " " -f 4 > ${repertoire_emmabuntus}/voices_espeak.txt

LANGUE_ESPEAK=""
langue_country_lower=$(echo "${langue_country,,}")

while read voice ; do

    if [[ ${langue_code} == ${voice} || "${langue_code}-${langue_country_lower}" == ${voice} ]];  then

        LANGUE_ESPEAK="${voice}"
        echo "LANGUE_ESPEAK = ${LANGUE_ESPEAK}" >> ${fichier_init_config}

    fi
done < "${repertoire_emmabuntus}/voices_espeak.txt"

if [[ ${LANGUE_ESPEAK} == "" ]]; then
    LANGUE_ESPEAK=${langue_espeak_default}
    ENABLE_ESPEAK="false"
fi

export LANGUE_ESPEAK
export ENABLE_ESPEAK

update_config_emmabuntus Language_espeak ${LANGUE_ESPEAK} ${env_emmabuntus}

if  [[ ${ENABLE_ESPEAK} == "true" ]]; then
    update_config_orca espeak ${LANGUE_ESPEAK}
else
    update_config_orca pico ${LANGUE_PICO}
fi


# Detection Laptop
/usr/bin/laptop-detect
status=$?
echo "status Laptop = ${status}" >> ${fichier_init_config}

if [ ${status} -eq 0 ] ; then
    # Laptop
    LAPTOP_ORCA="true"
else
    LAPTOP_ORCA="false"
fi

update_config_emmabuntus Laptop_orca ${LAPTOP_ORCA} ${env_emmabuntus}
update_laptop_orca ${LAPTOP_ORCA}

echo "Language_pico = ${LANGUE_PICO} : espeak = ${ENABLE_ESPEAK} : Language_espeak = ${LANGUE_ESPEAK} : Laptop_orca = ${LAPTOP_ORCA}" >> ${fichier_init_config}

fi

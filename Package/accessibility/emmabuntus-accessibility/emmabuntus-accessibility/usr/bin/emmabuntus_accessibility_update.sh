#! /bin/bash

# emmabuntus_accessibility_update.sh --
#
#   This file permits to test update for accessibilty for the Emmabuntus Distrib.
#
#   Created on 2010-24 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 Xfce/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

# Command valeurs admises : test
command=$1

###Pour exporter la librairie de gettext.
set -a
source gettext.sh
set +a
export TEXTDOMAIN=emmabuntus-accessibility
export TEXTDOMAINDIR="/usr/share/locale"
. gettext.sh


repertoire_emmabuntus=~/.config/emmabuntus
env_emmabuntus=${repertoire_emmabuntus}/env_emmabuntus
attente_lecture_message="10"

#  chargement des variables d'environnement
. ${env_emmabuntus}

source /usr/bin/emmabuntus_accessibility_func.sh

message_update="$(eval_gettext 'Updates are available. Launch the update application via Alt Super U')"

langue_pico_accessibility_update="$(eval_gettext '#langue_pico_accessibility_update en-US')"
langue_pico_accessibility_update=$(echo ${langue_pico_accessibility_update} | cut -d " " -f 2)
if [[ ${langue_pico_accessibility_update} == "en-US" && ${Language_pico} == "en-GB" ]] ; then langue_pico_accessibility_update="en-GB" ; fi

if [[ ${command} == "test" || ( ${Update_info} == "1" &&  ${Accessibility} == "1" && ! $(cat /proc/cmdline | grep -i live) ) ]] ; then

    if [[ $(ping -w 1 sourceforge.net | grep "1 received") ]] ; then

        if [[ ${command} == "" ]] ; then
            sleep ${attente_lecture_message}
        fi

        if [ $(apt list --upgradable 2>/dev/null | wc -l) -gt 1 ] ; then

            message_audio_pico "${message_update}" "${langue_pico_accessibility_update}"

        fi

    fi

fi

#! /bin/bash

# emmabuntus_accessibility_ventoy.sh --
#
#   This file permit to use ventoy for accessibility on the Emmabuntus Distrib.
#
#   Created on 2010-24 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 Xfce/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

###Pour exporter la librairie de gettext.
set -a
source gettext.sh
set +a
export TEXTDOMAIN=emmabuntus-accessibility
export TEXTDOMAINDIR="/usr/share/locale"
. gettext.sh

repertoire_emmabuntus=~/.config/emmabuntus
env_emmabuntus=${repertoire_emmabuntus}/env_emmabuntus
dir_ventoy="/opt/ventoy"
exec_ventoy="Ventoy2Disk.sh"
attente_lecture_message="2"


KEY_USB=$(lsblk -o NAME,SIZE,TRAN | grep usb | cut  -d ' ' -f1)
VENTOY_CURVER=$(cat ${dir_ventoy}/ventoy/version)

message_quit="$(eval_gettext 'Utility output.')"
message_exit="$(eval_gettext 'Press any key to exit utility')"
message_usb_not_found="$(eval_gettext 'USB stick not found.')"
message_usb_found="$(eval_gettext 'Stick found')"
message_ventoy_not_found1="$(eval_gettext 'Ventoy was not found on the USB stick.')"
message_ventoy_not_found2="$(eval_gettext 'Press Y to install Ventoy on the USB stick')"
message_ventoy_install1="$(eval_gettext 'Warning: installing Ventoy on the USB key destroys all the existing data.')"
message_ventoy_install2="$(eval_gettext 'Please confirm this by typing Y, otherwise press any other key')"
message_ventoy_not_updated_version1="$(eval_gettext 'Ventoy on the USB stick is not up to date.')"
message_ventoy_not_updated_version2="$(eval_gettext 'Press Y to update Ventoy on the USB stick')"
message_ventoy_updated_version="$(eval_gettext 'Ventoy on the USB stick is up to date.')"
message_install_finish_ok="$(eval_gettext 'Installation completed successfully.')"
message_install_finish_ko="$(eval_gettext 'Installation failed.')"
message_update_finish="$(eval_gettext 'Update completed.')"
CODE_OK_MINUSCULE="y"
CODE_OK_MAJUSCULE="Y"

sleep ${attente_lecture_message}

# Identification de la clé USB
# Finding out the USB Key
if [ -z ${KEY_USB} ] ; then

   echo -e "\n${message_usb_not_found}"
   read -n 1 -p "${message_exit} ? " read_no_found_key

   exit 0

else

    KEY_USB_FOUND="/dev/${KEY_USB}"
	
	echo -e ""
    read -n 1 -p "${message_usb_found} ${KEY_USB^^}" -t 1
                  
fi

KEY_USB_VENTOY_VERSION=$(sudo ${dir_ventoy}/${exec_ventoy} -l ${KEY_USB_FOUND} | grep "Ventoy Version in Disk" | cut -d ":" -f 2 | cut -d " " -f 2)

if [ -z ${KEY_USB_VENTOY_VERSION} ] ; then

   echo -e "\n${message_ventoy_not_found1}" 
   read -n 1 -p "${message_ventoy_not_found2} ? " read_install_ventoy

   if [ "${read_install_ventoy}" = ${CODE_OK_MINUSCULE} ] || [ "${read_install_ventoy}" = ${CODE_OK_MAJUSCULE} ] ; then

		echo ""
        echo -e "\n${message_ventoy_install1}"
        read -n 1 -p "${message_ventoy_install2} ? " read_install_ventoy_confirm

        if [ "${read_install_ventoy_confirm}" = ${CODE_OK_MINUSCULE} ] || [ "${read_install_ventoy_confirm}" = ${CODE_OK_MAJUSCULE} ] ; then
        
			echo ""

            sudo ${dir_ventoy}/${exec_ventoy} -i ${KEY_USB_FOUND} < <(echo 'y' ; echo 'y')

            KEY_USB_VENTOY_VERSION_TEST=$(sudo ${dir_ventoy}/${exec_ventoy} -l ${KEY_USB_FOUND} | grep "Ventoy Version in Disk" | cut -d ":" -f 2 | cut -d " " -f 2)

            if  [ "${KEY_USB_VENTOY_VERSION_TEST}" == ${VENTOY_CURVER} ] ; then

                echo -e "\n${message_install_finish_ok}"
                read -n 1 -p "${message_exit} ? " read_install_finish

            else

                echo -e "\n${message_install_finish_ko}"
                read -n 1 -p "${message_exit} ? " read_install_finish

            fi

        else

            echo -e "\n${message_quit}"

        fi

    else

        echo -e "\n${message_quit}"

   fi

else

    if [ "${KEY_USB_VENTOY_VERSION}" != ${VENTOY_CURVER} ] ; then


        recho -e "\n${message_ventoy_not_updated_version1}"
        read -n 1 -p "${message_ventoy_not_updated_version2} ? " read_update_ventoy

        if [ "${read_update_ventoy}" = ${CODE_OK_MINUSCULE} ] || [ "${read_update_ventoy}" = ${CODE_OK_MAJUSCULE} ] ; then
        
				echo ""

                sudo ${dir_ventoy}/${exec_ventoy} -u ${KEY_USB_FOUND} < <(echo 'y')

                echo -e "\n${message_update_finish}"
                read -n 1 -p "${message_exit} ? " read_install_finish

        else

            echo -e "\n${message_quit}"

        fi

    else

        echo -e "\n${message_ventoy_updated_version}"
        read -n 1 -p "${message_exit} ? " read_install_finish
	
    fi

fi


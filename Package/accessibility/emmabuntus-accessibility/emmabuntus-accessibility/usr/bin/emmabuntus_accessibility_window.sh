#! /bin/bash

# emmabuntus_accessibility_window.sh --
#
#   This file permits to prepare Accessibilty for the Emmabuntus Distrib.
#
#   Created on 2010-24 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 Xfce/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

. "$HOME/.config/user-dirs.dirs"


###Pour exporter la librairie de gettext.
set -a
source gettext.sh
set +a
export TEXTDOMAIN=emmabuntus-accessibility
export TEXTDOMAINDIR="/usr/share/locale"
. gettext.sh

repertoire_emmabuntus=~/.config/emmabuntus
env_emmabuntus=${repertoire_emmabuntus}/env_emmabuntus
fichier_init_config=${repertoire_emmabuntus}/emmabuntus_config_accessibility_window.txt

Init=""
Init=$1

nom_fenetre="$(eval_gettext 'Accessibility')"

msg_orca=$(eval_gettext 'Activate Orca')

comment_orca=$(eval_gettext 'Enables screen audio reader')
comment_config_orca=$(eval_gettext 'Allows you to configure the screen audio reader')
msg_audio_info=$(eval_gettext 'Activate audio information')
comment_audio_info=$(eval_gettext 'Allows you to activate audio information messages')
msg_internet_info=$(eval_gettext 'Activate Internet state information')
comment_internet_info=$(eval_gettext 'Allows you to activate Internet state audio messages')
msg_update_info=$(eval_gettext 'Activate updates state information')
comment_update_info=$(eval_gettext 'Allows you to activate audio messages on updates state')
msg_pico=$(eval_gettext 'Svox Pico : ')
comment_pico=$(eval_gettext 'Language selection for SvoX Pico speech synthesis')
msg_enable_espeak=$(eval_gettext 'Activate eSpeak speech synthesis')
comment_enable_espeak=$(eval_gettext 'Allows you to activate eSpeak speech synthesis for Orca')
msg_espeak=$(eval_gettext 'eSpeak : ')
comment_espeak=$(eval_gettext 'Language selection for eSpeak speech synthesis')
msg_laptop_orca=$(eval_gettext 'Activate shortcuts for laptop')
comment_laptop_orca=$(eval_gettext 'Allows you to activate key binding for Orca on laptop')

msg_compiz=$(eval_gettext 'Activate Compiz')
comment_compiz=$(eval_gettext 'Zoom management utility, cursor settings')
comment_config_compiz=$(eval_gettext 'Allows you to configure the Compiz utility')

msg_dark_theme=$(eval_gettext 'Activate dark theme')
comment_dark_theme=$(eval_gettext 'The dark theme is less tiring on the eyes')
msg_postinstall=$(eval_gettext 'Activate post-installation')
comment_postinstall=$(eval_gettext 'Enables Emmabuntüs post-installation')

msg_cursorname=$(eval_gettext 'Cursor selection : ')
comment_cursorname=$(eval_gettext 'Cursor shape selection')
msg_cursorsize=$(eval_gettext 'Size : ')
comment_cursorsize=$(eval_gettext 'Allows you to set the cursor size with a minimum value of 16')
msg_themename=$(eval_gettext 'Theme selection : ')
comment_themename=$(eval_gettext 'Graphic theme selection')
msg_iconthemename=$(eval_gettext 'Icon selection : ')
comment_iconthemename=$(eval_gettext 'Icon theme selection')

msg_fontname=$(eval_gettext 'Font selection : ')
comment_fontname=$(eval_gettext 'Accessibility font type selection')
msg_fontsize=$(eval_gettext 'Size : ')
comment_fontsize=$(eval_gettext 'Allows you to set the font size with a minimum value of 11')
msg_autologin=$(eval_gettext 'Activate automatic connection')
comment_autologin=$(eval_gettext 'Enables automatic session connection')

msg_emulation_mouse=$(eval_gettext 'Mouse emulation selection : ')
comment_emulation_mouse=$(eval_gettext 'Improves mouse usage capabilities')
msg_emulation_mouse_off=$(eval_gettext 'Off')
msg_numpad=$(eval_gettext 'NumPad')

msg_keyboard=$(eval_gettext 'Keyboard : ')
btn_keyboard_select=$(eval_gettext 'Keyboard selection ')
btn_visual_aid=$(eval_gettext 'Visual aid')
btn_audio_aid=$(eval_gettext 'Audio aid')
btn_visual_shortcuts=$(eval_gettext 'Visual shortcuts')
btn_audio_shortcuts=$(eval_gettext 'Audio shortcuts')
btn_decline=$(eval_gettext 'Decline ')
btn_activate=$(eval_gettext 'Activate  ')

msg_orca_enable_cairodock=$(eval_gettext 'Activate Dock')
comment_orca_enable_cairodock=$(eval_gettext 'Allow to enable Cairo-dock with Orca')
msg_compiz_enable_cairodock=$(eval_gettext 'Activate Dock')
comment_compiz_enable_cairodock=$(eval_gettext 'Allow to enable Cairo-dock with Compiz')

language_pico_enum=( "de-DE" "en-GB" "en-US" "es-ES" "fr-FR" "it-IT" )
cursorthemename_value_enum=( "Adwaita" "ComixCursors-Opaque-Black" "ComixCursors-Opaque-Blue"  "ComixCursors-Opaque-Green" "ComixCursors-Opaque-Orange" "ComixCursors-Opaque-Red" "ComixCursors-Opaque-White" "Maya-Orange" "Maya-Black" "Maya-White" "Bgreat-Black" "Bgreat-White" )
themename_value_enum=( "Arc-Dark-largerborders" "BlackMATE" "HighContrast" "Arc-Darker-largerborders" "Qogir-dark-largerborders" "Qogir-largerborders" "Qogir-light-largerborders" )
iconthemename_value_enum=( "revival-blue" "ContrastHigh" "HighContrast" "Papirus" "Papirus-Dark" "Papirus-Light" "breeze" )
fontthemename_value_enum=( "Liberation Sans" "Eido" "Luciole" "OpenDyslexic" "Tiresias Infofont" )
fontthemesize_value_max_enum=( "20" "16" "17" "18" "18" )

if [[ $(ps -A | grep "xfce4-session") ]] ; then
    emulation_mouse_enum=( "${msg_emulation_mouse_off}" "${msg_numpad}" "MouseTweaks" )
else
    emulation_mouse_enum=( "${msg_emulation_mouse_off}" "MouseTweaks" "${msg_emulation_mouse_off}")
fi

choix_cursorname=${cursorthemename_value_enum[0]}
cursorsize_init="16"
choix_themename=${themename_value_enum[0]}
choix_iconthemename=${iconthemename_value_enum[0]}
choix_fontname=${fontthemename_value_enum[0]}
fontsize_init="11"

# Activation de option dispoinible uniquement sous Xfce
if [[ $(ps -A | grep "xfce4-session") ]] ; then
    CHECKBOX_XFCE_ACTIF="true"

    # Lecture des paramètres
    channel="xsettings"
    channel_base_gtk="/Gtk/"
    channel_base_net="/Net/"
    choix_cursorname=$(xfconf-query --verbose --create --channel "${channel}" --property "${channel_base_gtk}CursorThemeName")
    choix_themename=$(xfconf-query --verbose --create --channel "${channel}" --property "${channel_base_net}ThemeName")
    choix_iconthemename=$(xfconf-query --verbose --create --channel "${channel}" --property "${channel_base_net}IconThemeName")
    cursorsize_init=$(xfconf-query --verbose --create --channel "${channel}" --property "${channel_base_gtk}CursorThemeSize")
    read_fontname=$(xfconf-query --verbose --create --channel "${channel}" --property "${channel_base_gtk}FontName")
    nb_mot=$(echo ${read_fontname} | wc -w)
    fontsize_init=$(echo ${read_fontname} | cut -d " " -f ${nb_mot})
    choix_fontname=$(echo ${read_fontname} | cut -d " " -f ${nb_mot} --complement)

else
    CHECKBOX_XFCE_ACTIF="false"
fi

# Remplacement de curseur absent dans la liste
if [[ ! $(printf '%s\n' "${cursorthemename_value_enum[@]}" | grep -x "${choix_cursorname}" )  ]] ; then
    if [[ "${choix_cursorname}" !=  "default" ]] ; then
        cursorthemename_value_enum[0]=${choix_cursorname}
    else
        choix_cursorname=${cursorthemename_value_enum[0]}
    fi
fi

# Remplacement du thème absent dans la liste
if [[ ! $(printf '%s\n' "${themename_value_enum[@]}" | grep -x "${choix_themename}" )  ]] ; then
    themename_value_enum[0]=${choix_themename}
fi

# Remplacement de icôme absente dans la liste
if [[ ! $(printf '%s\n' "${iconthemename_value_enum[@]}" | grep -x "${choix_iconthemename}" )  ]] ; then
    iconthemename_value_enum[0]=${choix_iconthemename}
fi

# Remplacement de la police absente dans la liste
if [[ ! $(printf '%s\n' "${fontthemename_value_enum[@]}" | grep -x "${choix_fontname}" )  ]] ; then
    fontthemename_value_enum[0]=${choix_fontname}
fi

if [[ ${EMULATION_MOUSE} == "Off" ]] ; then
    EMULATION_MOUSE="${msg_emulation_mouse_off}"
elif [[ ${EMULATION_MOUSE} == "NumPad" ]] ; then
    EMULATION_MOUSE="${msg_numpad}"
fi

export language_pico_enum
export cursorthemename_value_enum
export themename_value_enum
export iconthemename_value_enum
export fontthemename_value_enum
export fontthemesize_value_max_enum

# Utilisateur n'appartient pas au group sudo et mode live
if [[ $(groups | grep sudo) == "" || $(cat /proc/cmdline | grep -i boot=live) != "" ]] ; then
    autologin_visible="false"
    AUTOLOGIN="false"
else
    autologin_visible="true"
fi

# Affichage de la post-install
if [[ ${Init} != "" ]] ; then
    postinstall_visible="false"
    POSTINSTALL="false"
else
    postinstall_visible="true"
fi


source /usr/bin/emmabuntus_found_theme.sh

highlight_color=$(FOUND_HIGHLIGHT_COLOR)

if [[ ${highlight_color} == blue ]] ; then
    DARK_THEME="false"
else
    DARK_THEME="true"
fi

WIDTH_REQUEST_WINDOW="800"

export WINDOW_DIALOG_WELCOME='<window title="'${nom_fenetre}'" icon-name="gtk-info" resizable="false">
<vbox spacing="0">

<text use-markup="true" wrap="false" xalign="0" justify="3">
<input>echo "'$message_accueil_accessibility_visuel'" | sed "s%\\\%%g"</input>
</text>

<vbox>

<frame   Audio>

  <hbox spacing="0" width_request="${WIDTH_REQUEST_WINDOW}" space-expand="true" space-fill="false">

    <hbox>
    <checkbox active="'${ORCA}'" tooltip-text="'${comment_orca}'">
        <variable>ORCA</variable>
        <label>'${msg_orca}'</label>
    </checkbox>
    <button tooltip-text="'${comment_config_orca}'">
    <input file icon="orca"></input>
    <action>xdotool key Super+Alt+s</action>
    </button>
    </hbox>

    <hbox>
    <text><label>'${msg_pico}'</label></text>
    <comboboxtext allow-empty="false" value-in-list="true" tooltip-text="'${comment_pico}'">
    <variable>LANGUAGEPICO</variable>
    <default>'${LANGUE_PICO}'</default>
    <input>echo '${language_pico_enum[@]}' | tr " " "\n"</input>
    </comboboxtext>
    </hbox>

    <checkbox active="'${ENABLE_ESPEAK}'" tooltip-text="'${comment_enable_espeak}'">
    <variable>ENABLE_ESPEAK</variable>
    <label>'${msg_enable_espeak}'</label>
    </checkbox>

    <hbox>
    <text><label>'${msg_espeak}'</label></text>
    <comboboxtext allow-empty="false" value-in-list="true" tooltip-text="'${comment_espeak}'">
    <variable>LANGUAGEESPEAK</variable>
    <default>'${LANGUE_ESPEAK}'</default>
    <input file>'${repertoire_emmabuntus}/voices_espeak.txt'</input>
    </comboboxtext>
    </hbox>

   <checkbox active="'${ORCA_ENABLE_CAIRODOCK}'" tooltip-text="'${comment_orca_enable_cairodock}'">
     <variable>ORCA_ENABLE_CAIRODOCK</variable>
     <label>'${msg_orca_enable_cairodock}'</label>
   </checkbox>

  </hbox>

  <hbox spacing="0" width_request="${WIDTH_REQUEST_WINDOW}" space-expand="true" space-fill="false">

    <checkbox active="'${LAPTOP_ORCA}'" tooltip-text="'${comment_laptop_orca}'">
        <variable>LAPTOP_ORCA</variable>
        <label>'${msg_laptop_orca}'</label>
    </checkbox>

    <checkbox active="'${AUDIO_INFO}'" tooltip-text="'${comment_audio_info}'">
        <variable>AUDIO_INFO</variable>
        <label>'${msg_audio_info}'</label>
    </checkbox>

    <checkbox active="'${INTERNET_INFO}'" tooltip-text="'${comment_internet_info}'">
        <variable>INTERNET_INFO</variable>
        <label>'${msg_internet_info}'</label>
    </checkbox>

    <checkbox active="'${UPDATE_INFO}'" tooltip-text="'${comment_update_info}'">
        <variable>UPDATE_INFO</variable>
        <label>'${msg_update_info}'</label>
    </checkbox>

  </hbox>

</frame>

<frame   Visual>

  <hbox spacing="0" width_request="${WIDTH_REQUEST_WINDOW}" space-expand="true" space-fill="false" >

   <hbox>
   <checkbox active="'${COMPIZ}'" tooltip-text="'${comment_compiz}'">
     <variable>COMPIZ</variable>
     <label>'${msg_compiz}'</label>
   </checkbox>

   <hbox>
   <button tooltip-text="'${comment_config_compiz}'">
   <input file icon="ccsm"></input>
   <action>ccsm  --category=Accessibility &</action>
   </button>
   </hbox>
   </hbox>

   <checkbox active="'${DARK_THEME}'" tooltip-text="'${comment_dark_theme}'" sensitive="'${CHECKBOX_XFCE_ACTIF}'">
     <variable>DARK_THEME</variable>
     <label>'${msg_dark_theme}'</label>
     <action>/usr/bin/emmabuntus_accessibility_window_desktop.sh CorlorTheme "${DARK_THEME}"</action>
   </checkbox>

    <checkbox active="'${POSTINSTALL}'" tooltip-text="'${comment_postinstall}'" visible="'${postinstall_visible}'">
      <variable>POSTINSTALL</variable>
      <label>'${msg_postinstall}'</label>
    </checkbox>

   <hbox>
    <text>
      <label>'${msg_keyboard}'</label>
    </text>

    <entry editable="false" width_request="100">
      <default>"'${KEYBOARD}'"</default>
      <variable>KEYBOARD</variable>
    </entry>

    <button>
    <label>"'${btn_keyboard_select}'"</label>
    <input file icon="lxkeymap"></input>
    <action>/usr/bin/emmabuntus_accessibility_window_desktop.sh LxKeyMap</action>
    </button>
   </hbox>

   <checkbox active="'${COMPIZ_ENABLE_CAIRODOCK}'" tooltip-text="'${comment_compiz_enable_cairodock}'">
     <variable>COMPIZ_ENABLE_CAIRODOCK</variable>
     <label>'${msg_compiz_enable_cairodock}'</label>
   </checkbox>

  </hbox>

  <hbox spacing="0" width_request="${WIDTH_REQUEST_WINDOW}" space-expand="true" space-fill="false" >

   <hbox>
   <text><label>'${msg_cursorname}'</label></text>
   <comboboxtext allow-empty="false" value-in-list="true" tooltip-text="'${comment_cursorname}'" sensitive="'${CHECKBOX_XFCE_ACTIF}'">
    <variable>CURSORNAME</variable>
    <default>'${choix_cursorname}'</default>
    <input>echo '${cursorthemename_value_enum[@]}' | tr " " "\n"</input>
    <action>/usr/bin/emmabuntus_accessibility_window_desktop.sh CursorThemeName ${CURSORNAME}</action>
   </comboboxtext>
   </hbox>


   <hbox>
   <text><label>'${msg_cursorsize}'</label></text>
   <entry tooltip-text="'${comment_cursorsize}'" width_request="60" sensitive="'${CHECKBOX_XFCE_ACTIF}'">
    <input>echo '${cursorsize_init}'</input>
    <variable>CURSORSIZE</variable>
    <action>/usr/bin/emmabuntus_accessibility_window_desktop.sh CursorThemeSize ${CURSORSIZE}</action>
   </entry>
   </hbox>


   <hbox>
   <text><label>'${msg_fontname}'</label></text>
   <comboboxtext allow-empty="false" value-in-list="true" tooltip-text="'${comment_fontname}'" sensitive="'${CHECKBOX_XFCE_ACTIF}'">
    <variable>FONTNAME</variable>
    <default>'${choix_fontname}'</default>
    <item>'${fontthemename_value_enum[0]}'</item>
    <item>'${fontthemename_value_enum[1]}'</item>
    <item>'${fontthemename_value_enum[2]}'</item>
    <item>'${fontthemename_value_enum[3]}'</item>
    <item>'${fontthemename_value_enum[4]}'</item>
    <action>/usr/bin/emmabuntus_accessibility_window_desktop.sh FontName "${FONTNAME}" "${FONTSIZE}"</action>
   </comboboxtext>
   </hbox>

   <hbox>
   <text><label>'${msg_fontsize}'</label></text>
   <entry tooltip-text="'${comment_fontsize}'" width_request="60" sensitive="'${CHECKBOX_XFCE_ACTIF}'">
    <input>echo '${fontsize_init}'</input>
    <variable>FONTSIZE</variable>
    <action>/usr/bin/emmabuntus_accessibility_window_desktop.sh FontName "${FONTNAME}" "${FONTSIZE}"</action>
   </entry>
   </hbox>

  </hbox>

  <hbox spacing="0" width_request="${WIDTH_REQUEST_WINDOW}" space-expand="true" space-fill="false" >

   <hbox>
   <text><label>'${msg_themename}'</label></text>
   <comboboxtext allow-empty="false" value-in-list="true" tooltip-text="'${comment_themename}'" sensitive="'${CHECKBOX_XFCE_ACTIF}'">
    <variable>THEMENAME</variable>
    <default>'${choix_themename}'</default>
    <input>echo '${themename_value_enum[@]}' | tr " " "\n"</input>
    <action>/usr/bin/emmabuntus_accessibility_window_desktop.sh ThemeName "${THEMENAME}"</action>
   </comboboxtext>
   </hbox>

   <hbox>
   <text><label>'${msg_iconthemename}'</label></text>
   <comboboxtext allow-empty="false" value-in-list="true" tooltip-text="'${comment_iconthemename}'" sensitive="'${CHECKBOX_XFCE_ACTIF}'">
    <variable>ICONTHEMENAME</variable>
    <default>'${choix_iconthemename}'</default>
    <input>echo '${iconthemename_value_enum[@]}' | tr " " "\n"</input>
    <action>/usr/bin/emmabuntus_accessibility_window_desktop.sh IconThemeName "${ICONTHEMENAME}"</action>
   </comboboxtext>
   </hbox>

   <hbox>
   <text><label>'${msg_emulation_mouse}'</label></text>
   <comboboxtext allow-empty="false" value-in-list="true" tooltip-text="'${comment_emulation_mouse}'">
   <variable>EMULATION_MOUSE</variable>
   <default>'${EMULATION_MOUSE}'</default>
   <item>'${emulation_mouse_enum[0]}'</item>
   <item>'${emulation_mouse_enum[1]}'</item>
   <item>'${emulation_mouse_enum[2]}'</item>
   </comboboxtext>
   </hbox>

  </hbox>

</frame>

  <checkbox active="'${AUTOLOGIN}'" tooltip-text="'${comment_autologin}'" visible="'${autologin_visible}'">
    <variable>AUTOLOGIN</variable>
    <label>'${msg_autologin}'</label>
  </checkbox>

<hseparator space-expand="true" space-fill="true"></hseparator>

</vbox>

  <hbox>
    <hbox>
    <button>
    <label>"'${btn_visual_aid}'"</label>
    <input file stock="gtk-find"></input>
    <action>/usr/bin/emmabuntus_accessibility_help.sh visual</action>
    </button>
    <button>
    <label>"'${btn_audio_aid}'"</label>
    <input file stock="gtk-help"></input>
    <action>/usr/bin/emmabuntus_accessibility_help.sh audio</action>
    </button>
    </hbox>

    <hbox>
    <button>
    <label>"'${btn_visual_shortcuts}'"</label>
    <input file stock="gtk-find"></input>
    <action>/usr/bin/emmabuntus_accessibility_help_shortcuts.sh visual compact</action>
    </button>
    <button>
    <label>"'${btn_audio_shortcuts}'"</label>
    <input file stock="gtk-help"></input>
    <action>/usr/bin/emmabuntus_accessibility_help_shortcuts.sh audio compact</action>
    </button>
    </hbox>

   <text><label>"                                                                    "</label></text>

    <hbox>
    <button>
    <label>"'${btn_decline}'"</label>
    <input file stock="gtk-cancel"></input>
    <action>exit:Cancel</action>
    </button>

    <button can-default="true" has-default="true" use-stock="true" is-focus="true">
    <label>"'${btn_activate}'"</label>
    <input file stock="gtk-ok"></input>
    <action>exit:OK</action>
    </button>
    </hbox>

  </hbox>

</vbox>

<action signal="key-press-event">/usr/bin/emmabuntus_accessibility_window_audio.sh ${KEY_SYM} ${KEY_MOD} ${ORCA} ${LANGUAGEPICO} ${ENABLE_ESPEAK} ${LANGUAGEESPEAK} ${LAPTOP_ORCA} ${AUDIO_INFO} ${INTERNET_INFO} ${UPDATE_INFO} ${AUTOLOGIN} ${COMPIZ} ${DARK_THEME} ${POSTINSTALL} ${KEYBOARD}</action>

</window>'

#gtkdialog --center --program=WINDOW_DIALOG_WELCOME

MENU_DIALOG_WELCOME="$(gtkdialog --center --program=WINDOW_DIALOG_WELCOME)"

echo "MENU_DIALOG_WELCOME=$MENU_DIALOG_WELCOME" >> ${fichier_init_config}

eval ${MENU_DIALOG_WELCOME}

source /usr/bin/emmabuntus_accessibility_func.sh


if [[ ${EXIT} == "OK" ]] ; then

    update_config_emmabuntus Orca ${ORCA} ${env_emmabuntus}
    update_config_emmabuntus Compiz ${COMPIZ} ${env_emmabuntus}

    update_config_emmabuntus Audio_info ${AUDIO_INFO} ${env_emmabuntus}
    update_config_emmabuntus Internet_info ${INTERNET_INFO} ${env_emmabuntus}
    update_config_emmabuntus Update_info ${UPDATE_INFO} ${env_emmabuntus}
    update_config_emmabuntus Dark_theme ${DARK_THEME} ${env_emmabuntus}
    update_config_emmabuntus Postinstall ${POSTINSTALL} ${env_emmabuntus}
    update_config_emmabuntus Autologin ${AUTOLOGIN} ${env_emmabuntus}
    update_config_emmabuntus Enable_espeak ${ENABLE_ESPEAK} ${env_emmabuntus}
    update_config_emmabuntus Laptop_orca ${LAPTOP_ORCA} ${env_emmabuntus}
    update_laptop_orca ${LAPTOP_ORCA}
    update_config_tuxtype ${LANGUE_ESPEAK}
    update_config_ebook_speaker ${LANGUE_ESPEAK} ${LANGUE_PICO}
    update_config_emmabuntus Language_pico ${LANGUAGEPICO} ${env_emmabuntus}
    update_config_emmabuntus Language_espeak ${LANGUAGEESPEAK} ${env_emmabuntus}

    update_config_emmabuntus Orca_enable_cairodock ${ORCA_ENABLE_CAIRODOCK} ${env_emmabuntus}
    update_config_emmabuntus Compiz_enable_cairodock ${COMPIZ_ENABLE_CAIRODOCK} ${env_emmabuntus}

    if [[ ${EMULATION_MOUSE} == "${msg_emulation_mouse_off}" ]] ; then
        update_config_emmabuntus Emulation_mouse "Off" ${env_emmabuntus}
    elif [[ ${EMULATION_MOUSE} == "${msg_numpad}" ]] ; then
        update_config_emmabuntus Emulation_mouse "NumPad" ${env_emmabuntus}
    else
        update_config_emmabuntus Emulation_mouse ${EMULATION_MOUSE} ${env_emmabuntus}
    fi

    if [[ ${ENABLE_ESPEAK} == "true" ]] ; then
        update_config_orca espeak ${LANGUAGEESPEAK}
    else
        update_config_orca pico ${LANGUAGEPICO}
    fi

fi

if [[ ${ORCA} == "true" || ${COMPIZ} == "true" ]] ; then
    update_config_emmabuntus Accessibility "true" ${env_emmabuntus}
else
    update_config_emmabuntus Accessibility "false" ${env_emmabuntus}
fi

pkill -f emmabuntus_accessibility_window > /dev/null

if [[ ${EXIT} == "Cancel" ]] ; then
    exit 1
else
    exit 0
fi

#! /bin/bash

# Méthode paquet origine
nom=emmabuntus-accessibility
version=1.0.1
nom_paquet=${nom}_${version}_all.deb

# Méthode assemblage
dpkg-deb -b ${nom} ${nom_paquet}

if test -f ${nom_paquet} ; then

cp ${nom_paquet} ../../../config/includes.chroot/Install/accessibility/

#rm ${nom_paquet}

else
    echo "Paquet non généré"
fi

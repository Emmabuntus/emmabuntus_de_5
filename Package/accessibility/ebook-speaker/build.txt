
# https://benjamin.sonntag.fr/2014/compilation-dun-paquet-debian-a-partir-des-sources.html

sudo apt-get install build-essential devscripts

# Prendre la version de https://github.com/book-readers/ebook-speaker  https://github.com/book-readers/ebook-speaker/archive/refs/heads/main.zip

sudo apt install libncursesw5 libmagic1 libpulse0 libxml2  libsox-fmt-mp3 libsox-fmt-pulse unar txt2man man2html-base html2text gettext espeak libreoffice-writer calibre pandoc poppler-utils ghostscript tesseract-ocr sane-utils netpbm unrtf wget txt2man man2html libsox3

sudo apt install libasound2-dev libmagic-dev libncursesw5-dev libsox-dev libpulse-dev libsndfile1-dev libxml2-dev

export LDFLAGS="-Wl,--copy-dt-needed-entries"

copier un fichier Readme abscent dans doc

cd ebook-speaker-6.2

debuild -i -us -uc -b

# Faire le patch puis
quilt refresh

debuild -i -us -uc -b



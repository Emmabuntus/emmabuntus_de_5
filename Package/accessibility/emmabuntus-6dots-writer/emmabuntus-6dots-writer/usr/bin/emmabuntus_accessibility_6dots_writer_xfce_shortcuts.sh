#! /bin/bash

# emmabuntus_accessibility_6dots_writer_xfce_shortcuts.sh --
#
#   This file modify shortcuts for accessibility on the Emmabuntus Distrib.
#
#   Created on 2010-24 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 Xfce/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

# Affiche tous les raccourcis
# xfconf-query -c xfce4-keyboard-shortcuts -l -v

# Recherche si un raccourci est affecté
# shortcut="<Super>e"
# xfconf-query -c xfce4-keyboard-shortcuts -l -v | grep -i ${shortcut}

repertoire_emmabuntus=~/.config/emmabuntus
fichier_init_config=${repertoire_emmabuntus}/init_emmabuntus_accessibility_6dots_writer_xfce_shortcuts.txt

# Test si session sous Xfce
if [[ $(ps -A | grep "xfce4-session") ]] ; then

# Commandes possibles : --force, --test
command=$1

if [[ ( ${command} == "--force" || ${command} == "--test" || ! ( -f  ${fichier_init_config} ) ) && $( ps -A | grep xfce4-panel ) ]] ; then

max_shortcut=4

shortcuts_key=(

                   [0]='<Super>g'                                                                      [1]='<Ctrl>g'
                   [2]='<Super>d'                                                                      [3]='<Ctrl>d'

                   )


shortcuts_command=(

                   [0]='/usr/bin/emmabuntus_6dots_writer.sh "geany"'                                   [1]='/usr/bin/emmabuntus_6dots_writer.sh "geany" "braille"'
                   [2]='/usr/bin/emmabuntus_6dots_writer.sh "libreoffice"'                             [3]='/usr/bin/emmabuntus_6dots_writer.sh "libreoffice" "braille"'
                  )


shortcut_key_present=""
shortcut_key=""
shortcut_command=""
shortcut_base="/commands/custom/"
delay_test="5"


if [[ ${command} == "" ]] ; then

    DATE=`date +"%d:%m:%Y - %H:%M:%S"`
    echo "DATE = $DATE" >> ${fichier_init_config}

fi


for (( i=0; i<=${max_shortcut}-1; i++ ))
do

    if [[ ${shortcuts_key[$i]} != "" ]] ; then

        shortcut_key_present=$(xfconf-query -c xfce4-keyboard-shortcuts -l -v | grep -i "${shortcut_base}${shortcuts_key[$i]} ")

        if [[ ${shortcut_key_present} != "" ]] ; then
            shortcut_key_present_command=$(xfconf-query -c xfce4-keyboard-shortcuts -p "${shortcut_base}${shortcuts_key[$i]}" -v)
        else
            shortcut_key_present_command=""
        fi

        shortcut_key=${shortcuts_key[$i]}
        shortcut_command=${shortcuts_command[$i]}

        #echo "$i"
        #echo "${shortcut_key}"
        #echo "${shortcut_command}"
        #echo "Shortcut key present : ${shortcut_key_present}"

        if [[ ${command} == "--test" ]] ; then

            if [[ ${shortcut_key_present} != "" || ${shortcut_key_present_command} != "" ]] ; then

                echo "Shortcut key present :  $i : ${shortcut_key_present} : ${shortcut_key_present_command}"

            else

                shortcut_key_xdotool=${shortcut_key//</}
                shortcut_key_xdotool=${shortcut_key_xdotool//>/+}
                shortcut_key_xdotool=${shortcut_key_xdotool//Primary/Ctrl}

                if [[ ${shortcut_key} != "" ]] ; then

                    echo "Send shortcut key : $i : ${shortcut_key_xdotool}"

                    xdotool key --clearmodifiers ${shortcut_key_xdotool}

                    sleep ${delay_test}

                else

                    echo "Shortcut key empty :  $i !!!"

                fi

            fi

        else

            if [[ ${shortcut_key_present} == "" ]] ; then

                if [[ ${shortcut_key} != "" && ${shortcut_command} != "" ]] ; then

                    echo "Update shortcut key : $i : ${shortcut_key} : ${shortcut_command}" >> ${fichier_init_config}

                    xfconf-query --verbose --create --channel xfce4-keyboard-shortcuts --property "${shortcut_base}${shortcut_key}"  --type string  --set "${shortcut_command}"

                fi

            elif [[ ${shortcut_key_present_command} != ${shortcut_command} ]] ; then

                if [[ ${command} == "--force" ]] ; then

                    echo "Force shortcut key : $i : ${shortcut_key} : ${shortcut_command}"

                    xfconf-query --verbose --create --channel xfce4-keyboard-shortcuts --property "${shortcut_base}${shortcut_key}"  --type string  --set "${shortcut_command}"

                else

                    echo "Shortcut key present :  $i : ${shortcut_key_present}" >> ${fichier_init_config}

                fi
            fi
        fi

    else

        echo "Shortcut key empty :  $i !!!" >> ${fichier_init_config}

    fi

done

fi

fi

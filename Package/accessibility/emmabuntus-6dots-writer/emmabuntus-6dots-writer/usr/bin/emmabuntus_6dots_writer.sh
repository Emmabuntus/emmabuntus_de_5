#!/bin/bash

# emmabuntus_6dots_writer.sh --
#
#    This file permits Type text using the PC keyboard using braille patterns,
#    similar to the Perkins method for Emmabuntüs.
#
#   Created on 2010-24 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 Xfce/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

###Pour exporter la librairie de gettext.
set -a
source gettext.sh
set +a
export TEXTDOMAIN=emmabuntus-accessibility-6dots-writer
export TEXTDOMAINDIR="/usr/share/locale"
. gettext.sh

repertoire_emmabuntus=~/.config/emmabuntus
env_emmabuntus=${repertoire_emmabuntus}/env_emmabuntus
fichier_init_config=${repertoire_emmabuntus}/emmabuntus_6dots_writer.txt
config_file=~/isb.cfg
attente_lecture_message="2"
attente_test_fermeture_editeur="5"
erreur_selection_langue=0

if [[ "${1}" == "libreoffice" ]] ; then
    nom_editeur="libreoffice"
    nom_editeur_launcher="libreoffice --writer"
    nom_editeur_bin="soffice.bin"
else
    nom_editeur="geany"
    nom_editeur_launcher="geany"
    nom_editeur_bin="geany"
fi

#  chargement des variables d'environnement
. ${env_emmabuntus}

source /usr/bin/emmabuntus_accessibility_func.sh

if [[ "${nom_editeur}" == "libreoffice" ]] ; then
    message_accessibility_6dots_writer_name="$(eval_gettext 'Starting LibreOffice word processor')"
else
    message_accessibility_6dots_writer_name="$(eval_gettext 'Starting Geany text editor')"
fi

message_accessibility_6dots_writer_info="$(eval_gettext 'to type text using the PC keyboard using braille patterns, similar to the Perkins method')"
message_accessibility_6dots_writer_info_braille="$(eval_gettext 'to type braille text with the PC keyboard using braille patterns, similar to the Perkins method')"
message_accessibility_6dots_writer_close="$(eval_gettext 'Remember to close the editing software to exit Perkins method entry mode')"
message_accessibility_error_selection_language="$(eval_gettext 'Unable to select desired language. The configuration file will be fixed.')"
message_accessibility_config_update="$(eval_gettext 'Config file fixed')"
message_accessibility_config_update_ko="$(eval_gettext 'Configuration file fixing failed.')"
message_accessibility_close_application="$(eval_gettext 'Closing the application')"

langue_pico_accessibility_6dots_writer="$(eval_gettext '#langue_pico_accessibility_6dots_writer en-US')"
langue_pico_accessibility_6dots_writer=$(echo ${langue_pico_accessibility_6dots_writer} | cut -d " " -f 2)
if [[ ${langue_pico_accessibility_6dots_writer} == "en-US" && ${Language_pico} == "en-GB" ]] ; then langue_pico_accessibility_6dots_writer="en-GB" ; fi

# Création du fichier, s'il n'existe pas
if ! test -f ${config_file} ; then

tee ${config_file} > /dev/null <<EOT
[cfg]
dot-1 = 33
dot-2 = 32
dot-3 = 31
dot-4 = 36
dot-5 = 37
dot-6 = 38
dot-7 = 44
dot-8 = 52
punctuation_key = 39
capitol_switch_key = 34
letter_deletion_key = 35
switch_between_languages = 119
list_switch_key = 56
abbreviation_key = 30
one_hand_skip_key = 20
simple-mode = 0
conventional-braille = 0
one-hand-mode = 0
one-hand-conversion-delay = 500
liblouis-mode = 0
default-language = 0
liblouis-table-list = unicode.dis,en-us-g2.ctb
checked_languages = english-en,numerical-en,french-fr,spanish-es,braille-en
EOT

fi


# Changement de la config
if test -f ${config_file} ; then

    if [[ $(cat ${config_file} | grep "checked_languages = english-en,numerical-en,french-fr,spanish-es,braille-en") ]] ; then

        if [[ "${2}" == "braille" ]]; then
            sed s/"^default-language[^$]*"/"default-language = 4"/ ${config_file} > ${config_file}.tmp
        elif [[ $LANG == fr* ]]; then
            sed s/"^default-language[^$]*"/"default-language = 2"/ ${config_file} > ${config_file}.tmp
        elif [[ $LANG == es* ]]; then
            sed s/"^default-language[^$]*"/"default-language = 3"/ ${config_file} > ${config_file}.tmp
        else
            sed s/"^default-language[^$]*"/"default-language = 0"/ ${config_file} > ${config_file}.tmp
        fi

        if test -f ${config_file}.tmp ; then
            cp ${config_file}.tmp ${config_file}
            rm ${config_file}.tmp
        fi
    else
        erreur_selection_langue=1
    fi
else
    erreur_selection_langue=1
fi

if [ ${erreur_selection_langue} -eq "1" ]; then
    sleep ${attente_lecture_message}
    message_audio_pico "${message_accessibility_error_selection_language}" "${langue_pico_accessibility_6dots_writer}"

    sed s/"^checked_languages[^$]*"/"checked_languages = english-en,numerical-en,french-fr,spanish-es,braille-en"/ ${config_file} > ${config_file}.tmp

    cp ${config_file}.tmp ${config_file}
    rm ${config_file}.tmp

    if [[ $(cat ${config_file} | grep "checked_languages = english-en,numerical-en,french-fr,spanish-es,braille-en") ]] ; then

        message_audio_pico "${message_accessibility_config_update}" "${langue_pico_accessibility_6dots_writer}"
        message_audio_pico "${message_accessibility_close_application}" "${langue_pico_accessibility_6dots_writer}"

        exit 1

    else

        message_audio_pico "${message_accessibility_config_update_ko}" "${langue_pico_accessibility_6dots_writer}"
        message_audio_pico "${message_accessibility_close_application}" "${langue_pico_accessibility_6dots_writer}"

        exit 2

    fi

fi

if [[ $(ps ax | grep -vw grep | grep ibus-braille) ]] ; then
    echo "Ibus-braille is on" >> ${fichier_init_config}
else
    echo "Ibus-braille start" >> ${fichier_init_config}
    ibus-braille &
fi

sleep ${attente_lecture_message}

if [[ "${2}" == "braille" ]]; then
    message_accessibility_6dots_writer="${message_accessibility_6dots_writer_name} ${message_accessibility_6dots_writer_info_braille}"
else
    message_accessibility_6dots_writer="${message_accessibility_6dots_writer_name} ${message_accessibility_6dots_writer_info}"
fi

message_audio_pico "${message_accessibility_6dots_writer}" "${langue_pico_accessibility_6dots_writer}"
message_audio_pico "${message_accessibility_6dots_writer_close}" "${langue_pico_accessibility_6dots_writer}"


if [[ $(ps -A | grep ${nom_editeur_bin}) ]] ; then
    echo "${nom_editeur} is on" >> ${fichier_init_config}
else
    echo "${nom_editeur} start" >> ${fichier_init_config}
    ${nom_editeur_launcher} &
fi

sleep ${attente_test_fermeture_editeur}

# Attente de la fermeture de l'éditeur de texte pour fermer ibus-braille
while ps -A | grep ${nom_editeur_bin} > /dev/null
do
    sleep ${attente_test_fermeture_editeur}
done


if [[ $(ps ax | grep -vw grep | grep ibus-braille) ]] ; then
    echo "Ibus-braille stop" >> ${fichier_init_config}
    pkill -f ibus-braille
fi

sleep 1

if [[ $(ps ax | grep -vw grep | grep ibus-braille) ]] ; then
    echo "Ibus-braille stop" >> ${fichier_init_config}
    pkill -f ibus-braille
fi

exit 0

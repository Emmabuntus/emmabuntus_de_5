��          t      �         -        ?     W  !   i  H   �     �  #   �  H     _   \  X   �  C    -   Y     �  !   �  5   �  ]   �  '   X  -   �  \   �  w     q   �                                    	              
          #langue_pico_accessibility_6dots_writer en-US Closing the application Config file fixed Configuration file fixing failed. Remember to close the editing software to exit Perkins method entry mode Starting Geany text editor Starting LibreOffice word processor Unable to select desired language. The configuration file will be fixed. to type braille text with the PC keyboard using braille patterns, similar to the Perkins method to type text using the PC keyboard using braille patterns, similar to the Perkins method Project-Id-Version: emmabuntus-accessibility-6dots-writer
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: Emmabuntüs Collective <contact@emmabuntus.org>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
 #langue_pico_accessibility_6dots_writer fr-FR Fermeture de l'application Fichier de configuration corrigé La correction du fichier de configuration a échoué. Pensez à fermer le logiciel d'édition pour quitter le mode de saisie de la méthode Perkins Démarrage de l'éditeur de texte Geany Démarrage du traitement de texte LibreOffice Impossible de sélectionner la langue souhaitée. Le fichier de configuration sera corrigé. taper du texte en braille à l'aide du clavier du PC en utilisant des motifs braille, similaires à la méthode Perkins pour taper du texte à l'aide du clavier du PC en utilisant des motifs braille, similaires à la méthode Perkins 
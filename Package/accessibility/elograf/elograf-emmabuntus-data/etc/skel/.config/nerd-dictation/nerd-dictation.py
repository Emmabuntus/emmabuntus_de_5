#!/usr/bin/env python3

# Mettez ce fichier dans le dossier ~/.config/nerd-dictation et nommez le nerd-dictation.py

# Afin d'ajouter les symboles de ponctuation lors de la dictée vocale,
# prononcer les séquences de mots ci-dessous, afin d'obtenir les symboles de ponctuation suivants :
#
# Remarque : le mot_commande par défaut est "papa", voir dessus.
# Si vous le changez cela peut induire un mauvais fonctionnement du script.
#
# - "."                        : mot_commande + point + final
# - "."                        : mot_commande + point (suivi de la prochaine lettre en majuscule)
# - ","                        : mot_commande + virgule
# - ";"                        : mot_commande + point + virgule
# - ":"                        : mot_commande + deux + points
# - "?"                        : mot_commande + interrogation (ne pas prononcer le D apostrophe avant le mot interrogation)
# - "!"                        : mot_commande + exclamation (ne pas prononcer le D apostrophe avant le mot exclamation)
# - "." avec retour à la ligne : mot_commande + point + ligne
# - retour à la ligne          : mot_commande + ligne
# - "."                        : mot_commande + point + bar

mot_commande = "papa"

def nerd_dictation_process(text):
    words = text.split(" ")
    a_suivre = False
    nouvelle_ligne = True
    point = False
    deux = False
    text_transforme = ""
    for word in words:
        if point:
            if word.startswith( "virgule"):
                #remplacer par point virgule
                text_transforme += " ; "
            elif word.startswith( "final") or word.startswith( "bar"):
                #on met le point tout seul
                text_transforme += ". "
            elif word.startswith( "ligne"):
                text_transforme += ".\r"
                nouvelle_ligne = True
            else:
                #remplacer par point Majuscule
                text_transforme += ". " + word.capitalize()
                point = False
                a_suivre = False
        elif deux:
            if word.startswith("point"):
                text_transforme += " :"
                deux = False
                a_suivre = False
        elif a_suivre:
            #  le mot commande a été prononcé
            if word == "ligne":
                #ajouter virgule
                text_transforme += "\r"
                a_suivre = False
                nouvelle_ligne = True
            elif word == "virgule":
                #ajouter virgule
                text_transforme += ","
                a_suivre = False
            elif word.startswith("interrogation"):
                #ajouter un point d'interrogation
                text_transforme += " ?"
                a_suivre = False
            elif word.startswith("exclamation"):
                #ajouter un point d'exclamation
                text_transforme += " !"
                a_suivre = False
            elif word == "point-virgule":
                text_transforme += " ;"
                a_suivre = False
            elif word.startswith("point"):
                point = True
            elif word == "deux" or word == "de":
                deux = True
            else:
                # le mot commande n'est pas suivi de mot clé. On sort le mot commande avec le suivant
                text_transforme += " " + mot_commande + " " + word
                a_suivre = False
        elif word == mot_commande:
            a_suivre = True
        else:
            if nouvelle_ligne:
               text_transforme += word.capitalize()
               nouvelle_ligne = False
            else:
                text_transforme += " " + word
    return text_transforme

#! /bin/bash

# emmabuntus_elograf_on_off.sh --
#
#   This file permits to prepare Accessibilty for the Emmabuntus Distrib.
#
#   Created on 2010-24 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 Xfce/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

. "$HOME/.config/user-dirs.dirs"

###Pour exporter la librairie de gettext.
set -a
source gettext.sh
set +a
export TEXTDOMAIN=emmabuntus-accessibility
export TEXTDOMAINDIR="/usr/share/locale"
. gettext.sh


repertoire_emmabuntus=~/.config/emmabuntus
env_emmabuntus=${repertoire_emmabuntus}/env_emmabuntus
attente_lecture_message="1"

source /usr/bin/emmabuntus_accessibility_func.sh


langue_pico_message_accueil_elograf_audio="$(eval_gettext '#langue_pico_message_accueil_elograf_audio en-US')"
langue_pico_message_accueil_elograf_audio=$(echo ${langue_pico_message_accueil_elograf_audio} | cut -d " " -f 2)

message_accueil_elograf_1="$(eval_gettext 'Elograf is voice dictation software.')"

message_accueil_elograf_2="$(eval_gettext 'Currently it is only configured for the French language.')"

message_accueil_elograf_3="$(eval_gettext 'To use it click on Activate or press the \042Enter\042 key, otherwise press the \042Q\042 key to quit,')"

message_accueil_elograf_4="$(eval_gettext 'then open either the Geany text editor using the Alt Super \042G\042 shortcut or Libre Office Writer uising the Super \042O\042 shortcut.')"

message_accueil_elograf_5="$(eval_gettext 'To add punctuation, consult the shortcuts manual.')"

message_accueil_elograf_6="$(eval_gettext 'To remove this window on the next launch, uncheck the option, or press the \042D\042 key.')"

message_accueil_elograf_7="$(eval_gettext 'To stop Elograf from operating in the system tray, restart it with the Alt Super E shortcut.')"

message_elograf_start="$(eval_gettext 'Elograf launched')"
message_elograf_stop="$(eval_gettext 'Elograf shutdown')"

message_accueil_elograf_audio="${message_accueil_elograf_1}
${message_accueil_elograf_2}
${message_accueil_elograf_3}
${message_accueil_elograf_4}
${message_accueil_elograf_5}
${message_accueil_elograf_6}
${message_accueil_elograf_7}"

message_accueil_elograf_visuel="\n\
${message_accueil_elograf_1}\n\
\n\
${message_accueil_elograf_2}\n\
\n\
${message_accueil_elograf_3}\n\
${message_accueil_elograf_4}\n\
\n\
${message_accueil_elograf_5}\n\
\n\
${message_accueil_elograf_6}\n\
\n\
${message_accueil_elograf_7}"

export message_accueil_elograf_audio
export message_accueil_elograf_visuel

# Si Orca d'activé attendre x secondes avant de lire le message d'information sur la modification du son
if [[ $(ps -A | grep orca) ]] ; then
    sleep ${attente_lecture_message}
fi

if [[ $(ps -A | grep "elograf") ]] ; then

    # Lancement de la lecture audio en parallèle
    message_audio_pico "${message_elograf_stop}" "${langue_pico_message_accueil_elograf_audio}" &

    nerd-dictation end

    if [[ $(ps -A | grep "elograf") ]] ; then
        killall elograf > /dev/null
    fi

else

    #  chargement des variables d'environnement
    . ${env_emmabuntus}

    if [[ ${langue_pico_message_accueil_elograf_audio} == "en-US" && ${Language_pico} == "en-GB" ]] ; then langue_pico_message_accueil_elograf_audio="en-GB" ; fi

    if [[ ${Elograf} == "1" ]] ; then
        ELOGRAF="true"
    else
        ELOGRAF="false"
    fi

    export ELOGRAF

    if [[ ${Elograf_display} == "1" ]] ; then
        ELOGRAF_DISPLAY="true"
        export ELOGRAF_DISPLAY

        # Lancement de la lecture audio en parallèle
        message_audio_pico "${message_accueil_elograf_audio}" "${langue_pico_message_accueil_elograf_audio}" &

        /usr/bin/emmabuntus_elograf_window.sh

    fi

    #  chargement des variables d'environnement
    . ${env_emmabuntus}

    if [[ ${Elograf} == "1" ]] ; then

        # Lancement de la lecture audio en parallèle
        message_audio_pico "${message_elograf_start}" "${langue_pico_message_accueil_elograf_audio}" &

        elograf --start &
    fi

    update_config_emmabuntus Elograf "true" ${env_emmabuntus}

fi

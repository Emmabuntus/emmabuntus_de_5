#! /bin/bash

# emmabuntus_elograf_window_audio.sh --
#
#   This file permits to prepare Accessibilty for the Emmabuntus Distrib.
#
#   Created on 2010-24 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 Xfce/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

###Pour exporter la librairie de gettext.
set -a
source gettext.sh
set +a
export TEXTDOMAIN=emmabuntus-accessibility
export TEXTDOMAINDIR="/usr/share/locale"
. gettext.sh

repertoire_emmabuntus=~/.config/emmabuntus
env_emmabuntus=${repertoire_emmabuntus}/env_emmabuntus
fichier_init_config=${repertoire_emmabuntus}/emmabuntus_elograf_window.txt

#  chargement des variables d'environnement
. ${env_emmabuntus}

source /usr/bin/emmabuntus_accessibility_func.sh

langue_pico_elograf_window_audio="$(eval_gettext '#langue_pico_elograf_window_audio en-US')"
langue_pico_elograf_window_audio=$(echo ${langue_pico_elograf_window_audio} | cut -d " " -f 2)
if [[ ${langue_pico_elograf_window_audio} == "en-US" && ${Language_pico} == "en-GB" ]] ; then langue_pico_elograf_window_audio="en-GB" ; fi

# Touche valides :
# r : Relecture de l'aide,
# Espace : Arrêt lecture audio, Enter : Valider la fenêtre, q : Quitter la fenêtre

key="$(eval_gettext 'Key')"
function_on="$(eval_gettext 'enabled')"
function_off="$(eval_gettext 'disabled')"
msg_elograf_display="$(eval_gettext 'window')"
function_validation="$(eval_gettext 'configuration validation')"
function_not_available="$(eval_gettext 'function not available')"
message=""

re_is_integer='^[0-9]+$'

key_symbol=${1}
key_mod=${2}
elograf_display_value=${3}

key_symbol_maj=$(echo ${key_symbol} | tr [a-z] [A-Z])

echo "key_symbol=${key_symbol} : key_mod=${2} : elograf_display_value=${3}" >> ${fichier_init_config}


message_audio_key () {

    function=${1}
    value=${2}
    status=${3}

    if [ "${value}" == "true" ] ; then
        variable="false"
        if [[ ${status} != "" ]] ; then
            message="${function} ${function_on}"
        else
            message="${key} ${key_symbol_maj} ${function} ${function_off}"
        fi
    else
        variable="true"
        if [[ ${status} != "" ]] ; then
            message="${function} ${function_off}"
        else
            message="${key} ${key_symbol_maj} ${function} ${function_on}"
        fi
    fi

    message_audio_pico "${message}" "${langue_pico_elograf_window_audio}"

    echo "${variable}"

}

if [[ ${key_symbol_maj} == "D" ]] ; then

    ELOGRAF_DISPLAY=$(message_audio_key "${msg_elograf_display}" ${elograf_display_value})

    pkill gtkdialog > /dev/null
    /usr/bin/emmabuntus_elograf_window.sh

elif [[ ${key_symbol_maj} == "R" ]] ; then

    message_audio_pico "${message_accueil_elograf_audio}" "${langue_pico_elograf_window_audio}" &

elif [[ ${key_symbol} == "Escape" ]] ; then

    if [[ $(ps -A | grep aplay) ]] ; then pkill aplay > /dev/null ; fi

elif [[ ${key_symbol_maj} == "Q" ]] ; then

    pkill gtkdialog > /dev/null
    /usr/bin/emmabuntus_elograf_window.sh &

    while true; do sleep 0.2; if [[ $(ps axg | grep -vw grep | grep -w gtkdialog) ]] ; then pkill gtkdialog; break; fi; done

elif [[ ${key_symbol} == "Return" ]] ; then

    update_config_emmabuntus Elograf_display ${elograf_display_value} ${env_emmabuntus}

    message="${key} ${key_symbol} ${function_validation}"
    message_audio_pico "${message}" "${langue_pico_elograf_window_audio}"
    pkill gtkdialog > /dev/null

else

    message="${key} ${key_symbol_maj} ${function_not_available}"
    message_audio_pico "${message}" "${langue_pico_elograf_window_audio}"

fi


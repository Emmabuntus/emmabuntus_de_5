#! /bin/bash

# emmabuntus_elograf_windows.sh --
#
#   This file permits to prepare Accessibilty for the Emmabuntus Distrib.
#
#   Created on 2010-24 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 Xfce/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################


. "$HOME/.config/user-dirs.dirs"


###Pour exporter la librairie de gettext.
set -a
source gettext.sh
set +a
export TEXTDOMAIN=emmabuntus-accessibility
export TEXTDOMAINDIR="/usr/share/locale"
. gettext.sh

repertoire_emmabuntus=~/.config/emmabuntus
env_emmabuntus=${repertoire_emmabuntus}/env_emmabuntus
fichier_init_config=${repertoire_emmabuntus}/emmabuntus_elograf_window.txt

nom_fenetre="$(eval_gettext 'Elograf')"

msg_elograf_display=$(eval_gettext 'Show this window at next startup')
btn_decline=$(eval_gettext 'Decline ')
btn_activate=$(eval_gettext 'Activate  ')

WIDTH_REQUEST_WINDOW="800"

export WINDOW_DIALOG_WELCOME='<window title="'${nom_fenetre}'" icon-name="gtk-info" resizable="false">
<vbox spacing="0">

<text use-markup="true" wrap="false" xalign="0" justify="3">
<input>echo "'$message_accueil_elograf_visuel'" | sed "s%\\\%%g"</input>
</text>

<hseparator space-expand="true" space-fill="true"></hseparator>

  <hbox>

    <checkbox active="'${ELOGRAF_DISPLAY}'">
    <variable>ELOGRAF_DISPLAY</variable>
    <label>'${msg_elograf_display}'</label>
    </checkbox>

   <text><label>"                                                                                          "</label></text>

    <hbox>
    <button>
    <label>"'${btn_decline}'"</label>
    <input file stock="gtk-cancel"></input>
    <action>exit:Cancel</action>
    </button>

    <button can-default="true" has-default="true" use-stock="true" is-focus="true">
    <label>"'${btn_activate}'"</label>
    <input file stock="gtk-ok"></input>
    <action>exit:OK</action>
    </button>
    </hbox>

  </hbox>

</vbox>

<action signal="key-press-event">/usr/bin/emmabuntus_elograf_window_audio.sh ${KEY_SYM} ${KEY_MOD} ${ELOGRAF_DISPLAY}</action>

</window>'

#gtkdialog --center --program=WINDOW_DIALOG_WELCOME

MENU_DIALOG_WELCOME="$(gtkdialog --center --program=WINDOW_DIALOG_WELCOME)"

echo "MENU_DIALOG_WELCOME=$MENU_DIALOG_WELCOME"  >> ${fichier_init_config}

eval ${MENU_DIALOG_WELCOME}

if [[ $(ps -A | grep aplay) ]] ; then pkill aplay > /dev/null ; fi

source /usr/bin/emmabuntus_accessibility_func.sh


if [[ ${EXIT} == "OK" ]] ; then

    update_config_emmabuntus Elograf "true" ${env_emmabuntus}
    update_config_emmabuntus Elograf_display ${ELOGRAF_DISPLAY} ${env_emmabuntus}

else

    update_config_emmabuntus Elograf "false" ${env_emmabuntus}
    update_config_emmabuntus Elograf_display ${ELOGRAF_DISPLAY} ${env_emmabuntus}

fi

pkill -f emmabuntus_elograf_window > /dev/null

if [[ ${EXIT} == "Cancel" ]] ; then
    exit 1
else
    exit 0
fi

#! /bin/bash

# Méthode paquet origine
nom=tuxtype-emmabuntus-data
version=1.0.0
nom_paquet=${nom}_${version}_all.deb

# Méthode assemblage
dpkg-deb -b ${nom} ${nom_paquet}

if test -f ${nom_paquet} ; then

cp ${nom_paquet} ../../../config/includes.chroot/Install/accessibility/

rm ${nom_paquet}

else
    echo "Paquet non généré"
fi

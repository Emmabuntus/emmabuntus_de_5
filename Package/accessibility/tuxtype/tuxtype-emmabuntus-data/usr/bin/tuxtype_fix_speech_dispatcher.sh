#!/bin/bash

# tuxtype_fix_speech_dispatcher.sh --
#
#   This file permits to launch tuxtype for accessibilty for the Emmabuntus Distrib.
#
#   Created on 2010-24 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 Xfce/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

file=/etc/speech-dispatcher/speechd.conf

orca_on=0
speech_dispatcher_on=0
speech_dispatcher_pico_on=0

if [[ $(ps -A | grep "orca") ]] ; then
    orca_on=1
fi

if [[ $(ps -A | grep "speech-dispatch") ]] ; then
    speech_dispatcher_on=1
    pkill speech-dispatch
    sleep 2
fi

if [[ $(cat ${file} | grep "^DefaultModule pico") ]] ; then
    speech_dispatcher_pico_on=1
    sudo /usr/bin/emmabuntus_accessibility_speech_dispatcher_update_exec.sh
fi

tuxtype --tts

if [[ $(ps -A | grep "speech-dispatch") ]] ; then
    pkill speech-dispatch
    sleep 1
fi

if [ ${speech_dispatcher_pico_on} -eq 1 ] ; then
    sudo /usr/bin/emmabuntus_accessibility_speech_dispatcher_update_exec.sh "pico"
fi

if [ ${speech_dispatcher_on} -eq 1 ] ; then
    speech-dispatcher -d
fi

if [ ${orca_on} -eq 1 ] ; then
    orca -r &
fi



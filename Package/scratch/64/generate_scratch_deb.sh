#!/bin/bash

dir_work_scratch=/tmp

# Install electron
sudo apt install -y -qq npm gdebi p7zip-full
npm install electron --save-dev
ls ./node_modules/electron/dist

# Extract scratch-desktop
rm -rf ${dir_work_scratch}/scratch-desktop
mkdir ${dir_work_scratch}/scratch-desktop
wget --max-redirect 5 -c -O ${dir_work_scratch}/scratch-desktop.exe 'https://downloads.scratch.mit.edu/desktop/Scratch%20Setup.exe'
7za x -aoa -y ${dir_work_scratch}/scratch-desktop.exe -o${dir_work_scratch}/scratch-desktop

# Create and launch electron app
cp -rf node_modules/electron/dist/* ${dir_work_scratch}/scratch-desktop/
ln -fsr ${dir_work_scratch}/scratch-desktop/electron ${dir_work_scratch}/scratch-desktop/scratch-desktop
${dir_work_scratch}/scratch-desktop/scratch-desktop

# Fix permissions
chmod 755 ${dir_work_scratch}/scratch-desktop/locales
chmod 755 ${dir_work_scratch}/scratch-desktop/swiftshader
chmod 755 ${dir_work_scratch}/scratch-desktop/resources
chmod 755 ${dir_work_scratch}/scratch-desktop/resources/static
chmod 755 ${dir_work_scratch}/scratch-desktop/resources/static/assets

# Get application icon
wget -c -O ${dir_work_scratch}/scratch-desktop/resources/Icon.png 'https://scratch.mit.edu/images/download/icon.png'

######## create deb package
npm install electron-installer-debian
wget -c -O ${dir_work_scratch}/config-deb.json 'https://gist.githubusercontent.com/lyshie/0c49393076b8b375ca1bd98c28f95fb0/raw/223cc112d99c3bdc1829fc25b19c260856134a82/config-deb.json'
./node_modules/.bin/electron-installer-debian --config ${dir_work_scratch}/config-deb.json
cp /tmp/scratch-desktop_*.deb .


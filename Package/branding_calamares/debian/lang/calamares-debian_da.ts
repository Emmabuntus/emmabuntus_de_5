<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="da">
<context>
    <name>show</name>
    <message>
        <location filename="../show.qml" line="45"/>
        <source>Welcome to Debian 12 GNU/Linux - Emmabuntus DE 5.&lt;br/&gt;The rest of the installation is automated and should complete in a few minutes.</source>
        <translation>Velkommen til Debian 12 GNU/Linux - Emmabuntus DE 5. &lt;br/&gt; Resten af ​​installationen er automatiseret og vil blive gennemført i løbet af nogle minutter.</translation>
    </message>
    <message>
        <location filename="../show.qml" line="67"/>
        <source>More customization available through the post-installation process.</source>
    <translation>Mere tilpasning er tilgængelig efter installationen.</translation>
    </message>
    <message>
        <location filename="../show.qml" line="88"/>
        <source>More accessibility for everyone, thanks to the three dock levels !</source>
        <translation>Nemme genveje for alle takket være de tre dock-niveauer !</translation>
    </message>
    <message>
        <location filename="../show.qml" line="109"/>
        <source>More than 60 software programs available, which allow the beginners to discover GNU/Linux.</source>
        <translation>Mere end 60 tilgængelige softwareprogrammer, der giver begyndere mulighed for at lære GNU/Linux.</translation>
    </message>
    <message>
        <location filename="../show.qml" line="130"/>
        <source>More than 10 edutainment software programs for training, including Kiwix the offline Wikipedia reader.</source>
        <translation>Mere end 10 uddannelse softwareprogrammer, herunder Kiwix offline Wikipedia-læser.</translation>
    </message>
    <message>
        <location filename="../show.qml" line="151"/>
        <source>Emmabuntüs allows blind and visually impaired people to use their computer.</source>
        <translation>Emmabuntüs giver blinde og synshandicappede mulighed for at bruge deres computer.</translation>
    </message>
    <message>
        <location filename="../show.qml" line="171"/>
        <source>Emmabuntus supports social and environmental projects through the Lilo ethical and solidarity research engine.</source>
        <translation>Emmabuntus understøtter sociale og miljømæssige projekter gennem Lilo etiske og solidariske forskning søgemaskine.</translation>
    </message>
        <message>
        <location filename="../show.qml" line="191"/>
        <source>Emmabuntus collaborates with more than 10 associations, particularly in Africa, in order to reduce the digital divide.</source>
        <translation>Emmabuntus samarbejder med mere end 10 foreninger, især i Afrika, for at reducere den digitale kløft.</translation>
    </message>
    <message>
        <location filename="../show.qml" line="211"/>
        <source>Emmabuntus together with YovoTogo and JUMP Lab'Orione have equipped and run 33 computer rooms in Togo.</source>
        <translation>Emmabuntus har sammen med YovoTogo og JUMP Lab'Orione udstyret og undervist i 33 computerlokaler i Togo.</translation>
    </message>
    <message>
        <location filename="../show.qml" line="231"/>
        <source>Emmabuntus is also a refurbishing key, based on Ventoy,&lt;br/&gt;allowing you to quickly and easily refurbish a computer with GNU/Linux.</source>
        <translation>Emmabuntus er også en renoveringsnøgle, baseret på Ventoy,&lt;br/&gt;som giver dig mulighed for hurtigt og nemt at renovere en computer med GNU/Linux.</translation>
    </message>
</context>
</TS>

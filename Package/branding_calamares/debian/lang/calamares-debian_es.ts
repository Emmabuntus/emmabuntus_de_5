<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es">
<context>
    <name>show</name>
    <message>
        <location filename="../show.qml" line="45"/>
        <source>Welcome to Debian 12 GNU/Linux - Emmabuntus DE 5.&lt;br/&gt;The rest of the installation is automated and should complete in a few minutes.</source>
        <translation>Bienvenido a  Debian 12 GNU/Linux - Emmabuntüs DE 5.&lt;br/&gt;El resto de la instalación está automatizada y debería completarse en unos minutos.</translation>
    </message>
    <message>
        <location filename="../show.qml" line="67"/>
        <source>More customization available through the post-installation process.</source>
        <translation>Más opciones de personalización están disponibles a través del proceso posterior a la instalación.</translation>
    </message>
    <message>
        <location filename="../show.qml" line="89"/>
        <source>More accessibility for everyone, thanks to the three dock levels !</source>
        <translation>¡Más accesibilidad para todos, gracias a los tres niveles de dock!</translation>
    </message>
    <message>
        <location filename="../show.qml" line="109"/>
        <source>More than 60 software programs available, which allow the beginners to discover GNU/Linux.</source>
        <translation>Más de 60 programas de software disponibles, que permiten a los principiantes descubrir GNU/Linux.</translation>
    </message>
    <message>
        <location filename="../show.qml" line="130"/>
        <source>More than 10 edutainment software programs for training, including Kiwix the offline Wikipedia reader.</source>
        <translation>Más de 10 programas de software de entretenimiento educativo para capacitación, incluido Kiwix, el lector de Wikipedia fuera de línea.</translation>
    </message>
    <message>
        <location filename="../show.qml" line="151"/>
        <source>Emmabuntüs allows blind and visually impaired people to use their computer.</source>
        <translation>Emmabuntüs permite a personas ciegas y con discapacidad visual utilizar su ordenador.</translation>
    </message>
    <message>
        <location filename="../show.qml" line="171"/>
        <source>Emmabuntus supports social and environmental projects through the Lilo ethical and solidarity research engine.</source>
        <translation>Emmabuntüs apoya proyectos sociales y ambientales a través del motor de investigación ética y solidaria de Lilo.</translation>
    </message>
        <message>
        <location filename="../show.qml" line="191"/>
        <source>Emmabuntus collaborates with more than 10 associations, particularly in Africa, in order to reduce the digital divide.</source>
        <translation>Emmabuntüs colabora con más de 10 asociaciones, particularmente en África, para reducir la brecha digital.</translation>
    </message>
    <message>
        <location filename="../show.qml" line="211"/>
        <source>Emmabuntus together with YovoTogo and JUMP Lab'Orione have equipped and run 33 computer rooms in Togo.</source>
        <translation>Emmabuntüs junto con YovoTogo y JUMP Lab'Orione han equipado y administrado 33 salas de computadoras en Togo.</translation>
    </message>
    <message>
        <location filename="../show.qml" line="231"/>
        <source>Emmabuntus is also a refurbishing key, based on Ventoy,&lt;br/&gt;allowing you to quickly and easily refurbish a computer with GNU/Linux.</source>
        <translation>Emmabuntüs es también una clave de restauración, basada en Ventoy,&lt;br/&gt;que le permite restaurar rápida y fácilmente una computadora con GNU/Linux.</translation>
    </message>
</context>
</TS>

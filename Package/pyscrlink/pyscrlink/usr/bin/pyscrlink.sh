#! /bin/bash

# pyscrlink.sh --
#
#   This file permit to install and run pyscrlink,
#   see https://github.com/kawasaki/pyscrlink
#
#   Created on 2010-23 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was checked and validated on Debian 12 XFCE/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

repertoire_py=.local/py3
repertoire_work=/home/${USER}/${repertoire_py}

GREEN='\033[0;32m'
RED='\033[0;31m'
ORANGE='\033[0;33m'
NC='\033[0m' # No Color
LANGUE=$(echo ${LANG} | cut -d _ -f1)

if [ ${LANGUE} = fr ] ; then
    message_live="\nLa fonction n'est pas disponible en mode live\n"
    message_not_internet="\nInternet n'est pas accessible\nL'installation n'est pas possible\n"
    message_install="\nInstallation de Py Scratch Link ...\nNe fermez pas le terminal pendant l'installation\n"
    message_start="\nPy Scratch Link a démarré ...\nNe fermez pas ce terminal tant que Scratch n'est pas terminé\n"
    message_error="\nPy Scratch Link n'est pas installé"
    message_pause="Tapez \"Entrée\" pour arrêter"
else
    message_live="\nTs function is not available in live mode\n"
    message_not_internet="\nInternet is not available\nInstallation not possible\n"
    message_install="\nInstall Py Scratch Link ...\nDon't close the terminal during install\n"
    message_start="\nPy Scratch Link started ...\nDon't close this terminal until Scratch is complete\n"
    message_error="\nPy Scratch Link is not installed"
    message_pause="Type \"Enter\" to stop"
fi

if [[ $(cat /proc/cmdline | grep -i boot=live) ]]
then

    echo -e "${RED}${message_live}${NC}"
    echo -e "${ORANGE}${message_pause}${NC}"
    read pause
    exit 1
fi


ping -q -c 2 sourceforge.net >/dev/null 2>&1

if [ $? -eq 0 ]; then

    if ! test -d ${repertoire_work} ; then

        echo -e "${GREEN}${message_install}${NC}"
        python3 -m venv ${repertoire_work}
        ${repertoire_work}/bin/pip3 install pyscrlink

        ${repertoire_work}/bin/bluepy_helper_cap

    fi


    if test -f ${repertoire_work}/bin/scratch_link ; then

        echo -e "${GREEN}${message_start}${NC}"
        ${repertoire_work}/bin/scratch_link

    else

        echo -e "${RED}${message_error}${NC}"
        echo -e "${ORANGE}${message_pause}${NC}"
        read pause

    fi

else

    echo -e "${RED}${message_not_internet}${NC}"
    echo -e "${ORANGE}${message_pause}${NC}"
    read pause

fi

# Emmabuntüs Debian Edition makefile basé sur le script d'HandyLinux (collectif Emmabuntüs <contact@emmabuntus.org>)
#
# This script need to install packages : livebuild, apt-cacher-ng, etc and to configure to work correctly
#
# To install these tools make : sudo apt-get update && sudo apt-get install live-build live-manual live-tools git apt-cacher-ng dpkg-dev
#
# To assemble these iso make : sudo apt-get update && sudo apt-get install xorriso isolinux
#

NOM_VERSION_EMMABUNTUS="EmmaDE5"
NOM_VERSION_EMMABUNTUS_LONG="Emmabuntus\ DE\ 5"
NOM_VERSION_EMMABUNTUS_ISO="emmabuntus-de5"
VERSION_EMMABUNTUS=1.04
NOM_VERSION_DEBIAN=bookworm
NOM_VERSION_DEBIAN_ISO=12.9

NOM_PUBLISHER="Emmabuntus\ project\;\ https:\/\/emmabuntus.org"

LIVE_BUILD_VERSION=$(shell lb --version)
NOM_TOOL="live-build"

PARTMAN=partman-auto_
PARTMAN_VERSION=162

PARTMAN_NB_FILE=$(shell ls -1q cache/packages.installer_debian-installer.udeb/$(PARTMAN)* 2>/dev/null | wc -l)
DATE=$(shell date '+%Y%m%d%H%M%S'00)

ifeq ($(MAKECMDGOALS),32b)
ARCH=i386
ARCH_ISO=i686
VERSION=32
endif

ifeq ($(MAKECMDGOALS),32s)
ARCH=i386
ARCH_ISO=i686
VERSION=32
endif

ifeq ($(MAKECMDGOALS),64b)
ARCH=amd64
ARCH_ISO=amd64
VERSION=64
endif

ifeq ($(MAKECMDGOALS),64s)
ARCH=amd64
ARCH_ISO=amd64
VERSION=64
endif




all:
	@echo "Usage: as root"
	@echo "make 32b       : build $(NOM_VERSION_EMMABUNTUS) i686"
	@echo "make 64b       : build $(NOM_VERSION_EMMABUNTUS) amd64"
	@echo "make 32s       : build $(NOM_VERSION_EMMABUNTUS) i686 and stop the computer after"
	@echo "make 64s       : build $(NOM_VERSION_EMMABUNTUS) amd64  and stop the computer after"
	@echo "make clean     : clean up i386 or amd64 build directories"
	@echo "make cleanfull : clean up log & cache directories"

arch32:
	@echo "-------------------------"
	@echo "building Emmabuntüs DE i686"
	@echo "-------------------------"
	@echo "distro=EmmaDE_i386" > config/includes.chroot/Install/env_arch.sh


arch64:
	@echo "--------------------------"
	@echo "building Emmabuntüs DE amd64"
	@echo "--------------------------"
	@echo "distro=EmmaDE_amd64" > config/includes.chroot/Install/env_arch.sh

verif:
	@if dpkg --get-selections live-build | grep -e "\<install\>" 1> /dev/null; then echo>/dev/null; \
	else echo "Error: Build stop because live-build isn't install !!!"; exit 1;  fi
	@if dpkg --get-selections live-tools | grep -e "\<install\>" 1> /dev/null; then echo>/dev/null; \
	else echo "Error: Build stop because live-tools isn't install !!!"; exit 2;  fi
	@if dpkg --get-selections dpkg-dev | grep -e "\<install\>" 1> /dev/null; then echo>/dev/null; \
	else echo "Error: Build stop because dpkg-dev isn't install !!!"; exit 3;  fi
	@if dpkg --get-selections xorriso | grep -e "\<install\>" 1> /dev/null; then echo>/dev/null; \
	else echo "Error: Build stop because xorriso isn't install !!!"; exit 4;  fi
	@if dpkg --get-selections isolinux | grep -e "\<install\>" 1> /dev/null; then echo>/dev/null; \
	else echo "Error: Build stop because isolinux isn't install !!!"; exit 5;  fi
	@if ps -A | grep apt-cacher-ng 1> /dev/null; then echo>/dev/null; \
	else echo "Warning: apt-cacher-ng isn't install or started !!!"; fi
	@if [ $(PARTMAN_NB_FILE) -gt 1 ]; then echo "Error: Build stop because PARTMAN have multiple version in cache !!!"; exit 6; fi
	@if test -d cache/packages.installer_debian-installer.udeb ; \
		then if test -e cache/packages.installer_debian-installer.udeb/$(PARTMAN)$(PARTMAN_VERSION)_$(ARCH).udeb; then echo>/dev/null; \
		else echo "cache/packages.installer_debian-installer.udeb/$(PARTMAN)$(PARTMAN_VERSION)_$(ARCH).udeb" ; \
			echo "Error: Build stop because PARTMAN not the same version in cache and patch !!!"; exit 7;  fi; fi

install:
	@cp CHANGELOG config/includes.binary/CHANGELOG
	@cp CHANGELOG-FR config/includes.binary/CHANGELOG-FR
	@cp LICENSE config/includes.binary/LICENSE
	@cp auto/config$(VERSION) auto/config
	@cp arch/$(VERSION)/logo+emerald.png config/includes.chroot/usr/share/plymouth/themes/emerald/logo+emerald.png
	@if ! test -d cache/packages.installer_debian-installer.udeb; \
		then if ! test -d cache; then mkdir cache; fi; \
			if ! test -d cache/packages.installer_debian-installer.udeb; \
				then mkdir cache/packages.installer_debian-installer.udeb; fi ; fi
	@cp arch/$(VERSION)/packages.installer_debian-installer.udeb/* cache/packages.installer_debian-installer.udeb
	@if test -f cache/packages.installer_debian-installer.udeb/$(PARTMAN)_$(ARCH).udeb; \
		then chmod a-w cache/packages.installer_debian-installer.udeb/$(PARTMAN)_$(ARCH).udeb; fi
	@if ! test -d config/includes.binary/.disk; \
		then mkdir config/includes.binary/.disk; fi
	@cp -R arch/$(VERSION)/.disk/* config/includes.binary/.disk
	@cp -R arch/$(VERSION)/opt/* config/includes.chroot/opt
	@cp -R arch/$(VERSION)/config_efi/* config/includes.binary
	@cp arch/$(VERSION)/.disk/info config/includes.chroot/etc/skel/.config/emmabuntus/info
	@if ! test -d config/includes.chroot/usr/share/locale; \
		then mkdir config/includes.chroot/usr/share/locale; fi
	@if  test -d Languages/locale; \
		then cp -R Languages/locale/* config/includes.chroot/usr/share/locale; fi
	@find config/includes.chroot/usr/share/locale -name *.po -exec rm "{}" \;
	@find config/includes.chroot/usr/share/locale -name *.*.mo -exec rm "{}" \;
	@if  test -d config/includes.chroot/usr/share/locale/en; \
		then rm -r config/includes.chroot/usr/share/locale/en; fi

	lb build

	@echo "Move pool-udeb ----------------------"
	@if test -d binary/pool-udeb; \
		then cp -R binary/pool-udeb/* binary/pool; \
		rm -fr binary/pool-udeb; fi

	@echo "Updated package Kernel main ----------------------"
	@dpkg-scanpackages binary/pool/main > arch/$(VERSION)/config_efi/dists/$(NOM_VERSION_DEBIAN)/main/binary-$(ARCH)/Packages.tmp
	@sed s/^Filename:\ binary\\//Filename:\ / arch/$(VERSION)/config_efi/dists/$(NOM_VERSION_DEBIAN)/main/binary-$(ARCH)/Packages.tmp > arch/$(VERSION)/config_efi/dists/$(NOM_VERSION_DEBIAN)/main/binary-$(ARCH)/Packages
	@rm arch/$(VERSION)/config_efi/dists/$(NOM_VERSION_DEBIAN)/main/binary-$(ARCH)/Packages.tmp
	@cp arch/$(VERSION)/config_efi/dists/$(NOM_VERSION_DEBIAN)/main/binary-$(ARCH)/Packages binary/dists/$(NOM_VERSION_DEBIAN)/main/binary-$(ARCH)/Packages
	@gzip -kf binary/dists/$(NOM_VERSION_DEBIAN)/main/binary-$(ARCH)/Packages

	@echo "Updated Kernel ----------------------"
	@cp -R arch/Install_kernel/$(VERSION)/install/* binary/install/
	@rm -f binary/pool/main/l/linux-signed-$(ARCH)/*
	@cp -R arch/Install_linux_package/$(VERSION)/* binary/pool/main/l/linux-signed-$(ARCH)/

	@echo "Updated package installer ----------------------"
	@dpkg-scanpackages -t udeb binary/pool/main > binary/dists/$(NOM_VERSION_DEBIAN)/main/debian-installer/binary-$(ARCH)/Packages.tmp
	@sed s/^Filename:\ binary\\//Filename:\ / binary/dists/$(NOM_VERSION_DEBIAN)/main/debian-installer/binary-$(ARCH)/Packages.tmp > binary/dists/$(NOM_VERSION_DEBIAN)/main/debian-installer/binary-$(ARCH)/Packages
	@rm binary/dists/$(NOM_VERSION_DEBIAN)/main/debian-installer/binary-$(ARCH)/Packages.tmp
	@gzip -kf binary/dists/$(NOM_VERSION_DEBIAN)/main/debian-installer/binary-$(ARCH)/Packages

	@echo "Removed Kernel live duplicate ----------------------"
	@mv -f binary/live/initrd.img-* binary/live/initrd.img
	@mv -f binary/live/vmlinuz-* binary/live/vmlinuz
	@rm -f binary/live/filesystem.packages
	@rm -f binary/live/filesystem.packages-remove

	@echo "Updated Kernel Firmware ----------------------"
	@rm -f binary/firmware/*
	@if ! test -d binary/firmware; then mkdir binary/firmware; fi
	@if test -d binary/pool/non-free; then find binary/pool/non-free -type f -name '*.deb' -exec cp {} binary/firmware \;; fi
	@if test -d binary/pool/main/f/firmware-free; then cp binary/pool/main/f/firmware-free/* binary/firmware ; fi
	@if test -d binary/pool/main/h/hdmi2usb-fx2-firmware; then cp binary/pool/main/h/hdmi2usb-fx2-firmware/* binary/firmware ; fi
	@if test -d binary/pool/main/i/ixo-usb-jtag; then cp binary/pool/main/i/ixo-usb-jtag/* binary/firmware ; fi
	@rm -fr binary/pool/non-free/*

	@echo "Calculated release file ----------------------"
	@rm -f binary/dists/$(NOM_VERSION_DEBIAN)/Release
	@rm -fr binary/dists/$(NOM_VERSION_DEBIAN)/contrib
	@rm -fr binary/dists/$(NOM_VERSION_DEBIAN)/non-free
	@rm -fr binary/pool/contrib
	@rm -fr binary/pool/non-free
	@apt-ftparchive release \
	-o APT::FTPArchive::Release::Architectures=$(ARCH) \
	-o APT::FTPArchive::Release::Codename=$(NOM_VERSION_DEBIAN) \
	-o APT::FTPArchive::Release::Components=main \
	-o APT::FTPArchive::Release::Label=Debian \
	-o APT::FTPArchive::Release::Origin=Debian \
	-o APT::FTPArchive::Release::Suite=stable \
	binary/dists/$(NOM_VERSION_DEBIAN) > Release
	@cp Release binary/dists/$(NOM_VERSION_DEBIAN)/Release
	@rm -f Release

	@echo "Calculated checksum ----------------------"
	@rm -f binary/md5sum.txt
	@find binary -type f -exec md5sum "{}" + > md5sum.tmp
	@sed s/\ binary/\ ./ md5sum.tmp > binary/md5sum.txt
	@md5sum -c --quiet md5sum.tmp
	@rm md5sum.tmp

	@echo "Generated new ISO  ----------------------"
	@rm -f ./live-image-*.iso
	@echo "DATE = $(DATE)"

	# Copy image files needed by grub-pc launcher
	@cp /usr/lib/grub/i386-pc/boot_hybrid.img binary/boot/grub/i386-pc

	@xorriso -as mkisofs -R -r -J -joliet-long -l -cache-inodes -iso-level 3 -partition_offset 16 -follow-links \
     -A "$(NOM_VERSION_EMMABUNTUS_LONG)" -p "$(NOM_TOOL):$(LIVE_BUILD_VERSION)" -publisher "$(NOM_PUBLISHER)" \
     -V "$(NOM_VERSION_EMMABUNTUS)_$(VERSION_EMMABUNTUS)_$(ARCH_ISO)" --modification-date=$(DATE) \
     -o $(NOM_VERSION_EMMABUNTUS_ISO)-$(ARCH_ISO)-$(NOM_VERSION_DEBIAN_ISO)-$(VERSION_EMMABUNTUS).iso \
     --grub2-mbr binary/boot/grub/i386-pc/boot_hybrid.img \
     --mbr-force-bootable \
     -append_partition 2 28732ac11ff8d211ba4b00a0c93ec93b binary/boot/grub/efi.img \
     -appended_part_as_gpt \
     -c 'boot.catalog' \
     -b 'boot/grub/grub_eltorito' \
       -no-emul-boot -boot-load-size 4 -boot-info-table --grub2-boot-info \
     -eltorito-alt-boot \
     -e '--interval:appended_partition_2:::' \
       -no-emul-boot \
     binary

	@xorriso -indev $(NOM_VERSION_EMMABUNTUS_ISO)-$(ARCH_ISO)-$(NOM_VERSION_DEBIAN_ISO)-$(VERSION_EMMABUNTUS).iso -report_system_area plain

	@./control_assemblage.sh

32b: arch32 verif install

64b: arch64 verif install

stop:
	@echo "-------------------------"
	@echo "Arrêt du système dans 5 minutes"
	@echo "-------------------------"
	@shutdown +5

32s: arch32 verif install stop

64s: arch64 verif install stop

clean_all:
	@echo "--------------------------"
	@echo "cleaning build directories"
	@echo "--------------------------"
	@if test -f config/includes.binary/CHANGELOG; then rm config/includes.binary/CHANGELOG; fi
	@if test -f config/includes.binary/CHANGELOG-FR; then rm config/includes.binary/CHANGELOG-FR; fi
	@if test -f config/includes.binary/LICENSE; then rm config/includes.binary/LICENSE; fi
	@if test -f auto/config; then rm auto/config; fi
	@if test -f config/includes.binary/.disk/info; then rm config/includes.binary/.disk/info; fi
	@if test -f config/includes.chroot/etc/skel/.config/emmabuntus/info; then rm config/includes.chroot/etc/skel/.config/emmabuntus/info; fi
	@if test -f config/includes.chroot/Install/env_arch.sh; then rm config/includes.chroot/Install/env_arch.sh; fi
	@if test -f config/includes.chroot/usr/share/plymouth/themes/emerald/logo+emerald.png; \
		then rm config/includes.chroot/usr/share/plymouth/themes/emerald/logo+emerald.png; fi
	@if test -d config/includes.chroot/opt/Install_non_free_softwares/Codecs; \
		then rm -R config/includes.chroot/opt/Install_non_free_softwares/Codecs; fi
	@if ls config/includes.chroot/opt/Install_non_free_softwares/libdvdcss2_*.deb 2> /dev/null; \
		then rm config/includes.chroot/opt/Install_non_free_softwares/libdvdcss2_*.deb; fi
	@if ls config/includes.chroot/opt/Turboprint/turboprint_*.deb 2> /dev/null; \
		then rm config/includes.chroot/opt/Turboprint/turboprint_*.deb; fi
	@if ls config/includes.chroot/opt/ctparental/dnscrypt-proxy_*.deb 2> /dev/null; \
		then rm config/includes.chroot/opt/ctparental/dnscrypt-proxy_*.deb; fi
	@if ls config/includes.chroot/opt/lxkeymap/lxkeymap.appimage 2> /dev/null; \
		then rm config/includes.chroot/opt/lxkeymap/lxkeymap.appimage; fi
	@if test -d config/includes.binary/pool; \
		then rm -r config/includes.binary/pool; fi
	@if test -d config/includes.binary/dists; \
		then rm -r config/includes.binary/dists; fi
	@if test -d config/includes.binary/EFI; \
		then rm -r config/includes.binary/EFI; fi
	@if test -f config/includes.binary/boot/grub/efi.img; \
		then rm config/includes.binary/boot/grub/efi.img ; fi
	@if test -d config/includes.chroot/usr/share/locale; \
		then rm -r config/includes.chroot/usr/share/locale; fi

clean: clean_all
	lb clean

cleanfull:
	@echo "----------------------------------"
	@echo "cleaning build & cache directories"
	@echo "----------------------------------"
	rm -R -f cache


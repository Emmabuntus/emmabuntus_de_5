#!/usr/bin/tclsh

set file_in_amd64 "live-image-amd64.packages"
set file_in_i386  "live-image-i386.packages"
set file_out "package.txt"

if [file exists $file_in_amd64] {

    puts "Fichier d'assemblage amd64"
    set file_in $file_in_amd64

} elseif [file exists $file_in_i386] {

    puts "Fichier d'assemblage i386"
    set file_in $file_in_i386

} else {

    puts "Pas de fichier d'assemblage"
    exit 1

}


set list_paquets [ list abiword alsa-tools ancestris arandr asunder audacity bash belooted bleachbit boot-repair boot-sav bum cairo-dock-core calibre clean-ubiquity clementine compiz cups curl daisy-player ebook-speaker evince exfalso falkon ffmpeg file filezilla findutils firefox-esr font-manager gcompris-qt geany ghostscript gimp gnome-disk-utility gnome-search-tool gnumeric gparted gpodder grub-common gthumb gtkhash guvcview handbrake handytri hardinfo homebank inkscape inxi Jami kaffeine kdenlive kdelibs-bin kiwix libreoffice linux-image-686 linux-image-amd64 mediainfo-gui mousetweaks mysql-common openjdk-17-jre:i386 openjdk-17-jre:amd64 openssh-client openssl orca os-prober os-uninstaller parted perl pidgin pulseaudio python3 qlipper quodlibet radiotray-ng libqtcore4:i386 libqtcore4:amd64 lxqt-session radiotray recordmydesktop redshift scratch scribus synaptic syslinux systemd-zram-generator sysvinit-utils tar libtcl timeshift tcpdump thunar thunderbird transmission-gtk tuxmath tuxpaint tuxtype udev verbiste vim-common virtualbox vlc vokoscreen-ng warpinator xfburn xfdesktop4 xl-wallpaper xscreensaver xz-utils]


set id_file_in [ open $file_in r  ]
set id_file_out [ open $file_out w+ ]

puts $id_file_out "List of packages included in Emmabuntüs DE 5"
puts $id_file_out "----------------------------------------------"
puts $id_file_out ""

while {[gets $id_file_in line] >= 0 } {

    foreach nom_paquet $list_paquets {
    #puts $nom_paquet

    set result [regexp -nocase -- $nom_paquet $line]

        if {"$nom_paquet" == "openjdk-8-jre"} {

            if  {[regexp -nocase -- "^$nom_paquet" $line]} {

            puts $id_file_out $line

            }

        } else {

            if  {[regexp -nocase -- "^$nom_paquet\t" $line]} {

            puts $id_file_out $line

        }
        }
    }
}


close $id_file_in
close $id_file_out



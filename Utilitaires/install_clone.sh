#!/bin/sh

# Préparation données de Culture Libre

if ! test -d /FreeCulture/
then
   sudo mkdir /FreeCulture
fi

sudo chmod 666 -R /FreeCulture
mkdir /FreeCulture/Wikipedia
mkdir /FreeCulture/eBooks
mkdir /FreeCulture/Musique
mkdir /FreeCulture/Daisy

# Ensuite copier les fichiers dans /FreeCulture
# Faire le pointage dans Calibre, Clementine et Kiwix
# Ensuite supprimer le dossier /FreeCulture/eBooks qui est inutile

rm -R /FreeCulture/eBooks

# Pointage des versions de Kiwix pour l'accessibilité
# Modifier la variable Library_zim pour Kiwix dans env_emmabuntus

repertoire_emmabuntus=~/.config/emmabuntus
env_emmabuntus=${repertoire_emmabuntus}/env_emmabuntus

echo "Library_zim=\"/FreeCulture/Wikipedia/\"" >> ${env_emmabuntus}

# Ajout des logiciels supprimés suite aux évolutions d'Emmabuntüs pour la réduction de la taille de l'ISO

sudo apt-get update

sudo apt-get install -y hedgewars

sudo apt-get install -y supertuxkart

sudo dpkg --add-architecture i386
sudo apt update
sudo apt install -y wine32
sudo apt install -y wine winetricks fonts-wine libwine playonlinux

sudo apt-get install -y stellarium

sudo apt-get install -y supertux

sudo apt-get install -y tuxguitar

sudo apt-get install -y darktable

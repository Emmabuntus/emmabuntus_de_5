#!/bin/bash

file_name="packages_list"
file_in="${file_name}_brut.txt"
file_out="${file_name}.txt"
file_out_exclude="${file_name}_exclude.txt"

dpkg -l > ${file_in}

echo "List of packages included in Emmabuntüs DE 5" > ${file_out}
echo "----------------------------------------------" >> ${file_out}

echo "----------------------------------------------" > ${file_out_exclude}

while read -r line ; do


    if  [[ $(echo ${line} | grep "^ii") ]] ; then

        paquet=$(echo ${line} | cut -d " " -f 2 | cut -d " " -f 1)
        version=$(echo ${line} | cut -d " " -f 3 | cut -d " " -f 1)

        echo "${paquet}^${version}" >> ${file_out}

    else

        echo ${line} >> ${file_out_exclude}

    fi

done  < ${file_in}

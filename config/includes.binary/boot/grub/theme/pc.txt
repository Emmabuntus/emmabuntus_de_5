title-color: "white"
title-text: "Installer menu"
title-font: "DejaVu Sans Bold 20"
desktop-color: "black"
desktop-image: "/boot/grub/splash.png"
message-color: "white"
message-bg-color: "black"
terminal-font:  "Unifont Regular 16"

+ boot_menu {
  left = 10%
  width = 75%
  top = 300
  height = 350
  item_font = "DejaVu Sans Bold 16"
  item_color = #d3d3d3
  selected_item_color = #f4c300
  item_height = 20
  item_padding = 15
  item_spacing = 5
}

+ vbox {
  top = 100%-60
  left = 10%
  + hbox {
    top = 0
    + label {text = "Enter :  " font = "DejaVu Sans Bold 16" color = "white" align = "left"}
    + label {text = "Select             " font = "DejaVu Sans Bold 14" color = "#d3d3d3" align = "left"}
    + label {text = "           " font = "DejaVu Sans Bold 14" color = "white" align = "left"}
    + label {text = "E :  " font = "DejaVu Sans Bold 16" color = "white" align = "left"}
    + label {text = "Edit Selection     " font = "DejaVu Sans Bold 14" color = "#d3d3d3" align = "left"}
    + label {text = "           " font = "DejaVu Sans Bold 14" color = "white" align = "left"}
    + label {text = "C :  " font = "DejaVu Sans Bold 16" color = "white" align = "left"}
    + label {text = "GRUB Command line" font = "DejaVu Sans Bold 14" color = "#d3d3d3" align = "left"}
  }
}

#! /bin/bash

# calamares_install.sh --
#
#   This file permits to classic Calamares for the Emmabuntus Distrib.
#
#   Created on 2010-22 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 Xfce/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

###Pour exporter la librairie de gettext.
set -a
source gettext.sh
set +a
export TEXTDOMAIN=emmabuntus
export TEXTDOMAINDIR="/usr/share/locale"
. gettext.sh
emmabuntus=$0


file_config="/etc/calamares/settings.conf"
repertoire_emmabuntus=~/.config/emmabuntus
env_emmabuntus=${repertoire_emmabuntus}/env_emmabuntus
fichier_init=${repertoire_emmabuntus}/init_calamares.txt
OEM_LOGIN="oem"
OEM_PASSWORD="oem"

nom_distribution="Emmabuntüs Debian Edition 5"
nom_distribution_fr="Emmabuntüs Debian Édition 5"

if [[ -f ${file_config} ]] ; then



source /usr/bin/emmabuntus_found_theme.sh
highlight_color=$(FOUND_HIGHLIGHT_COLOR)

if [[ $LANG == fr* ]] ; then
    nom_distribution_mise_forme="\<b>\<i>$nom_distribution_fr\</i>\</b>"
else
    nom_distribution_mise_forme="\<b>\<i>$nom_distribution\</i>\</b>"
fi

titre_fenetre="$(eval_gettext 'Install') ${nom_distribution}"

message_accueil="\n\
$(eval_gettext 'You wish to install') ${nom_distribution_mise_forme}.\n\
\n\
$(eval_gettext 'Two installation modes are available : the Classic mode for a personal install ')\n\
$(eval_gettext 'or the OEM mode dedicated to the systems integrators.')\n\
\n\
\<span color=\'${highlight_color}\'>$(eval_gettext 'Note:')\</span> $(eval_gettext 'in OEM mode, the user name is:') \<span color=\'${highlight_color}\'>\<b>${OEM_LOGIN}\</b>\</span>, $(eval_gettext 'the user and root passwords are both:') \<span color=\'${highlight_color}\'>\<b>${OEM_PASSWORD}\</b>\</span>\n"



    export WINDOW_DIALOG_WELCOME='<window title="'${titre_fenetre}'" icon-name="gtk-info" resizable="false">
    <vbox spacing="0">

    <text use-markup="true" wrap="false" xalign="0" justify="3">
    <input>echo "'${message_accueil}'" | sed "s%\\\%%g"</input>
    </text>

    <hbox spacing="10" space-expand="false" space-fill="false">

    <button>
    <label>'$(eval_gettext 'Cancel')'</label>
    <input file stock="gtk-cancel"></input>
    <action>exit:Cancel</action>
    </button>

    <button>
    <label>'$(eval_gettext 'OEM installation  ')'</label>
    <input file stock="gtk-execute"></input>
    <action>exit:OEM</action>
    </button>

    <button can-default="true" has-default="true" use-stock="true" is-focus="true">
    <label>'$(eval_gettext 'Classic installation  ')'</label>
    <input file stock="gtk-ok"></input>
    <action>exit:OK</action>
    </button>

    </hbox>

    </vbox>
    </window>'

    MENU_DIALOG_WELCOME="$(gtkdialog --center --program=WINDOW_DIALOG_WELCOME)"
    eval ${MENU_DIALOG_WELCOME}
    # echo "MENU_DIALOG_WELCOME=${MENU_DIALOG_WELCOME}"

        if [ ${EXIT} == "OK" ] ; then

            echo "Installation classique " >> ${fichier_init}
            echo "DATE = $DATE" >> ${fichier_init}

            /usr/bin/calamares_install_exec.sh

        elif [ ${EXIT} == "OEM" ] ; then

            echo "Installation OEM " >> ${fichier_init}
            echo "DATE = $DATE" >> ${fichier_init}

            /usr/bin/calamares_oem_preinstall_exec.sh

        fi

    exit 0

else

    exit 1

fi



#! /bin/bash

# enable_cairo_dock_exec.sh --
#
#   This file permits to enable/disable cairo-dock for Emmabuntüs distribution
#
#   Created on 2010-24 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was checked and validated on Debian 12 XFCE/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

#var
cmd_enable_dock=$1

env_emmabuntus=$2


if [[ ${cmd_enable_dock} == "1" ]]
then

    if [[ $(ps -A | grep "xfce4-session") ]]
    then

        sed s/"^Dock_XFCE=\"[0-9]*\""/"Dock_XFCE=\"1\""/ ${env_emmabuntus} > ${env_emmabuntus}.tmp

        cp ${env_emmabuntus}.tmp ${env_emmabuntus}
        rm ${env_emmabuntus}.tmp

    elif [[ $(ps -A | grep "lxsession") ]]
    then
        sed s/"^Dock_LXDE=\"[0-9]*\""/"Dock_LXDE=\"1\""/ ${env_emmabuntus} > ${env_emmabuntus}.tmp

        cp ${env_emmabuntus}.tmp ${env_emmabuntus}
        rm ${env_emmabuntus}.tmp

    elif [[ $(ps -A | grep "openbox") ]]
    then
        sed s/"^Dock_OpenBox=\"[0-9]*\""/"Dock_OpenBox=\"1\""/ ${env_emmabuntus} > ${env_emmabuntus}.tmp

        cp ${env_emmabuntus}.tmp ${env_emmabuntus}
        rm ${env_emmabuntus}.tmp

    elif [[ $(ps -A | grep "lxqt-session") ]]
    then
        sed s/"^Dock_LXQT=\"[0-9]*\""/"Dock_LXQT=\"1\""/ ${env_emmabuntus} > ${env_emmabuntus}.tmp

        cp ${env_emmabuntus}.tmp ${env_emmabuntus}
        rm ${env_emmabuntus}.tmp
    fi


elif [[ ${cmd_enable_dock} == "0" ]]
then

    if [[ $(ps -A | grep "xfce4-session") ]]
    then
        sed s/"^Dock_XFCE=\"[0-9]*\""/"Dock_XFCE=\"0\""/ ${env_emmabuntus} > ${env_emmabuntus}.tmp

        cp ${env_emmabuntus}.tmp ${env_emmabuntus}
        rm ${env_emmabuntus}.tmp

    elif [[ $(ps -A | grep "lxsession") ]]
    then
        sed s/"^Dock_LXDE=\"[0-9]*\""/"Dock_LXDE=\"0\""/ ${env_emmabuntus} > ${env_emmabuntus}.tmp

        cp ${env_emmabuntus}.tmp ${env_emmabuntus}
        rm ${env_emmabuntus}.tmp

    elif [[ $(ps -A | grep "openbox") ]]
    then
        sed s/"^Dock_OpenBox=\"[0-9]*\""/"Dock_OpenBox=\"0\""/ ${env_emmabuntus} > ${env_emmabuntus}.tmp

        cp ${env_emmabuntus}.tmp ${env_emmabuntus}
        rm ${env_emmabuntus}.tmp

    elif [[ $(ps -A | grep "lxqt-session") ]]
    then
        sed s/"^Dock_LXQT=\"[0-9]*\""/"Dock_LXQT=\"0\""/ ${env_emmabuntus} > ${env_emmabuntus}.tmp

        cp ${env_emmabuntus}.tmp ${env_emmabuntus}
        rm ${env_emmabuntus}.tmp
    fi
fi

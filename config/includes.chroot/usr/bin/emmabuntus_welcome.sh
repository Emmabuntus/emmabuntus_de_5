#!/bin/bash


# emmabuntus_welcome.sh --
#
#   This file permits to display a Welcome panel for the Emmabuntüs Distrib.
#
#   Created on 2010-25 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 XFCE/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

########################################################################################################


###Pour exporter la librairie de gettext.
set -a
source gettext.sh
set +a
export TEXTDOMAIN=emmabuntus
export TEXTDOMAINDIR="/usr/share/locale"
. gettext.sh
emmabuntus=$0


nom_distribution="Emmabuntus Debian Edition 5"
nom_titre_window="Emmabuntüs Welcome"
repertoire_emmabuntus=~/.config/emmabuntus
env_emmabuntus=${repertoire_emmabuntus}/env_emmabuntus
repertoire_script=/usr/bin

Init=""

Init=$1

# echo "Init=$Init"


#  chargement des variables d'environnement
. ${env_emmabuntus}

source /usr/bin/emmabuntus_found_theme.sh
highlight_color=$(FOUND_HIGHLIGHT_COLOR)
#highlight_color="red"

if [[ $Welcome_window == "0" ]]  ; then
    CHECKBOX_RESTART="false"
else
    CHECKBOX_RESTART="true"
fi


if [[ ${Init} != ""  || ${CHECKBOX_RESTART} == "true" || $(cat /proc/cmdline | grep -i boot=live) ]]
then

if  [[ ! $(cat /proc/cmdline | grep -i boot=live) && $(ping -w 1 sourceforge.net | grep "1 received") ]] ; then
message_info="\n\
\<b>\<big>\<span color=\'${highlight_color}\'>$(eval_gettext 'Welcome to ')\<i>Emmabuntüs Debian Edition 5\</i>.\</span>\</big>\</b>\n\
\n\
\<b>\<big>$(eval_gettext 'This distribution is made to refurbish computers,')\</big>\</b>\n\
\<b>\<big>$(eval_gettext 'and let everyone discover GNU/Linux.')\</big>\</b>\n\
\n\
$(eval_gettext 'If you appreciate Emmabuntüs, please support us by automatically donating the water drops')\n\
$(eval_gettext 'collected during your internet searches via Lilo. To do so, click on the \042Contribute\042 icon below:')"

HEIGHT_WINDOW=450

if [[ $(ps -A | grep "xfce4-session") ]] ; then
    WIDHT_WINDOW=700
else
    WIDHT_WINDOW=800
fi

else

message_info="\n\
\<b>\<big>\<span color=\'${highlight_color}\'>$(eval_gettext 'Welcome to ')\<i>Emmabuntüs Debian Edition 5\</i>.\</span>\</big>\</b>\n\
\n\
\<b>\<big>$(eval_gettext 'This distribution is made to refurbish computers,')\</big>\</b>\n\
\<b>\<big>$(eval_gettext 'and let everyone discover GNU/Linux.')\</big>\</b>"

HEIGHT_WINDOW=450
WIDHT_WINDOW=650

fi
LIVE_PASSWORD="live"

message_info_login_live="$(eval_gettext 'Username:') \<span color=\'${highlight_color}\'>\<b>${USER}\</b>\</span>, $(eval_gettext 'the user and root passwords are both:') \<span color=\'${highlight_color}\'>\<b>${LIVE_PASSWORD}\</b>\</span>"


# Initilisation des variables de la fenêtre de dialogue
CHECKBOX_RESTART_VISIBLE=true
CHECKBOX_INFO_LOGIN_VISIBLE=false
TAILLE_ICONE=50
TAILLE_ICONE_BOUTON_LOGIN_INFO=20
VBOX_SPACING=10

if [[ $(cat /proc/cmdline | grep -i boot=live) ]]
then

    CHECKBOX_RESTART_VISIBLE=false
    CHECKBOX_INFO_LOGIN_VISIBLE=true

fi

export EMMABUNTUS_WELCOME='<window title="'$(eval_gettext 'Emmabuntüs - Welcome')'" icon-name="gtk-info" width_request="'${WIDHT_WINDOW}'" height_request="'${HEIGHT_WINDOW}'" resizable="false">
 <vbox spacing="5" >
 <text use-markup="true" wrap="false" justify="2">
 <input>echo "'${message_info}'" | sed "s%\\\%%g"</input>
 </text>

 <hseparator space-expand="true" space-fill="true"></hseparator>

 <vbox spacing="0" height_request="200">

 <hbox space-expand="true" space-fill="true">

 <vbox space-expand="true" space-fill="false" shadow-type="1">


    <button image-position="0">
    <input file>/usr/share/icons/emmabuntus.svg</input>
    <width>'$TAILLE_ICONE'</width>
    <label>" '$(eval_gettext 'Tutorials           ')'"</label>
    <variable>emmabuntus</variable>
    <action signal="enter-notify-event">activate:emmabuntus</action>
    <action signal="button-press-event">'$repertoire_script'/emmabuntus_welcome_func.sh EMMABUNTUS</action>
    </button>

    <button image-position="0">
    <input file>/usr/share/icons/les_cahiers_du_debutant.png</input>
    <width>'$TAILLE_ICONE'</width>
    <label>" '$(eval_gettext 'Debian Handbook ')'"</label>
    <variable>debian_cahier</variable>
    <action signal="enter-notify-event">activate:debian_cahier</action>
    <action signal="button-press-event">'$repertoire_script'/emmabuntus_welcome_func.sh DEBIAN_CAHIER</action>
    </button>


    <button image-position="0">
    <input file>/usr/share/icons/debian_reference.png</input>
    <width>'$TAILLE_ICONE'</width>
    <label>" '$(eval_gettext 'Debian Reference')'"</label>
    <variable>debian_reference</variable>
    <action signal="enter-notify-event">activate:debian_reference</action>
    <action signal="button-press-event">'$repertoire_script'/emmabuntus_welcome_func.sh DEBIAN_REFERENCE</action>
    </button>


 </vbox>


 <vbox space-expand="true" space-fill="false" shadow-type="1">

    <button image-position="0">
    <input file>/usr/share/icons/tools.png</input>
    <width>'$TAILLE_ICONE'</width>
    <label>" '$(eval_gettext 'Tools                  ')'"</label>
    <variable>outils</variable>
    <action signal="enter-notify-event">activate:outils</action>
    <action signal="button-press-event">'$repertoire_script'/emmabuntus_welcome_func.sh OUTILS</action>
    </button>

    <button image-position="0">
    <input file>/usr/share/icons/revival-blue/apps/scalable/preferences-desktop.svg</input>
    <width>'$TAILLE_ICONE'</width>
    <label>" '$(eval_gettext 'Settings Manager ')'"</label>
    <variable>gest_params</variable>
    <action signal="enter-notify-event">activate:gest_params</action>
    <action signal="button-press-event">'$repertoire_script'/emmabuntus_welcome_func.sh GEST_PARAMS</action>
    </button>

    <button image-position="0" >
    <input file>/usr/share/icons/revival-blue/apps/scalable/org.gnome.Software.svg</input>
    <width>'$TAILLE_ICONE'</width>
    <label>" '$(eval_gettext 'Software Center  ')'"</label>
    <variable>logitheque</variable>
    <action signal="enter-notify-event">activate:logitheque</action>
    <action signal="button-press-event">'$repertoire_script'/emmabuntus_welcome_func.sh LOGITHEQUE</action>
    </button>


 </vbox>

 <vbox space-expand="true" space-fill="false" shadow-type="1">

    <button image-position="0">
    <input file>/usr/share/icons/forum.png</input>
    <width>'$TAILLE_ICONE'</width>
    <label>" '$(eval_gettext 'Forum            ')'"</label>
    <variable>forum</variable>
    <action signal="enter-notify-event">activate:forum</action>
    <action signal="button-press-event">'$repertoire_script'/emmabuntus_welcome_func.sh FORUM</action>
    </button>

    <button image-position="0">
    <input file>/usr/share/icons/video.png</input>
    <width>'$TAILLE_ICONE'</width>
    <label>" '$(eval_gettext 'Videos            ')'"</label>
    <variable>video</variable>
    <action signal="enter-notify-event">activate:video</action>
    <action signal="button-press-event">'$repertoire_script'/emmabuntus_welcome_func.sh VIDEO</action>
    </button>

    <button image-position="0">
    <input file>/usr/share/icons/lilo.png</input>
    <width>'$TAILLE_ICONE'</width>
    <label>" '$(eval_gettext 'Contribute       ')'"</label>
    <variable>soutien</variable>
    <action signal="enter-notify-event">activate:soutien</action>
    <action signal="button-press-event">'$repertoire_script'/emmabuntus_welcome_func.sh SOUTIEN</action>
    </button>

    </vbox>

 </hbox>
 </vbox>


 <hseparator space-expand="true" space-fill="true"></hseparator>

  <hbox spacing="" space-expand="true" space-fill="false">
    <checkbox active="'${CHECKBOX_RESTART}'" visible="'${CHECKBOX_RESTART_VISIBLE}'">
    <variable>CHECKBOX_RESTART</variable>
    <label>'$(eval_gettext 'Show this window at next startup            ')'</label>
    <action>'$repertoire_script'/emmabuntus_welcome_func.sh CHECKBOX_RESTART ${CHECKBOX_RESTART} '${Welcome_window}'</action>
    </checkbox>

    <text use-markup="true" wrap="false" xalign="0" justify="3" visible="'${CHECKBOX_INFO_LOGIN_VISIBLE}'">
    <input>echo -n "'${message_info_login_live}'" | sed "s%\\\%%g"</input>
    </text>

    <hbox spacing="10" space-expand="true" space-fill="true">
    <button is-focus="true">
    <input file icon="gtk-close"></input>
    <label>'$(eval_gettext 'Close')'</label>
    <action>pkill -9 gnome-software</action>
    <action>exit:exit</action>
    </button>
    </hbox>
  </hbox>

 </vbox>
 </window>'


    MENU_DIALOG="$(gtkdialog --center --program=EMMABUNTUS_WELCOME)"

    eval ${MENU_DIALOG}
    # echo "MENU_DIALOG=${MENU_DIALOG}"


fi


#! /bin/bash

# start_cairo_dock.sh --
#
#   This file permits to stop cairo-dock logout.
#
#   Created on 2010-24 by Collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 XFCE/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################



if [[ $(ps -A | grep "xfce4-session") ]]
then

#echo "xfce4-session-logout"
rm -r ~/.cache/sessions/*
xfce4-session-logout


elif [[ $(ps -A | grep "lxsession") ]]
then

#echo "lxde-logout"
lxde-logout


elif [[ $(ps -A | grep "openbox") ]]
then

#echo "openbox-logout"
openbox --exit


elif [[ $(ps -A | grep "lxqt-session") ]]
then

#echo "lxqt-logout"
lxqt-leave

fi









#! /bin/bash


# remove_language_not_use.sh --
#
#   This file permits to remove all languages packages not used for the Emmabuntüs Distrib.
#
#   Created on 2010-23 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 XFCE/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


########################################################################################################


# Test si utilisateur n'appartient pas au groupe sudo
if [[ $(groups | grep sudo) == "" ]] ; then

    echo "User isn't in sudo group !!"
    exit 0

fi
###Pour exporter la librairie de gettext.
set -a
source gettext.sh
set +a
export TEXTDOMAIN=emmabuntus
export TEXTDOMAINDIR="/usr/share/locale"
. gettext.sh
emmabuntus=$0


nom_distribution="Emmabuntus Debian Edition 5"
nom_titre_window="Removing unused languages"
repertoire_emmabuntus=~/.config/emmabuntus
fichier_init_config_user=${repertoire_emmabuntus}/init_config_remove_language_not_use.txt
dir_install_non_free_softwares=/opt/Install_non_free_softwares
fichier_init_config_final=$dir_install_non_free_softwares/.init_config_remove_language_not_use.txt
repertoire_config_language=/usr/share/emmabuntus/

CHECKBOX_RESTART="true"
CHECKBOX_INSTALL="true"
CHECKBOX_SET_DEFAULT_LANG="true"
CHECKBOX_INTERNET_ACTIF="false"
CHECKBOX_RESTART_ACTIF="true"

choix=""
utilisateur=emmabuntus

Init=""
Init=$1
# echo "Init=$Init"

if [[ ${Init} != "" ]] ; then
    CHECKBOX_RESTART_ACTIF="false"
fi

source /usr/bin/emmabuntus_found_theme.sh
highlight_color=$(FOUND_HIGHLIGHT_COLOR)

if [[ ${Init} != ""  || ! ( -f $fichier_init_config_user || -f $fichier_init_config_final ) ]]
then

# Patch sur les fichiers de langues créés par Calamares
pkexec /usr/bin/emmabuntus_patch_calamares_locale_exec.sh

# Détermination des paquets de langues installés

  nom_paquet_firefox=firefox-esr-l10n-en*
  if [[ $(dpkg --get-selections ${nom_paquet_firefox} 2> /dev/null | grep -e "install") ]]
  then
      CHECKBOX_EN="true" ; CHECKBOX_EN_ACTIF="true"
  else
      CHECKBOX_EN="false" ; CHECKBOX_EN_ACTIF="false"
  fi

  nom_paquet_firefox=firefox-esr-l10n-fr*
  if [[ $(dpkg --get-selections ${nom_paquet_firefox} 2> /dev/null | grep -e "install") ]]
  then
      CHECKBOX_FR="true" ; CHECKBOX_FR_ACTIF="true"
  else
      CHECKBOX_FR="false" ; CHECKBOX_FR_ACTIF="false"
  fi

  nom_paquet_firefox=firefox-esr-l10n-es*
  if [[ $(dpkg --get-selections ${nom_paquet_firefox} 2> /dev/null | grep -e "install") ]]
  then
      CHECKBOX_ES="true" ; CHECKBOX_ES_ACTIF="true"
  else
      CHECKBOX_ES="false" ; CHECKBOX_ES_ACTIF="false"
  fi

  nom_paquet_firefox=firefox-esr-l10n-it*
  if [[ $(dpkg --get-selections ${nom_paquet_firefox} 2> /dev/null | grep -e "install") ]]
  then
      CHECKBOX_IT="true" ; CHECKBOX_IT_ACTIF="true"
  else
      CHECKBOX_IT="false" ; CHECKBOX_IT_ACTIF="false"
  fi

  nom_paquet_firefox=firefox-esr-l10n-de*
  if [[ $(dpkg --get-selections ${nom_paquet_firefox} 2> /dev/null | grep -e "install") ]]
  then
      CHECKBOX_DE="true" ; CHECKBOX_DE_ACTIF="true"
  else
      CHECKBOX_DE="false" ; CHECKBOX_DE_ACTIF="false"
  fi

  nom_paquet_firefox=firefox-esr-l10n-pt*
  if [[ $(dpkg --get-selections ${nom_paquet_firefox} 2> /dev/null | grep -e "install") ]]
  then
      CHECKBOX_PT="true" ; CHECKBOX_PT_ACTIF="true"
  else
      CHECKBOX_PT="false" ; CHECKBOX_PT_ACTIF="false"
  fi

  nom_paquet_firefox=firefox-esr-l10n-da*
  if [[ $(dpkg --get-selections ${nom_paquet_firefox} 2> /dev/null | grep -e "install") ]]
  then
      CHECKBOX_DA="true" ; CHECKBOX_DA_ACTIF="true"
  else
      CHECKBOX_DA="false" ; CHECKBOX_DA_ACTIF="false"
  fi

  nom_paquet_firefox=firefox-esr-l10n-ja*
  if [[ $(dpkg --get-selections ${nom_paquet_firefox} 2> /dev/null | grep -e "install") ]]
  then
      CHECKBOX_JA="true" ; CHECKBOX_JA_ACTIF="true"
  else
      CHECKBOX_JA="false" ; CHECKBOX_JA_ACTIF="false"
  fi

message_demarrage="\n\
\<span color=\'${highlight_color}\'>$(eval_gettext 'Do you want to remove unnecessary languages in the distribution ?')\</span>\n\
\n\
$(eval_gettext 'Some languages are not needed by your system')\n\
$(eval_gettext 'removing them will spare some disk space,')\n\
$(eval_gettext 'and also prevent downloading updates for these languages.')     "

message_validation="\
$(eval_gettext 'Please select the languages you no longer need:')"


if [[ $LANG == fr* ]]
then

    CHECKBOX_FR="false" ; CHECKBOX_FR_ACTIF="false"

elif [[ $LANG == de* ]]
then

    CHECKBOX_DE="false" ; CHECKBOX_DE_ACTIF="false"

elif [[ $LANG == pt* ]]
then

    CHECKBOX_PT="false" ; CHECKBOX_PT_ACTIF="false"

elif [[ $LANG == it* ]]
then

    CHECKBOX_IT="false" ; CHECKBOX_IT_ACTIF="false"

elif [[ $LANG == es* ]]
then

    CHECKBOX_ES="false" ; CHECKBOX_ES_ACTIF="false"


elif [[ $LANG == da* ]]
then

    CHECKBOX_DA="false" ; CHECKBOX_DA_ACTIF="false"

elif [[ $LANG == ja* ]]
then

    CHECKBOX_JA="false" ; CHECKBOX_JA_ACTIF="false"

else

    CHECKBOX_EN="false" ; CHECKBOX_EN_ACTIF="false"

fi


if [[ $(ping -w 1 sourceforge.net | grep "1 received") ]] ; then
    # echo "Internet is a live"
    CHECKBOX_INTERNET_ACTIF="true"
else
    # echo "Internet wasn't a live !!"
    CHECKBOX_INTERNET_ACTIF="false"
    CHECKBOX_INSTALL="false"
fi


# Determination du pays courant
locale=$(echo $LANG | cut -d '.' -f1)

if [[ ${locale} == "" ]] ; then
    locale=${LANG}
fi

# echo "LANG=${LANG}"

COUNTRY=$(grep ^${locale} ${repertoire_config_language}/language_locale | cut -d '"' -f2)

if [[ ${COUNTRY} == "" ]] ; then
    locale="en_US"
    COUNTRY=$(grep ^${locale} ${repertoire_config_language}/language_locale | cut -d '"' -f2)
fi

# echo "COUNTRY=${COUNTRY}"


 export WINDOW_DIALOG='<window title="'$(eval_gettext 'Language Management')'" icon-name="gtk-dialog-question" width_request="650"  height_request="500" resizable="false">
 <vbox spacing="0">
 <text use-markup="true" wrap="false" xalign="0" justify="3">
 <input>echo "'${message_demarrage}'" | sed "s%\\\%%g"</input>
 </text>
 <text use-markup="true" wrap="false" xalign="0" justify="3">
 <input>echo "'${message_validation}'" | sed "s%\\\%%g"</input>
 </text>

 <hseparator space-expand="true" space-fill="true"></hseparator>

 <vbox homogeneous="true" height_request="180">
  <checkbox active="'${CHECKBOX_FR}'" sensitive="'${CHECKBOX_FR_ACTIF}'">
    <variable>CHECKBOX_FR</variable>
    <label>Français</label>
  </checkbox>
  <checkbox active="'${CHECKBOX_EN}'" sensitive="'${CHECKBOX_EN_ACTIF}'">
    <variable>CHECKBOX_EN</variable>
    <label>English</label>
  </checkbox>
  <checkbox active="'${CHECKBOX_ES}'" sensitive="'${CHECKBOX_ES_ACTIF}'">
    <variable>CHECKBOX_ES</variable>
    <label>Español</label>
  </checkbox>
  <checkbox active="'${CHECKBOX_IT}'" sensitive="'${CHECKBOX_IT_ACTIF}'">
    <variable>CHECKBOX_IT</variable>
    <label>Italiano</label>
  </checkbox>
  <checkbox active="'${CHECKBOX_DE}'" sensitive="'${CHECKBOX_DE_ACTIF}'">
    <variable>CHECKBOX_DE</variable>
    <label>Deutsch</label>
  </checkbox>
  <checkbox active="'${CHECKBOX_PT}'" sensitive="'${CHECKBOX_PT_ACTIF}'">
    <variable>CHECKBOX_PT</variable>
    <label>Português</label>
  </checkbox>
  <checkbox active="'${CHECKBOX_DA}'" sensitive="'${CHECKBOX_DA_ACTIF}'">
    <variable>CHECKBOX_DA</variable>
    <label>Dansk</label>
  </checkbox>
  <checkbox active="'${CHECKBOX_JA}'" sensitive="'${CHECKBOX_JA_ACTIF}'">
    <variable>CHECKBOX_JA</variable>
    <label>日本語</label>
  </checkbox>

 </vbox>

 <hseparator space-expand="true" space-fill="true"></hseparator>

   <checkbox active="'${CHECKBOX_INSTALL}'" sensitive="'${CHECKBOX_INTERNET_ACTIF}'" tooltip-text="'$(eval_gettext 'Install additional language packs for the selected language in the drop-down menu below. Requires an active Internet connection.')'">
   <variable>CHECKBOX_INSTALL</variable>
   <label>"'$(eval_gettext 'Install the complements of the selected language below:')'"</label>
   </checkbox>

   <comboboxtext allow-empty="false" value-in-list="true">
   <variable>SELECT_COUNTRY</variable>
   <default>'${COUNTRY}'</default>
   <input file>'${repertoire_config_language}/countries'</input>
   </comboboxtext>

   <checkbox active="'${CHECKBOX_SET_DEFAULT_LANG}'" tooltip-text="'$(eval_gettext 'Requires a computer restart to take the new language into account')'">
   <variable>CHECKBOX_SET_DEFAULT_LANG</variable>
   <label>"'$(eval_gettext 'Set the language selected above as the default system language')'"</label>
   </checkbox>

 <hseparator space-expand="true" space-fill="true"></hseparator>

 <hbox spacing="10" space-expand="false" space-fill="false">

   <checkbox active="'${CHECKBOX_RESTART}'" sensitive="'${CHECKBOX_RESTART_ACTIF}'">
   <variable>CHECKBOX_RESTART</variable>
   <label>"'$(eval_gettext 'Show this window at next startup')'"</label>
   </checkbox>

   <hbox spacing="10" space-expand="true" space-fill="true">
   <button cancel></button>
   <button can-default="true" has-default="true" use-stock="true" is-focus="true">
   <label>gtk-ok</label>
   <action>exit:OK</action>
   </button>
   </hbox>
 </hbox>

 </vbox>
 </window>'


    if [[ $(cat /proc/cmdline | grep -i boot=live) ]]
    then

        exit 0

    else

        MENU_DIALOG="$(gtkdialog --center --program=WINDOW_DIALOG)"

        eval ${MENU_DIALOG}
        # echo "MENU_DIALOG=${MENU_DIALOG}"

        if [ ${CHECKBOX_FR} == "true" ]
        then
            choix=${choix}:Français
        fi

        if [ ${CHECKBOX_EN} == "true" ]
        then
            choix=${choix}:English
        fi

        if [ ${CHECKBOX_ES} == "true" ]
        then
            choix=${choix}:Español
        fi

        if [ ${CHECKBOX_IT} == "true" ]
        then
            choix=${choix}:Italiano
        fi

        if [ ${CHECKBOX_DE} == "true" ]
        then
            choix=${choix}:Deutsch
        fi

        if [ ${CHECKBOX_PT} == "true" ]
        then
            choix=${choix}:Português
        fi

        if [ ${CHECKBOX_DA} == "true" ]
        then
            choix=${choix}:Dansk
        fi

        if [ ${CHECKBOX_JA} == "true" ]
        then
            choix=${choix}:Japanese
        fi

        if [ ${CHECKBOX_INSTALL} == "true" ]
        then
            choix=${choix}:Install
        fi


        if [ ${CHECKBOX_SET_DEFAULT_LANG} == "true" ]
        then
            choix=${choix}:SetDefaultLanguage
        fi



        # echo "choix=${choix}"

        if [ ${CHECKBOX_RESTART} == "false" ]
        then
            echo $nom_distribution | tee $fichier_init_config_user > /dev/null
            echo "Fichier servant à ne pas afficher la fenêtre de bienvenue" | tee -a $fichier_init_config_user > /dev/null
        fi

        if [ ${EXIT} == "OK" ]
        then
            if [[ ${choix} != "" ]]
            then

                user=$USER

                # Détermination locale sélectionné
                select_locale=$(grep "${SELECT_COUNTRY}" ${repertoire_config_language}/language_locale | cut -d ':' -f1)

                if [[ ${select_locale} == "" ]]; then
                select_locale=${locale}
                fi

                pkexec /usr/bin/remove_language_not_use_exec.sh "${choix}" "${user}" "${select_locale}" "${highlight_color}"

            fi
        fi


    fi

fi


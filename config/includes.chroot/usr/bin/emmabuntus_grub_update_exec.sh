#!/bin/bash

# emmabuntus_grub_update_exec.sh --
#
#   This file permits to update grub menu
#
#   Created on 2010-23 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 XFCE/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################


mod_file=/etc/default/grub

utilisateur=$(getent passwd $PKEXEC_UID | cut -d: -f1)

repertoire_emmabuntus=/home/${utilisateur}/.config/emmabuntus
fichier_log=${repertoire_emmabuntus}/emmabuntus_grub_update_exec_log.txt

if test -f ${mod_file} ; then

    echo "Patch ${mod_file}" > ${fichier_log}
    sudo chown ${utilisateur}:${utilisateur} ${fichier_log}

    sed s/"#GRUB_DISABLE_OS_PROBER[^$]*"/"GRUB_DISABLE_OS_PROBER=false"/ ${mod_file} | sudo tee ${mod_file}.tmp > /dev/null
    sudo cp ${mod_file}.tmp ${mod_file}
    sudo rm ${mod_file}.tmp

    echo "Detecting others systems" >> ${fichier_log}

    xterm -T "Detecting others systems" -e "sudo update-grub"

fi



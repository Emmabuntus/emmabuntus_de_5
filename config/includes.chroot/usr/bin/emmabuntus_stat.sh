#!/bin/bash

# emmabuntus_stat.sh --
#
#   This file permits to know Emmabuntüs utilisation.
#
#   Created on 2010-25 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 XFCE/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################


nom_distribution="Emmabuntus Debian Edition 5"

emmabuntus_version=emmade5

repertoire_emmabuntus=~/.config/emmabuntus
env_emmabuntus=${repertoire_emmabuntus}/env_emmabuntus
fichier_info_stat="${repertoire_emmabuntus}/info_stat"
fichier_info_version="${repertoire_emmabuntus}/info"
number_start=0
number_start_send_stat_live=1
number_start_send_stat_install=10
url_statistics="http://statistics.emmabuntus.org"

export number_start


function write_log()
{

if test -f ${fichier_info_stat} ; then
    source ${fichier_info_stat}
else
# Création du fichier
tee ${fichier_info_stat} > /dev/null <<EOT
#!/bin/bash
number_start=0
EOT

chmod u+x ${fichier_info_stat}

number_start=0

fi

if [ -n "$number_start" ] && [ "$number_start" -eq "$number_start" ] 2>/dev/null; then
  number_start=$((${number_start}+1))
else
  number_start=1
fi

sed s/"^number_start=[^$]*"/"number_start=${number_start}"/ ${fichier_info_stat} > ${fichier_info_stat}.tmp

cp ${fichier_info_stat}.tmp ${fichier_info_stat}
rm ${fichier_info_stat}.tmp

}


function send_statistics()
{

local mode=$1
local bios=""

# Détermination type ordinateur
if [ $(uname -m) = "x86_64" ] ; then
    cpu="amd64"

    # Determination si UEFI ou SecureBoot
    if test -d /sys/firmware/efi/ ; then
        if [[ $(dpkg --get-selections mokutil 2> /dev/null | grep -e "install") ]] ; then
            if [ "$(mokutil --sb-state | grep "SecureBoot enabled")" != "" ] ; then
                bios="uefi_sb/"
            else
                bios="uefi/"
            fi
        else
            bios="uefi/"
        fi
    else
            bios="legacy/"
    fi

else
    cpu="i686"
    bios=""
fi

# Détermination type session
if [[ $(ps -A | grep "xfce4-session") ]] ; then
    session="xfce"
elif [[ $(ps -A | grep "lxqt-session") ]] ; then
    session="lxqt"
else
    session="other"
fi

# Détermination numéro de version
if test -f ${fichier_info_version} ; then

        num_ver=$(cat ${fichier_info_version} | cut -d '-' -f3 | cut -d ' ' -f2 | cut -d '.' -f1)
        num_rev=$(cat ${fichier_info_version} | cut -d '-' -f3 | cut -d ' ' -f2 | cut -d '.' -f2)

        if [[ ( ${num_ver} != "" ) && ( ${num_rev} != "" ) && ( ${num_ver} == ${num_rev} ) ]] ; then
            emmabuntus_version_num="${emmabuntus_version}/${num_ver}"
        elif [[ ( ${num_ver} != "" ) && ( ${num_rev} != "" ) ]] ; then
            emmabuntus_version_num="${emmabuntus_version}/${num_ver}_${num_rev}"
        else
            emmabuntus_version_num="${emmabuntus_version}/unknow"
        fi

else
    emmabuntus_version_num="${emmabuntus_version}/unknow"
fi

# Détermination de l'accessibilité

#  chargement des variables d'environnement
. ${env_emmabuntus}

if [[ ${Accessibility} == "1" ]] ; then
    mode="${mode}_accessibility"
fi


url_statistics_base=$(curl --max-time 10 --silent -sI ${url_statistics} | grep Location | cut -d ' ' -f2  | sed 's/\r$//')
file_statistics="stat.counter"
url_statistics_all="${url_statistics_base}${emmabuntus_version_num}/${mode}/${cpu}/${bios}${session}/${file_statistics}"

echo "url_statistics_all=${url_statistics_all}" >> ${fichier_info_stat}

curl --max-time 10 --silent -o ${repertoire_emmabuntus}/${file_statistics} -L ${url_statistics_all}

if test -f ${repertoire_emmabuntus}/${file_statistics} ; then
    rm ${repertoire_emmabuntus}/${file_statistics}
fi

}

# Partie d'analyse

if [[ $(ping -w 1 sourceforge.net | grep "1 received") ]] ; then
    # echo "Internet is a live"

    if [[ $(cat /proc/cmdline | grep -i boot=live) ]] ; then
        if ! test -f ${fichier_info_stat} ; then
            write_log
            if [[ "$number_start" -eq  "$number_start_send_stat_live" ]] ; then
                send_statistics live
            fi
        fi

    else
        write_log
        if [[ "$number_start" -eq "$number_start_send_stat_install" ]] ; then
            send_statistics install
        fi
    fi

fi

exit 0

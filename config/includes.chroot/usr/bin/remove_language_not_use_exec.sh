#! /bin/bash


# remove_language_not_use_exec.sh --
#
#   This file permits to remove all languages packages not used for the Emmabuntüs Distrib.
#
#   Created on 2010-22 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 XFCE/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


########################################################################################################


###Pour exporter la librairie de gettext.
set -a
source gettext.sh
set +a
export TEXTDOMAIN=emmabuntus
export TEXTDOMAINDIR="/usr/share/locale"
. gettext.sh
emmabuntus=$0


nom_distribution="Emmabuntus Debian Edition 5"
dir_install_non_free_softwares=/opt/Install_non_free_softwares
fichier_init_config=$dir_install_non_free_softwares/.init_config_remove_language_not_use_tmp.txt
fichier_init_config_final=$dir_install_non_free_softwares/.init_config_remove_language_not_use.txt
repertoire_config_language=/usr/share/emmabuntus/

delai_fenetre=20
delai_fenetre_selection=120
delai_fenetre_progression=1200 ; # temporisation de 20 minutes
choix=""
user=""
select_locale=""

# Récupération de l'argument après le passage en root
choix=$1
user=$2
select_locale=$3
highlight_color=$4

if [[ ${user} == root ]] ; then
dir_user=/root
else
dir_user=/home/${user}
fi

# Ajout sécurité sur le taille de select locale
nb_char_locale=$(echo -n ${select_locale} | wc -c)

if [ "$nb_char_locale" -gt 5 ]
then
    echo "select_locale KO"

    exit 0
fi

# Lancement de ce script pour corriger la modification des dépôts pendant l'installation
# sudo /opt/Depots/install_depot_emmabuntus.sh


# Définition par défaut des messages en Anglais

remove_fr="# $(eval_gettext 'Removing French language')"
remove_en="# $(eval_gettext 'Removing English language')"
remove_es="# $(eval_gettext 'Removing Spanish language')"
remove_it="# $(eval_gettext 'Removing Italian language')"
remove_de="# $(eval_gettext 'Removing German language')"
remove_pt="# $(eval_gettext 'Removing Portuguese language')"
remove_da="# $(eval_gettext 'Removing Danish language')"
remove_ja="# $(eval_gettext 'Removing Japanese language')"
remove_ar="# $(eval_gettext 'Removing Arabic language')"
install_package="# $(eval_gettext 'Installing additional language packages for the system current language')"


message_set_default_language="\n\
$(eval_gettext 'To make the default language change effective, you must restart your computer.')     \n\
\n\
\<span color=\'${highlight_color}\'>$(eval_gettext 'Do you want to restart your computer now?')\</span>"


message_set_default_language_ko="\n\
\<span color=\'${highlight_color}\'>$(eval_gettext 'The default language change is not effective       ')\</span>"


########################################################################################################
# Fonction installation des paquets
########################################################################################################

function Remove_package()
{
    local nom_paquet=$1

    if [[ (${nom_paquet} != "") ]]
    then
        if [[ $(dpkg --get-selections ${nom_paquet} 2> /dev/null | grep -e "install") ]];  then

            sudo apt-get remove -y -qq ${nom_paquet} 2> /dev/null

        fi
    fi

}


function Install_package()
{
    local paquet=$1
    local lcode=$2
    local ccode=$3

    echo "${install_package}"

    nom_paquet=${paquet}-${lcode}
    nom_paquet_variante=${paquet}-${lcode}-${ccode}

    if [[ (${paquet} != "") && (${lcode} != "") && (${ccode} != "") ]]
    then
        if [[ (! $(dpkg --get-selections ${nom_paquet} 2> /dev/null | grep -e "install")) ]];  then

            echo "${install_package} : ${nom_paquet}"

            sudo DEBIAN_FRONTEND=noninteractive apt-get install -y -qq ${nom_paquet} 2> /dev/null

            # Teste si la paquet a été installé, sinon on essayes installer la variante de langue
            if [[ ! $(dpkg --get-selections ${nom_paquet} 2> /dev/null | grep -e "install") ]];  then
                if [[ ! $(dpkg --get-selections ${nom_paquet_variante} 2> /dev/null  | grep -e "install") ]]; then

                    echo "${install_package} : ${nom_paquet_variante}"

                    sudo DEBIAN_FRONTEND=noninteractive apt-get install -y -qq ${nom_paquet_variante} 2> /dev/null

                fi
            fi

        fi
    fi

}


function Install_package_wordlist()
{
    local nom_paquet=""

    local lcode=$1
    local ccode=$2

    nom_paquet=$(grep ^wa:${lcode} ${repertoire_config_language}/language_packs | cut -d ':' -f4)

    echo "${install_package}"

    if [[ (${nom_paquet} != "") ]]
    then

        nom_paquet_all=${nom_paquet}

        for nom_paquet in ${nom_paquet_all}
        do

        if [[ ! $(dpkg --get-selections ${nom_paquet} 2> /dev/null | grep -e "install") ]];  then

            echo "${install_package} : ${nom_paquet}"

            sudo DEBIAN_FRONTEND=noninteractive apt-get install -y -qq ${nom_paquet} 2> /dev/null

        fi
        done

    fi

}



function Install_package_font()
{
    local nom_paquet=""
    local nom_paquet1=""

    local lcode=$1
    local ccode=$2

    nom_paquet=$(grep ^fn:${lcode} ${repertoire_config_language}/language_packs | cut -d ':' -f4)

    echo "${install_package}"

    if [[ (${nom_paquet} != "") ]]
    then

        nom_paquet_all=${nom_paquet}

        for nom_paquet in ${nom_paquet_all}
        do
            if [[ ! $(dpkg --get-selections ${nom_paquet} 2> /dev/null | grep -e "install") ]];  then

                echo "${install_package} : ${nom_paquet}"

                sudo DEBIAN_FRONTEND=noninteractive apt-get install -y -qq ${nom_paquet} 2> /dev/null

            fi
        done

    fi


}


########################################################################################################
# Suppresion des langues inutilisées de la distribution
########################################################################################################


langue=`echo $LANG | cut -d_ -f 1 `


if [[ "$choix" != ":SetDefaultLanguage" && "$choix" != "" ]] ; then

(

if [[ "$choix" != ":Install" && "$choix" != "" ]] ; then

    if [[ $choix == *Français* && ${select_locale} != *fr_* ]]
    then
        if [[ $langue != fr ]] ; then
        echo $remove_fr | sudo tee -a $fichier_init_config
        Remove_package firefox-esr-l10n-fr
        Remove_package thunderbird-l10n-fr
        Remove_package libreoffice-l10n-fr
        Remove_package debian-reference-fr
        Remove_package manpages-fr
        Remove_package manpages-fr-extra
        Remove_package hunspell-fr-modern
        #Remove_package wfrench
        Remove_package aspell-fr
        Remove_package hyphen-fr
        Remove_package mythes-fr
        Remove_package childsplay-alphabet-sounds-fr
        Remove_package gcompris-sound-fr
        Remove_package kde-l10n-fr
        fi
    fi



    if [[ $choix == *English* && ${select_locale} != *en_* ]]
        then
        if [[ $langue != en ]] ; then
        echo $remove_en | sudo tee -a $fichier_init_config
        Remove_package firefox-esr-l10n-en-gb
        Remove_package thunderbird-l10n-en-gb
        Remove_package myspell-en-gb
        Remove_package hunspell-en-us
        #Remove_package wbritish
        Remove_package childsplay-alphabet-sounds-en-gb
        Remove_package gcompris-sound-en
        fi
    fi



    if [[ $choix == *Español*  && ${select_locale} != *es_* ]] ; then
        if [[ $langue != es ]] ; then
        echo $remove_es | sudo tee -a $fichier_init_config
        Remove_package firefox-esr-l10n-es-es
        Remove_package thunderbird-l10n-es-es
        Remove_package libreoffice-l10n-es
        Remove_package myspell-es
        #Remove_package wspanish
        Remove_package childsplay-alphabet-sounds-es
        Remove_package gcompris-sound-es
        fi
    fi



    if [[ $choix == *Italiano* && ${select_locale} != *it_* ]] ; then
        if [[ $langue != it ]] ; then
        echo $remove_it | sudo tee -a $fichier_init_config
        Remove_package firefox-esr-l10n-it
        Remove_package thunderbird-l10n-it
        Remove_package libreoffice-l10n-it
        Remove_package myspell-it
        fi
    fi



    if [[ $choix == *Deutsch*  && ${select_locale} != *de_* ]] ; then
        if [[ $langue != de ]] ; then
        echo $remove_de | sudo tee -a $fichier_init_config
        Remove_package firefox-esr-l10n-de
        Remove_package thunderbird-l10n-de
        Remove_package libreoffice-l10n-de
        Remove_package hunspell-de-de
        fi
    fi



    if [[ $choix == *Português* && ${select_locale} != *pt_* ]] ; then
        if [[ $langue != pt ]] ; then
        echo $remove_pt | sudo tee -a $fichier_init_config
        Remove_package firefox-esr-l10n-pt-pt
        Remove_package thunderbird-l10n-pt-pt
        Remove_package libreoffice-l10n-pt
        Remove_package myspell-pt-pt
        fi
    fi

    if [[ $choix == *Dansk* && ${select_locale} != *da_* ]] ; then
        if [[ $langue != da ]] ; then
        echo $remove_da | sudo tee -a $fichier_init_config
        Remove_package firefox-esr-l10n-da
        Remove_package libreoffice-l10n-da
        fi
    fi

    if [[ $choix == *Japanese* && ${select_locale} != *ja_* ]] ; then
        if [[ $langue != da ]] ; then
        echo $remove_ja | sudo tee -a $fichier_init_config
        Remove_package firefox-esr-l10n-ja
        Remove_package libreoffice-l10n-ja
        fi
    fi

    if [[ $choix == *Arabian* && ${select_locale} != *ar_* ]] ; then
        if [[ $langue != ar ]] ; then
        echo $remove_ar | sudo tee -a $fichier_init_config
        fi
    fi

    if [[ $choix == *Français* ]]
    then
        if [[ $langue != fr ]] ; then
        Remove_package debian-beginners-handbook-fr
        fi
    else
        if [[ $langue == fr ]] ; then
        Remove_package debian-beginners-handbook-en
        fi
    fi

fi

if [[ $choix == *Install* ]] ; then

    LCODE=$(echo ${select_locale} | cut -d '_' -f1)
    CCODE=$(echo ${select_locale} | cut -d '_' -f2 | cut -d '.' -f1)
    ccode=$(echo "${CCODE,,}")

    if [[ (${LCODE} != "" ) && (${LCODE} != "" ) ]]; then

        echo $install_package | sudo tee -a $fichier_init_config

        sudo apt-get -qq update

        Install_package firefox-esr-l10n ${LCODE} ${ccode}
        Install_package thunderbird-l10n ${LCODE} ${ccode}
        Install_package libreoffice-l10n ${LCODE} ${ccode}
        Install_package libreoffice-help ${LCODE} ${ccode}
        Install_package gimp-help ${LCODE} ${ccode}
        Install_package childsplay-alphabet-sounds ${LCODE} ${ccode}
        Install_package gcompris-sound ${LCODE} ${ccode}
        Install_package kde-l10n ${LCODE} ${ccode}
        Install_package_wordlist ${LCODE} ${ccode}
        Install_package_font ${LCODE} ${ccode}
        Install_package hyphen ${LCODE} ${ccode}
        Install_package hunspell ${LCODE} ${ccode}
        Install_package aspell ${LCODE} ${ccode}
        Install_package mythes ${LCODE} ${ccode}

    fi

fi

echo "100"


        ) |
        zenity --progress --pulsate\
          --title="$(eval_gettext 'Operation in progress')" \
          --text="$(eval_gettext 'Removing ...')" \
          --width=700 \
          --no-cancel \
          --auto-close


            if [ $? = "1" ]
            then
              sudo dpkg --configure -a
              zenity --error --timeout=$delai_fenetre \
                --text="$(eval_gettext 'Operation canceled.')"
            else
              sudo apt-get -qq autoclean -y
              #sudo apt-get autoremove -y

              # Si le fichier utilisateur existe cela veut dire que l'utilisateur ne veut plus qu'on lui pose cette question
              # donc on le désactive pour les futurs utilisateurs
              if [[ ( -f ${dir_user}/.config/emmabuntus/init_config_remove_language_not_use.txt ) ]]
              then
                sudo cp -f $fichier_init_config $fichier_init_config_final
              fi

            fi



fi


if [[ $choix == *SetDefaultLanguage* ]] ; then

    dm_config_file=${dir_user}/.dmrc

    LCODE=$(echo ${select_locale} | cut -d '_' -f1)

    local_default=$(cat /etc/default/locale | grep ^LANG= | cut -d '=' -f2 | cut -d '"' -f2)

    locale_all="${select_locale}.UTF-8"
    locale_all_min="${select_locale}.utf8"

    # echo "select_locale=$select_locale"
    # echo "locale_all=$locale_all"
    # echo "local_default=$local_default"

    if [[ ${locale_all} != "" ]]; then
        if [[ ${locale_all} != ${local_default} ]]; then

            if test -f ${dm_config_file}
            then
                sed s/^Language=[^$]*/Language=${locale_all_min}/ ${dm_config_file} > ${dm_config_file}.tmp
                cp ${dm_config_file}.tmp ${dm_config_file}
                rm ${dm_config_file}.tmp
            fi

            set_locale_ok=false
            set_locale_return=$(sudo localectl set-locale ${locale_all} 2>&1)

            if [ $? -eq 0 ]; then
                echo "Changement langue valide : ${locale_all}"
                set_locale_ok=true
            else

                set_locale_return=$(sudo localectl set-locale ${select_locale} 2>&1)

                if [ $? -eq 0 ]; then
                    echo "Changement langue valide : ${select_locale}"
                    set_locale_ok=true
                else
                    echo "Changement langue invalide"
                    set_locale_ok=false
                fi

            fi

            if [[ ${set_locale_ok} == true ]] ; then

                # Suppression du fichier de config de Firefox pour permettre une réinitialisation
                # des variables de langue au prochain redémarrage
                rm ${dir_user}/.config/emmabuntus/config_language_firefox.txt

                export WINDOW_DIALOG_RESTART='<window title="'$(eval_gettext 'Changing the default language')'" icon-name="gtk-dialog-question" resizable="false">
                <vbox spacing="0">

                <text use-markup="true" wrap="false" xalign="0" justify="3">
                <input>echo "'${message_set_default_language}'" | sed "s%\\\%%g"</input>
                </text>

                <hbox spacing="10" space-expand="false" space-fill="false">
                <button cancel></button>
                <button can-default="true" has-default="true" use-stock="true" is-focus="true">
                <label>gtk-ok</label>
                <action>exit:OK</action>
                </button>
                </hbox>

                </vbox>
                </window>'

                MENU_DIALOG_RESTART="$(gtkdialog --center --program=WINDOW_DIALOG_RESTART)"

                eval ${MENU_DIALOG_RESTART}
                echo "MENU_DIALOG_RESTART=${MENU_DIALOG_RESTART}"

                if [ ${EXIT} == "OK" ]
                then
                    echo "Restart now"
                    reboot
                fi

            else

                export WINDOW_DIALOG_RESTART='<window title="'$(eval_gettext 'Changing the default language')'" icon-name="gtk-dialog-question" resizable="false">
                <vbox spacing="0">

                <text use-markup="true" wrap="false" xalign="0" justify="3">
                <input>echo "'${message_set_default_language_ko}'" | sed "s%\\\%%g"</input>
                </text>

                <hbox spacing="10" space-expand="false" space-fill="false">
                  <button can-default="true" has-default="true" use-stock="true" is-focus="true">
                <label>gtk-ok</label>
                <action>exit:OK</action>
                </button>
                </hbox>

                </vbox>
                </window>'

                MENU_DIALOG_RESTART="$(gtkdialog --center --program=WINDOW_DIALOG_RESTART)"

                eval ${MENU_DIALOG_RESTART}
                echo "MENU_DIALOG_RESTART=${MENU_DIALOG_RESTART}"

            fi

        fi
    fi

fi

sudo chmod o+r /var/lib/command-not-found/commands.db*

exit 0

#! /bin/bash

# acceptation_install_logiciel_non_libre.sh --
#
#   This file permits to install non-free softwares for the Emmabuntus Distrib.
#
#   Created on 2010-23 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 XFCE/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################


. "$HOME/.config/user-dirs.dirs"

# Test si utilisateur n'appartient pas au groupe sudo
if [[ $(groups | grep sudo) == "" ]] ; then

    echo "User isn't in sudo group !!"
    exit 1

fi

###Pour exporter la librairie de gettext.
set -a
source gettext.sh
set +a
export TEXTDOMAIN=emmabuntus
export TEXTDOMAINDIR="/usr/share/locale"
. gettext.sh
emmabuntus=$0


nom_distribution="Emmabuntus Debian Edition 5"
base_distribution="bookworm"
nom_titre_window="Install non-free softwares"
repertoire_emmabuntus=~/.config/emmabuntus
fichier_init_config_user=${repertoire_emmabuntus}/init_config_non_free_soft.txt
dir_install_non_free_softwares=/opt/Install_non_free_softwares
fichier_init_config_final=$dir_install_non_free_softwares/.init_config_non_free_soft.txt
delai_fenetre=20
delai_fenetre_selection=120
delai_fenetre_progression=1200 ; # temporisation de 20 minutes
dir_install_fonts_arial=/usr/share/fonts/truetype/msttcorefonts/Arial.ttf
dir_install_fonts_calibri=/usr/share/fonts/truetype/fonts_MS_Office_2007/Calibri.ttf

CHECKBOX_RESTART="true"
CHECKBOX_INTERNET_ACTIF="false"

choix=""
utilisateur=emmabuntus

bureau=$XDG_DESKTOP_DIR

source /usr/bin/emmabuntus_found_theme.sh
highlight_color=$(FOUND_HIGHLIGHT_COLOR)

if [[ ! ( -f $fichier_init_config_user || -f $fichier_init_config_final ) ]]
then

message_demarrage="\n\
\<span color=\'${highlight_color}\'>$(eval_gettext 'Would you like to install non-free software on this system ?')\</span>\n\
\n\
\<span color=\'${highlight_color}\'>$(eval_gettext 'Note:')\</span> $(eval_gettext 'You will be able to install these non-free software later on, by going to')\n\
$(eval_gettext 'the application Menu (icon at the top left), then \042Non-Free Software\042.')\n\
\n\
\<span color=\'"'red\'"'>$(eval_gettext 'Warning:')\</span> $(eval_gettext 'The packages \042Proprietary codecs\042 you are about to install are not supported')\n\
$(eval_gettext 'by the Ubuntu or Debian developers communities, and might be illegal in some countries.')\n\
\n\
$(eval_gettext 'The main reason why these \042Proprietary codecs\042 are not included by default is a matter of law.')\n\
$(eval_gettext 'It imposes a fee for each player using these \042Proprietary codecs\042,')\n\
$(eval_gettext 'before installing them, your are responsible to check their usage restrictions in your country.')"


# Initilisation des variables du tableau
CODECS=false ; ARIAL_FONTS=false ; CALIBRI_FONTS=false
msg_codecs="Codecs"
comment_codecs=$(eval_gettext 'Allows you to play protected DVDs')
ARIAL_FONTS_VISIBLE=true ; CALIBRI_FONTS_VISIBLE=true
HEIGHT_WINDOW=400
HEIGHT_CHECKBOX=100


if [[ $(ls /home | grep "^${utilisateur}") ]] ; then

    if [[ $(cat /proc/cmdline | grep -i boot=live) ]]
    then

        CODECS=true
        msg_codecs=$(eval_gettext 'Codecs for audio only')
        ARIAL_FONTS_VISIBLE=false ; CALIBRI_FONTS_VISIBLE=false
        HEIGHT_WINDOW=350
        HEIGHT_CHECKBOX=70

    else

        if [[ $(uname -m | grep -e "x86_64") ]] ; then

            CODECS=true ; ARIAL_FONTS=true ; CALIBRI_FONTS=true
            ARIAL_FONTS_VISIBLE=true ; CALIBRI_FONTS_VISIBLE=true
            HEIGHT_WINDOW=400
            HEIGHT_CHECKBOX=100

        else

            CODECS=true ; ARIAL_FONTS=true ; CALIBRI_FONTS=true
            ARIAL_FONTS_VISIBLE=true ; CALIBRI_FONTS_VISIBLE=true
            HEIGHT_WINDOW=400
            HEIGHT_CHECKBOX=100

        fi

    fi

else

    if [[ $(cat /proc/cmdline | grep -i boot=live) ]]
    then

        CODECS=false
        msg_codecs=$(eval_gettext 'Codecs for audio only')
        comment_codecs=$(eval_gettext 'Allows you to play audio in MP3 format')
        ARIAL_FONTS_VISIBLE=false ; CALIBRI_FONTS_VISIBLE=false
        HEIGHT_WINDOW=350
        HEIGHT_CHECKBOX=70

    else

        if [[ $(uname -m | grep -e "x86_64") ]] ; then

            CODECS=false ; ARIAL_FONTS=false ; CALIBRI_FONTS=false
            ARIAL_FONTS_VISIBLE=true ; CALIBRI_FONTS_VISIBLE=true
            HEIGHT_WINDOW=400
            HEIGHT_CHECKBOX=100

        else

            CODECS=false ; ARIAL_FONTS=false ; CALIBRI_FONTS=false
            ARIAL_FONTS_VISIBLE=true ; CALIBRI_FONTS_VISIBLE=true
            HEIGHT_WINDOW=400
            HEIGHT_CHECKBOX=100

        fi

    fi

fi


# Test de l'installation des paquets pour désactiver la visibilité de ceux-ci
CHECKBOX_CODECS_ACTIF="true"
CHECKBOX_ARIAL_FONTS_ACTIF="true"
CHECKBOX_CALIBRI_FONTS_ACTIF="true"


nom_paquet=libdvdcss2
if [[ $(dpkg --get-selections ${nom_paquet} 2> /dev/null | grep -e "install") ]] ; then
    CHECKBOX_CODECS_ACTIF="false" ; CODECS="true"
fi

if test -f ${dir_install_fonts_arial} ; then
    CHECKBOX_ARIAL_FONTS_ACTIF="false" ; ARIAL_FONTS="true"
fi

if test -f ${dir_install_fonts_calibri} ; then
    CHECKBOX_CALIBRI_FONTS_ACTIF="false" ; CALIBRI_FONTS="true"
fi

if [[ $(ping -w 1 sourceforge.net | grep "1 received") ]] ; then
    # echo "Internet is a live"
    CHECKBOX_INTERNET_ACTIF="true"
else
    # echo "Internet wasn't a live !!"
    CHECKBOX_INTERNET_ACTIF="false"
fi


export WINDOW_DIALOG='<window title="'$(eval_gettext 'Non-free software installation')'" icon-name="gtk-dialog-question" height_request="'${HEIGHT_WINDOW}'" maximize_initially="true" resizable="false">
<vbox spacing="5" space-fill="true" space-expand="true">
<text use-markup="true" wrap="false" xalign="0" justify="3">
<input>echo "'${message_demarrage}'" | sed "s%\\\%%g"</input>
</text>

<hseparator space-expand="true" space-fill="true"></hseparator>

<vbox homogeneous="true" height_request="'${HEIGHT_CHECKBOX}'">
 <checkbox active="'${CODECS}'" sensitive="'${CHECKBOX_CODECS_ACTIF}'" tooltip-text="'${comment_codecs}'">
   <variable>CODECS</variable>
   <label>'${msg_codecs}'</label>
 </checkbox>
 <checkbox active="'${ARIAL_FONTS}'" sensitive="'${CHECKBOX_ARIAL_FONTS_ACTIF}'"  visible="'${ARIAL_FONTS_VISIBLE}'" tooltip-text="'$(eval_gettext 'Allows better compatibility with documents produced by Microsoft Office suite up to the 2003 version')'">
   <variable>ARIAL_FONTS</variable>
   <label>'$(eval_gettext 'Arial Microsoft Fonts')'</label>
 </checkbox>
 <checkbox active="'${CALIBRI_FONTS}'" sensitive="'${CHECKBOX_CALIBRI_FONTS_ACTIF}'"  visible="'${CALIBRI_FONTS_VISIBLE}'" tooltip-text="'$(eval_gettext 'Allows better compatibility with documents produced by Microsoft Office suite since the 2007 version')'">
   <variable>CALIBRI_FONTS</variable>
   <label>'$(eval_gettext 'Calibri Microsoft Fonts')'</label>
 </checkbox>
</vbox>



 <hseparator space-expand="true" space-fill="true"></hseparator>

 <hbox spacing="10" space-expand="false" space-fill="false">
   <checkbox active="'${CHECKBOX_RESTART}'">
   <variable>CHECKBOX_RESTART</variable>
   <label>'$(eval_gettext 'Show this window at next startup')'</label>
   </checkbox>

   <hbox spacing="10" space-expand="true" space-fill="true">
   <button can-default="true" has-default="true" use-stock="true" is-focus="true">
   <label>gtk-cancel</label>
   <action>exit:Cancel</action>
   </button>
   <button ok></button>
   </hbox>
 </hbox>

</vbox>
</window>'


MENU_DIALOG="$(gtkdialog --center --program=WINDOW_DIALOG)"

eval ${MENU_DIALOG}
# echo "MENU_DIALOG=${MENU_DIALOG}"


if [[ ${CODECS} == "true" && ${CHECKBOX_CODECS_ACTIF} == "true" ]]
then
    choix=${choix}:Codecs
fi

if [[ ${ARIAL_FONTS} == "true" && ${CHECKBOX_ARIAL_FONTS_ACTIF} == "true" ]]
then
    choix=${choix}:Arial
fi

if [[ ${CALIBRI_FONTS} == "true" && ${CHECKBOX_CALIBRI_FONTS_ACTIF} == "true" ]]
then
    choix=${choix}:Calibri
fi


# echo "choix=${choix}"


if [[ $(cat /proc/cmdline | grep -i boot=live) ]]
then
   mode_live=1
else
   mode_live=0
fi


if [ ${CHECKBOX_RESTART} == "false" ]
then
    echo $nom_distribution | tee $fichier_init_config_user > /dev/null
    echo "Fichier servant à ne pas afficher la fenêtre de bienvenue" | tee -a $fichier_init_config_user > /dev/null
fi


if [ ${EXIT} == "OK" ]
then

    if [[ ${choix} != "" ]]
    then

        user=$USER

        pkexec /usr/bin/acceptation_install_logiciel_non_libre_exec.sh "$choix" "$mode_live" "${user}"


    fi

else
        # Appel de la fonction pour patch dépot suite à installation de Calamares
        user=$USER

        pkexec /usr/bin/acceptation_install_logiciel_non_libre_exec.sh "patch_depot" "$mode_live" "${user}"

fi



fi

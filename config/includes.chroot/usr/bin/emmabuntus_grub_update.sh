#!/bin/bash

# emmabuntus_grub_update.sh --
#
#   This file permits to update grub menu
#
#   Created on 2010-23 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 XFCE/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################


###Pour exporter la librairie de gettext.
set -a
source gettext.sh
set +a
export TEXTDOMAIN=emmabuntus
export TEXTDOMAINDIR="/usr/share/locale"
. gettext.sh
emmabuntus=$0

source /usr/bin/emmabuntus_found_theme.sh
highlight_color=$(FOUND_HIGHLIGHT_COLOR)

message_demarrage="\n\
$(eval_gettext 'Do you want to add others systems to GRUB menu? ')\n\
\n\
$(eval_gettext 'After applying this update, you need to sign out or restart your computer to start others systems.')"

export WINDOW_DIALOG='<window title="'$(eval_gettext 'Launch os-prober')'" icon-name="gtk-dialog-question" resizable="false">
<vbox spacing="0">

<text use-markup="true" wrap="false" xalign="0" justify="3">
<input>echo "'$message_demarrage'" | sed "s%\\\%%g"</input>
</text>

<hbox space-expand="false" space-fill="false">
<button>
<input file icon="gtk-no"></input>
<label>"'$(eval_gettext 'Cancel')'"</label>
<action>exit:exit</action>
</button>
<button can-default="true" has-default="true" use-stock="true" is-focus="true">
<label>gtk-ok</label>
<action>exit:OK</action>
</button>
</hbox>
</vbox>

</window>'

MENU_DIALOG="$(gtkdialog --center --program=WINDOW_DIALOG)"

eval ${MENU_DIALOG}
# echo "MENU_DIALOG=${MENU_DIALOG}"

if  [ ${EXIT} == "OK" ]
then
    pkexec /usr/bin/emmabuntus_grub_update_exec.sh
fi

#! /bin/bash

# lock_cairo_dock.sh --
#
#   This file handles the lock/unlock machanism of the cairo-dock
#   in the Emmabuntüs distribution
#
#   Created on 2010-22 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was checked and validated on Debian 12 XFCE/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

var=$1

###Pour exporter la librairie de gettext.
set -a
source gettext.sh
set +a
export TEXTDOMAIN=emmabuntus
export TEXTDOMAINDIR="/usr/share/locale"
. gettext.sh
emmabuntus=$0


nom_distribution="Emmabuntus Debian Edition 5"

if [[ ${USER} == root ]] ; then
dir_user=/root
else
dir_user=/home/${USER}
fi

repertoire_emmabuntus=${dir_user}/.config/emmabuntus
env_emmabuntus=${repertoire_emmabuntus}/env_emmabuntus

cmd_lock_dock=""

langue=`echo $LANG | cut -d_ -f 1 `

#  chargement des variables d'environnement
# (loading environment variables)
. ${env_emmabuntus}

source /usr/bin/emmabuntus_found_theme.sh
highlight_color=$(FOUND_HIGHLIGHT_COLOR)

nom_logiciel_affichage=

message_info_dock="\n\
\<span color=\'${highlight_color}\'>Information :\</span>$(eval_gettext 'In the context of education or public presentation usage'),     \n\
$(eval_gettext 'we recommend the dock lock activation.')"

message_lock_dock="\n\
$(eval_gettext 'Want to lock the Emmabuntüs default dock (Cairo-Dock) ?')\n\
${message_info_dock}"

message_unlock_dock="\n\
$(eval_gettext 'Want to unlock the Emmabuntüs default dock (Cairo-Dock) ?')\n\
${message_info_dock}"


# Test de l'état du vérrouillage du dock
# (test of the dock locking state)
if [[ $Lock_Dock == "1" ]]
then
    # Dock verrouillé (dock is locked)
    message_info_dock=${message_unlock_dock}
    cmd_lock_dock=0
else
    # Dock déverrouillé (dock is unlocked)
    message_info_dock=${message_lock_dock}
    cmd_lock_dock=1
fi


export WINDOW_DIALOG_LOCK_DOCK='<window title="'$(eval_gettext 'Lock/Unlock dock')'" icon-name="gtk-dialog-question" resizable="false">
<vbox spacing="0">

<text use-markup="true" wrap="false" xalign="0" justify="3">
<input>echo "'$message_info_dock'" | sed "s%\\\%%g"</input>
</text>

<hbox spacing="10" space-expand="false" space-fill="false">
<button cancel></button>
<button can-default="true" has-default="true" use-stock="true" is-focus="true">
<label>gtk-ok</label>
<action>exit:OK</action>
</button>
</hbox>

</vbox>
</window>'

MENU_DIALOG_LOCK_DOCK="$(gtkdialog --center --program=WINDOW_DIALOG_LOCK_DOCK)"

eval ${MENU_DIALOG_LOCK_DOCK}
echo "MENU_DIALOG_LOCK_DOCK=${MENU_DIALOG_LOCK_DOCK}"


if [ ${EXIT} == "OK" ]
then

    if ! [ ${cmd_lock_dock} = "" ]
    then

        if [[ ${var} == "Init" ]]
        then

            /usr/bin/lock_cairo_dock_exec.sh ${cmd_lock_dock} ${env_emmabuntus}

        else

            pkexec /usr/bin/lock_cairo_dock_exec.sh ${cmd_lock_dock} ${env_emmabuntus}

            # Redémarrage du dock pour prendre en compte les modifications de la configuration
            # (Restarting the dock to take into account the modifications of the configuration)

            /usr/bin/start_cairo_dock.sh &
        fi

    fi

fi






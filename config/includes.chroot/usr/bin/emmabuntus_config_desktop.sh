#! /bin/bash

# Emmabuntus_config_desktop.sh --
#
#   This file permits to config :
#    - RadioTray-NG config file
#    - Shortcuts to the user's folders
#
#   Created on 2010-22 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 XFCE/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

. "$HOME/.config/user-dirs.dirs"

nom_distribution="Emmabuntus Debian Edition 5"
repertoire_emmabuntus=~/.config/emmabuntus
fichier_init_config_user=${repertoire_emmabuntus}/init_config_shortcut.txt
repertoire_radiotray=~/.config/radiotray-ng
fichier_radiotray=bookmarks
fichier_radiotray_extension=json

langue=`echo $LANG | cut -d_ -f 1 `


# Gestion des raccourcis de RadioTray en fonction de la langue
if (test -f ${repertoire_radiotray}/${fichier_radiotray}_${langue}.${fichier_radiotray_extension})
then

    if (test -L ${repertoire_radiotray}/${fichier_radiotray}.${fichier_radiotray_extension}) ; then unlink  ${repertoire_radiotray}/${fichier_radiotray}.${fichier_radiotray_extension} ; fi
    if (test -f ${repertoire_radiotray}/${fichier_radiotray}.${fichier_radiotray_extension}) ; then rm ${repertoire_radiotray}/${fichier_radiotray}.${fichier_radiotray_extension} ; fi
    ln -s  ${repertoire_radiotray}/${fichier_radiotray}_${langue}.${fichier_radiotray_extension} ${repertoire_radiotray}/${fichier_radiotray}.${fichier_radiotray_extension}

else

    if (test -L ${repertoire_radiotray}/${fichier_radiotray}.${fichier_radiotray_extension}) ; then unlink  ${repertoire_radiotray}/${fichier_radiotray}.${fichier_radiotray_extension} ; fi
    if (test -f ${repertoire_radiotray}/${fichier_radiotray}.${fichier_radiotray_extension}) ; then rm ${repertoire_radiotray}/${fichier_radiotray}.${fichier_radiotray_extension} ; fi
    ln -s ${repertoire_radiotray}/${fichier_radiotray}_all.${fichier_radiotray_extension} ${repertoire_radiotray}/${fichier_radiotray}.${fichier_radiotray_extension}

fi

# Gestion des raccourcis vers les dossiers de l'utilisateur
if [[ ! ( -f ~/.config/gtk-3.0/bookmarks || -f $fichier_init_config_user ) ]]
then

    echo "file://${XDG_DOWNLOAD_DIR}" >> ~/.config/gtk-3.0/bookmarks
    echo "file://${XDG_DOCUMENTS_DIR}" >> ~/.config/gtk-3.0/bookmarks
    echo "file://${XDG_PICTURES_DIR}" >> ~/.config/gtk-3.0/bookmarks
    echo "file://${XDG_MUSIC_DIR}" >> ~/.config/gtk-3.0/bookmarks
    echo "file://${XDG_VIDEOS_DIR}" >> ~/.config/gtk-3.0/bookmarks

    echo $nom_distribution | tee $fichier_init_config_user > /dev/null
    echo "Fichier servant à ne pas relancer la création des raccourcis vers les dossiers de l'utilisateur" | tee -a $fichier_init_config_user > /dev/null

fi

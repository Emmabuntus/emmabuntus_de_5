#!/bin/bash

# Emmabuntus_create_cache_apt.sh --
#
#   This file permits to create apt cache
#   is the cache is not present for Emmabuntüs distribution.
#
#   Created on 2010-24 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 XFCE/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################


repertoire_script=/usr/bin
file_cache_apt=/var/cache/apt/pkgcache.bin
TAILLE_FILE_CACHE_APT_OK=5000000

repertoire_emmabuntus=~/.config/emmabuntus
fichier_log=${repertoire_emmabuntus}/emmabuntus_create_cache_apt_log.txt

cache_apt_exite="true"
file_cache_apt_size=0
action=""


file_cache_apt=/var/cache/apt/pkgcache.bin
# Désactivation de la mise à jour du cache apt en mode Live
# car cela utilise trop de RAM
if [[ $(cat /proc/cmdline | grep -i boot=live) ]]
then

    # echo "Mode live"

    exit 1

fi


if [[ $(ping -w 1 sourceforge.net | grep "1 received") ]] ; then

echo "Internet is a live" > ${fichier_log}


# teste si le cache apt existe

if [[ -f ${file_cache_apt} ]] ; then
    echo "Cache apt present" >> ${fichier_log}

    file_cache_apt_size=$(stat -c %s ${file_cache_apt})

    if test ${file_cache_apt_size} -gt ${TAILLE_FILE_CACHE_APT_OK}
    then
        echo "Cache apt size OK" >> ${fichier_log}
        cache_apt_exite="true"
    else
        echo "Cache apt size KO" >> ${fichier_log}
        cache_apt_exite="false"
    fi

else
    echo "Cache apt not present" >> ${fichier_log}
    cache_apt_exite="false"
fi


if [[ ${cache_apt_exite} == "false" ]] ; then

   echo "Create cache apt" >> ${fichier_log}
   pkexec ${repertoire_script}/emmabuntus_create_cache_apt_exec.sh

fi

fi


exit 0


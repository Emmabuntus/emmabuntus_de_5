#!/bin/bash


# emmabuntus_switch_lxqt.sh --
#
#   This file used to switch to LXQt for the Emmabuntüs Distrib.
#
#   Created on 2010-22 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 XFCE/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

########################################################################################################

rep_conf_login="/var/lib/AccountsService/users"
file_conf_session="/usr/share/xsessions/lxqt.desktop"

user=$1

# Controle de l'existance de l'utilisateur
if ! test -d /home/${user} ; then

    echo "Utilisateur inconnu : ${user}"
    exit 1

fi


file_conf_login="${rep_conf_login}/${user}"


if [[ -f ${file_conf_login} && -f ${file_conf_session} ]] ; then

    echo "Modif fichier de config"

    if [[ $(sudo cat ${file_conf_login} | grep "XSession") ]] ; then

        echo "Modif Xsession"

        sed s/^XSession=[^$]*/XSession=lxqt/ ${file_conf_login} | sudo tee ${file_conf_login}.tmp > /dev/null
        sudo cp ${file_conf_login}.tmp ${file_conf_login}
        sudo rm ${file_conf_login}.tmp

    else

        echo "Ajout Xsession"

        sed s/^SystemAccount/XSession=lxqt\\nSystemAccount/ ${file_conf_login} | sudo tee ${file_conf_login}.tmp > /dev/null
        sudo cp ${file_conf_login}.tmp ${file_conf_login}
        sudo rm ${file_conf_login}.tmp

    fi

    if [[ $(sudo cat ${file_conf_login} | grep "^Session") ]] ; then

        echo "Modif Session"

        sed s/^Session=[^$]*/Session=lxqt/ ${file_conf_login} | sudo tee ${file_conf_login}.tmp > /dev/null
        sudo cp ${file_conf_login}.tmp ${file_conf_login}
        sudo rm ${file_conf_login}.tmp
    else

        echo "Ajout Session"

        sed s/^SystemAccount/Session=lxqt\\nSystemAccount/ ${file_conf_login} | sudo tee ${file_conf_login}.tmp > /dev/null
        sudo cp ${file_conf_login}.tmp ${file_conf_login}
        sudo rm ${file_conf_login}.tmp

    fi

    sudo systemctl restart accounts-daemon
    sudo systemctl restart lightdm

elif [[ -f ${file_conf_session} ]] ; then

    echo "[User]" | sudo tee ${file_conf_login} > /dev/null
    echo "Language=${LANG}" | sudo tee -a ${file_conf_login} > /dev/null
    echo "XSession=lxqt" | sudo tee -a ${file_conf_login} > /dev/null
    echo "Session=lxqt" | sudo tee -a ${file_conf_login} > /dev/null
    echo "SystemAccount=false" | sudo tee -a ${file_conf_login} > /dev/null

    sudo systemctl restart accounts-daemon
    sudo systemctl restart lightdm
fi

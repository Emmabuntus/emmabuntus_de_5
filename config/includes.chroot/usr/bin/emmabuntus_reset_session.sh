#! /bin/bash

# emmabuntus_reset_session.sh --
#
#   This file permits to install reset user session for the Emmabuntus Distrib.
#
#   Created on 2010-23 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 XFCE/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

if [[ ${USER} == "invite" || ${USER} == "guest" ]] ; then

    dir_user=/home/${USER}

    rm -R ${dir_user}

    mkdir ${dir_user}

    cp -R /etc/skel/. ${dir_user}

    # Permet de ne pas poser les questions lors du lancement de la session
    dir_user_config="${dir_user}/.config/emmabuntus/"
    env_emmabuntus="${dir_user_config}/env_emmabuntus"

    touch ${dir_user_config}/init_config_non_free_soft.txt
    touch ${dir_user_config}/init_config_remove_language_not_use.txt
    touch ${dir_user_config}/init_config_shortcut.txt
    touch ${dir_user_config}/lxqt_init_config_postinstall.txt
    touch ${dir_user_config}/init_emmabuntus_config.txt
    touch ${dir_user_config}/init_emmabuntus_lxqt.txt
    touch ${dir_user_config}/init_gspeech_LXQT.txt
    touch ${dir_user_config}/init_gspeech_XCFE.txt
    touch ${dir_user_config}/lxqt_init_config_dock.txt
    touch ${dir_user_config}/lxqt_init_config_postinstall.txt
    touch ${dir_user_config}/xfce_init_config_dock.txt
    touch ${dir_user_config}/xfce_init_config_postinstall.txt


    # Activation du Dock XFCE
    sed s/"^Dock_XFCE=\"[0-9]*\""/"Dock_XFCE=\"1\""/ ${env_emmabuntus} > ${env_emmabuntus}.tmp
    cp ${env_emmabuntus}.tmp ${env_emmabuntus}
    rm ${env_emmabuntus}.tmp

    # Activation du Dock LXQT
    sed s/"^Dock_LXQT=\"[0-9]*\""/"Dock_LXQT=\"1\""/ ${env_emmabuntus} > ${env_emmabuntus}.tmp
    cp ${env_emmabuntus}.tmp ${env_emmabuntus}
    rm ${env_emmabuntus}.tmp

    # Verrouillage du Dock
    sed s/"^Lock_Dock=\"[0-9]*\""/"Lock_Dock=\"1\""/ ${env_emmabuntus} > ${env_emmabuntus}.tmp
    cp ${env_emmabuntus}.tmp ${env_emmabuntus}
    rm ${env_emmabuntus}.tmp

    # Activation fenêtre de bienvenue
    sed s/"^Welcome_window=\"[0-9]*\""/"Welcome_window=\"1\""/ ${env_emmabuntus} > ${env_emmabuntus}.tmp
    cp ${env_emmabuntus}.tmp ${env_emmabuntus}
    rm ${env_emmabuntus}.tmp

    # Définition du niveau du dock
    echo "Dock=Simple" > ${dir_user}/.config/cairo-dock-language/cairo-dock-choise-dock.conf

    chown -R ${USER}:${USER} ${dir_user}

    chmod -R u+rw ${dir_user}

fi


#! /bin/bash

# copy_env_user_system_exec.sh --
#
#   This file permits to copy user confif to system config for the Emmabuntus Distrib.
#
#   Created on 2010-24 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 Xfce/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

###Pour exporter la librairie de gettext.
set -a
source gettext.sh
set +a
export TEXTDOMAIN=emmabuntus
export TEXTDOMAINDIR="/usr/share/locale"
. gettext.sh
emmabuntus=$0


dir_user_config="/home/oem"
dir_system_config="/etc/skel"
dir_emmabuntus_config="/opt/User_Emmabuntus"
delai_fenetre=20
delai_fenetre_progression=60 ; # temporisation de 1 minute

copie_env_utilisateur="false"
copie_postinstall_oem="false"

copie_env_utilisateur=$1
copie_postinstall_oem=$2

if [[ ($USER == "oem") ]] ; then


    # 1 - sous-répertoire, 2 - répertoire cible, 3 - répertoire source, 4- force si répertoire cible n'existe pas
    function CP_DIR_USER_SYS()
    {
        dir=${1}
        dir_dst=${2}
        dir_src=${3}
        force=${4}

        if [[ ! -d ${dir_src}/${dir} ]] ; then
            echo "Répertoire source non présent : ${dir_src}/${dir}"
        elif [[ -d ${dir_dst}/${dir} ]] ; then
            sudo rm -R ${dir_dst}/${dir}
            sudo cp -R ${dir_src}/${dir} ${dir_dst}
            sudo chmod -R a+rwX ${dir_dst}/${dir}
        elif [[ ${force} == "1" ]] ; then
            sudo cp -R ${dir_src}/${dir} ${dir_dst}
            sudo chmod -R a+rwX ${dir_dst}/${dir}
        else
            echo "Répertoire destination non présent : ${dir_dst}/${dir}"
        fi

    }

    # 1 - fichier, 2 - répertoire cible, 3 - répertoire source, 4- force si fichier cible n'existe pas
    function CP_FILE_USER_SYS()
    {
        file=${1}
        dir_dst=${2}
        dir_src=${3}
        force=${4}

        if [[ ! -f ${dir_src}/${file} ]] ; then
            echo "Fichier source non présent : ${dir_src}/${file}"
        elif [[ -f ${dir_dst}/${file} ]] ; then
            sudo rm ${dir_dst}/${file}
            sudo cp ${dir_src}/${file} ${dir_dst}
            sudo chmod a+r ${dir_dst}/${file}
        elif [[ ${force} == "1" ]] ; then
            sudo cp ${dir_src}/${file} ${dir_dst}
            sudo chmod a+r ${dir_dst}/${file}
        else
            echo "Fichier destination non présent : ${dir_dst}/${file}"
        fi

    }

    (

    if [[ ${copie_env_utilisateur} == "true" ]] ; then

        CP_DIR_USER_SYS .thunderbird                ${dir_emmabuntus_config}          ${dir_user_config} 0
        CP_DIR_USER_SYS .mozilla                    ${dir_emmabuntus_config}          ${dir_user_config} 0

        CP_DIR_USER_SYS OpenBoard                   ${dir_system_config}/.local/share ${dir_user_config}/.local/share 0
        CP_DIR_USER_SYS file-manager                ${dir_system_config}/.local/share ${dir_user_config}/.local/share 0
        CP_DIR_USER_SYS applications                ${dir_system_config}/.local/share ${dir_user_config}/.local/share 0

        CP_DIR_USER_SYS .minetest                   ${dir_system_config}              ${dir_user_config} 0
        CP_DIR_USER_SYS .hplip                      ${dir_system_config}              ${dir_user_config} 0
        CP_DIR_USER_SYS .gconf                      ${dir_system_config}              ${dir_user_config} 0
        CP_DIR_USER_SYS .eteks                      ${dir_system_config}              ${dir_user_config} 0

        CP_DIR_USER_SYS calibre                     ${dir_system_config}/.config      ${dir_user_config}/.config 0
        CP_DIR_USER_SYS Clementine                  ${dir_system_config}/.config      ${dir_user_config}/.config 0
        CP_DIR_USER_SYS dconf                       ${dir_system_config}/.config      ${dir_user_config}/.config 0
        CP_DIR_USER_SYS falkon                      ${dir_system_config}/.config      ${dir_user_config}/.config 0
        CP_DIR_USER_SYS fontconfig                  ${dir_system_config}/.config      ${dir_user_config}/.config 0
        CP_DIR_USER_SYS geany                       ${dir_system_config}/.config      ${dir_user_config}/.config 0
        CP_DIR_USER_SYS GIMP                        ${dir_system_config}/.config      ${dir_user_config}/.config 0
        CP_DIR_USER_SYS libreoffice                 ${dir_system_config}/.config      ${dir_user_config}/.config 0
        CP_DIR_USER_SYS package-update-indicator    ${dir_system_config}/.config      ${dir_user_config}/.config 0
        CP_DIR_USER_SYS pcmanfm-qt                  ${dir_system_config}/.config      ${dir_user_config}/.config 0
        CP_DIR_USER_SYS qterminal.org               ${dir_system_config}/.config      ${dir_user_config}/.config 0
        CP_DIR_USER_SYS quodlibet                   ${dir_system_config}/.config      ${dir_user_config}/.config 0
        CP_DIR_USER_SYS radiotray-ng                ${dir_system_config}/.config      ${dir_user_config}/.config 0
        CP_DIR_USER_SYS Terminal                    ${dir_system_config}/.config      ${dir_user_config}/.config 0
        CP_DIR_USER_SYS Thunar                      ${dir_system_config}/.config      ${dir_user_config}/.config 0


        CP_DIR_USER_SYS .www.kiwix.org              ${dir_system_config}/.config      ${dir_user_config}/.config 1

        CP_FILE_USER_SYS .bash_logout               ${dir_system_config}             ${dir_user_config}         0
        CP_FILE_USER_SYS .bashrc                    ${dir_system_config}             ${dir_user_config}         0
        CP_FILE_USER_SYS .lirecouleur               ${dir_system_config}             ${dir_user_config}         0
        CP_FILE_USER_SYS .picosvoxooo               ${dir_system_config}             ${dir_user_config}         0
        CP_FILE_USER_SYS .profile                   ${dir_system_config}             ${dir_user_config}         0
        CP_FILE_USER_SYS .tuxpaintrc                ${dir_system_config}             ${dir_user_config}         0

        CP_FILE_USER_SYS kdenliverc                 ${dir_system_config}/.config     ${dir_user_config}/.config 0
        CP_FILE_USER_SYS lxqt-mimeapps.list         ${dir_system_config}/.config     ${dir_user_config}/.config 0
        CP_FILE_USER_SYS mimeapps.list              ${dir_system_config}/.config     ${dir_user_config}/.config 0
        CP_FILE_USER_SYS Trolltech.conf             ${dir_system_config}/.config     ${dir_user_config}/.config 0


    fi

    if [[ (${copie_postinstall_oem} == "true") && (${copie_env_utilisateur} == "true") ]] ; then

        CP_DIR_USER_SYS autostart                   ${dir_system_config}/.config      ${dir_user_config}/.config 0

        CP_FILE_USER_SYS keyboard-layout.xml        ${dir_user_config}/xfce4/xfconf/xfce-perchannel-xml   ${dir_system_config}/xfce4/xfconf/xfce-perchannel-xml 0
        CP_DIR_USER_SYS xfce4                       ${dir_system_config}/.config      ${dir_user_config}/.config 0

        CP_DIR_USER_SYS lxqt                        ${dir_system_config}/.config      ${dir_user_config}/.config 0

        CP_DIR_USER_SYS emmabuntus                  ${dir_system_config}/.config      ${dir_user_config}/.config 0
        sudo rm ${dir_system_config}/.config/emmabuntus/config_language_firefox.txt
        sudo rm ${dir_system_config}/.config/emmabuntus/postinstall_calamares.txt

        CP_DIR_USER_SYS cairo-dock-language         ${dir_system_config}/.config      ${dir_user_config}/.config 0

        CP_FILE_USER_SYS redshift.conf              ${dir_system_config}/.config     ${dir_user_config}/.config 0

        CP_DIR_USER_SYS gtk-3.0                     ${dir_system_config}/.config      ${dir_user_config}/.config 0

        # Evite de copier les raccourcis des dossiers
        sudo rm ${dir_system_config}/.config/gtk-3.0/bookmarks

        CP_DIR_USER_SYS gtk-2.0                     ${dir_system_config}/.config      ${dir_user_config}/.config 0
        CP_FILE_USER_SYS .gtkrc-2.0                 ${dir_system_config}              ${dir_user_config}         0

        CP_FILE_USER_SYS .xscreensaver              ${dir_system_config}             ${dir_user_config}          0

        # Si lancement de la finalisation de l'install OEM sous LXQt alors config de Calamares pour installer le lanceur de démarrage sous LXQt
        if [[ $(ps -A | grep "lxqt-session") ]] ; then
            if [[ -f /opt/calamares/displaymanager-lxqt.conf ]] ; then
                sudo cp /opt/calamares/displaymanager-lxqt.conf /etc/calamares/modules/displaymanager.conf
            fi
        fi

    elif [[ (${copie_postinstall_oem} == "false") && (${copie_env_utilisateur} == "true") ]] ; then

        CP_FILE_USER_SYS redshift-gtk.desktop             ${dir_user_config}/.config/autostart   ${dir_system_config}/.config/autostart 0
        CP_FILE_USER_SYS clipit-startup.desktop           ${dir_user_config}/.config/autostart   ${dir_system_config}/.config/autostart 0
        CP_FILE_USER_SYS lxqt-qlipper.desktop             ${dir_user_config}/.config/autostart   ${dir_system_config}/.config/autostart 0
        CP_DIR_USER_SYS autostart                         ${dir_system_config}/.config           ${dir_user_config}/.config 0

        CP_FILE_USER_SYS keyboard-layout.xml              ${dir_user_config}/xfce4/xfconf/xfce-perchannel-xml   ${dir_system_config}/xfce4/xfconf/xfce-perchannel-xml 0
        CP_FILE_USER_SYS xfce4-desktop.xml                ${dir_user_config}/xfce4/xfconf/xfce-perchannel-xml   ${dir_system_config}/xfce4/xfconf/xfce-perchannel-xml 0
        CP_DIR_USER_SYS xfce4                             ${dir_system_config}/.config      ${dir_user_config}/.config 0

        CP_DIR_USER_SYS cairo-dock-language               ${dir_system_config}/.config      ${dir_user_config}/.config 0
        sudo rm ${dir_system_config}/.config/cairo-dock-language/cairo-dock-choise-dock.conf
        sudo rm ${dir_system_config}/.config/cairo-dock-language/cairo-dock-choise-dock-old.conf

        CP_DIR_USER_SYS gtk-2.0                           ${dir_system_config}/.config      ${dir_user_config}/.config 0

    fi

    ) |
    zenity --progress --pulsate \
      --title="$(eval_gettext 'Copying in progress')" \
      --text="$(eval_gettext 'Copy ...')" \
      --width=500 \
      --percentage=0 --auto-close --no-cancel \
      --timeout=$delai_fenetre_progression
        if [ $? = "1" ]
        then
          zenity --error --timeout=$delai_fenetre \
            --text="$(eval_gettext 'Copy canceled.')"
        fi

        exit 0

else

    exit 1

fi



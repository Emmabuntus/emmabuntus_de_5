#! /bin/bash


# emmabuntus_patch_calamares_locale_exec.sh --
#
#   This file permits to patch on locale files created by Calamares.
#
#   Created on 2010-22 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 XFCE/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


########################################################################################################


fichier_locale_default=/etc/default/locale
fichier_locale_conf=/etc/locale.conf


if test -f  ${fichier_locale_conf}; then rm ${fichier_locale_conf}; fi

if test -f  ${fichier_locale_default}; then

    sed /"^LC_"/d ${fichier_locale_default} | sudo tee ${fichier_locale_default}.tmp > /dev/null
    sudo cp ${fichier_locale_default}.tmp ${fichier_locale_default}
    sudo rm ${fichier_locale_default}.tmp

fi

exit 0

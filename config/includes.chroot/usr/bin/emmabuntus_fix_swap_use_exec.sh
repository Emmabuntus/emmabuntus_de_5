#! /bin/bash


# Emmabuntus_fix_swap_use_exec.sh --
#
#   This file permits to automatically repare the usage of swap
#   after installation for Emmabuntüs distribution.
#
#   Created on 2010-22 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 XFCE/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


########################################################################################################


###Pour exporter la librairie de gettext.
set -a
source gettext.sh
set +a
export TEXTDOMAIN=emmabuntus
export TEXTDOMAINDIR="/usr/share/locale"
. gettext.sh
emmabuntus=$0


nom_distribution="Emmabuntus Debian Edition 5"
dir_install_non_free_softwares=/opt/Install_non_free_softwares
fichier_init_config_final=$dir_install_non_free_softwares/.init_config_fix_swap_use.txt
fstab_file=/etc/fstab

if [[ ${USER} == root ]] ; then
dir_user=/root
else
dir_user=/home/${USER}
fi

action=""
nb_ligne_fstab=1
nb_ligne_fstab_tmp1=0


# Récupération de l'argument après le passage en root
action=$1
swap_disk=$2
swap_uuid_disk=$3
swap_uuid_fsfab=$4
user=$5

#echo "action=${action}"
#echo "swap_disk=${swap_disk}"
#echo "swap_uuid_disk=${swap_uuid_disk}"
#echo "swap_uuid_fsfab=${swap_uuid_fsfab}"


if [ ${action} = "NO_SWAP_ON_FSTAB" ]; then

    # swap was on /dev/sdb1 during post-installation
    # UUID=cfc0e3b3-d72c-45a5-b4b2-8a1eff55ca21 none            swap    sw              0       0

    echo "# swap was on during ${swap_disk} post-installation" | sudo tee -a ${fstab_file} > /dev/null
    echo "UUID=${swap_uuid_disk} none            swap    sw              0       0" | sudo tee -a ${fstab_file} > /dev/null

    sudo /sbin/swapon --all

elif [ ${action} == "SWAP_INCORRECT_ON_FSTAB" ]; then

    disk=$(echo "${swap_disk}" |cut -d "/" -f3)

    sed s/^#\ swap\ was\ on\ [^$]*/#\ swap\ was\ on\ during\ \\/dev\\/${disk}\ post-installation/ ${fstab_file} | sudo tee ${fstab_file}.tmp > /dev/null

    sed s/UUID=[^s]*swap/UUID=${swap_uuid_disk}\ none\ \ \ \ \ \ \ \ \ \ \ \ swap/ ${fstab_file}.tmp | sudo tee ${fstab_file}.tmp1 > /dev/null

    # Sécurité remplacement fstab d'origine par controle du nombre de ligne du fichier d'origne avec celui modifié
    nb_ligne_fstab=$(cat ${fstab_file} | wc -l)
    nb_ligne_fstab_tmp1=$(cat ${fstab_file}.tmp1 | wc -l)

    if [[ ${nb_ligne_fstab} == ${nb_ligne_fstab_tmp1} ]]; then
        echo "Fstab corrected"
        sudo cp ${fstab_file}.tmp1 ${fstab_file}
    else
        echo "Fstab not changed, because number of lines of original file is different of corrected file"
        zenity --error --text="$(eval_gettext 'Installation Failed.')"
    fi

    sudo rm ${fstab_file}.tmp
    sudo rm ${fstab_file}.tmp1

    sudo /sbin/swapon --all

fi

# Si le fichier utilisateur existe cela veut dire que l'utilisateur ne veut plus qu'on lui pose cette question
# donc on le désactive pour les futurs utilisateurs
if [[ ( -f ${dir_user}/.config/emmabuntus/init_config_fix_swap_use.txt ) ]]
then
  sudo cp -f ${dir_user}/.config/emmabuntus/init_config_fix_swap_use.txt $fichier_init_config_final
fi

exit 0

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#
#   Calamares is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   Calamares is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with Calamares. If not, see <http://www.gnu.org/licenses/>.


from libcalamares.utils import target_env_call
from os.path import exists


class GrubPatch:

    def run(self):
        if exists('/etc/default/grub.calamares'):
            target_env_call(['mv', '/etc/default/grub.calamares', '/etc/default/grub'])

def run():

    patch = GrubPatch()

    return patch.run()
    

#! /bin/bash

# install_codecs_audio.sh --
#
#   This file permits to install non-free Codecs Audio for the Emmabuntus Distrib.
#
#   Created on 2010-24 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 XFCE/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################


nom_distribution="Emmabuntus Debian Edition 5"
base_distribution="bookworm"

nom_logiciel_affichage="Codecs Video"
nom_application="Codecs Video"



dir_install_non_free_softwares=/opt/Install_non_free_softwares
delai_fenetre=20
delai_fenetre_selection=120
utilisateur=emmabuntus



#  chargement des messages
. /opt/Install_non_free_softwares/emmabuntus_messages.sh


if test -z "$message_charge" ; then

    echo "# $nom_logiciel_affichage software installation canceled : Load message KO"

    exit 0

fi


(

#Désactivation de Gmenu de Cairo-dock pendant l'installation de logiciel pour éviter les fenêtres de notifications
/usr/bin/cairo_dock_gmenu_off.sh

# non-free-codecs

/opt/Depots/install_depot_non_free.sh

if [[ $(ping -w 1 sourceforge.net | grep "1 received") ]] ; then
    #echo "Internet is a live"

    # Suppression de la surveillance de la mise à jour des paquets
    pkill pk-update-icon

    echo "# $msg_repository_update"
    sudo apt-get -qq update

fi

echo "100" ;

) |
zenity --progress --pulsate \
  --title="$install_title" \
  --text="$install_text" \
  --width=500 \
  --percentage=0 --auto-close --no-cancel
    if [ $? = "1" ]
    then
      sudo dpkg --configure -a

      #Activation de Gmenu de Cairo-dock
      /usr/bin/cairo_dock_gmenu_on.sh

      zenity --error --timeout=$delai_fenetre \
        --text="$install_cancel"
    else
      sudo dpkg --configure -a
      echo "# Install Completed"
    fi


(

# Installation des logiciels non libres

echo "# $msg_install $nom_logiciel_affichage, $thank_waiting"

if [[ $(ping -w 1 sourceforge.net | grep "1 received") ]] ; then
    #echo "Internet is a live"

    sudo gdebi -n -q ${dir_install_non_free_softwares}/Codecs/gstreamer1.0-fluendo-mp3*.deb

    sudo DEBIAN_FRONTEND=noninteractive apt-get install -y -qq regionset
    sudo DEBIAN_FRONTEND=noninteractive apt-get install -y -qq unrar

    if [[ $(uname -m | grep -e "x86_64") ]] ; then
        sudo gdebi -n -q ${dir_install_non_free_softwares}/Codecs/w64codecs*.deb
    else
        sudo gdebi -n -q ${dir_install_non_free_softwares}/Codecs/w32codecs*.deb
    fi

    sudo gdebi -n -q ${dir_install_non_free_softwares}/libdvdcss2*.deb

else

    sudo gdebi -n -q ${dir_install_non_free_softwares}/Codecs/gstreamer1.0-fluendo-mp3*.deb

    sudo gdebi -n -q ${dir_install_non_free_softwares}/Codecs/regionset*.deb
    sudo gdebi -n -q ${dir_install_non_free_softwares}/Codecs/unrar*.deb

    if [[ $(uname -m | grep -e "x86_64") ]] ; then
        sudo gdebi -n -q ${dir_install_non_free_softwares}/Codecs/w64codecs*.deb
    else
        sudo gdebi -n -q ${dir_install_non_free_softwares}/Codecs/w32codecs*.deb
    fi

    sudo gdebi -n -q ${dir_install_non_free_softwares}/libdvdcss2*.deb

fi

#Activation de Gmenu de Cairo-dock
/usr/bin/cairo_dock_gmenu_on.sh

echo "100" ;

) |
zenity --progress --pulsate \
  --title="$install_title" \
  --text="$install_text" \
  --width=500 \
  --percentage=0 --auto-close --no-cancel
    if [ $? = "1" ]
    then
      sudo dpkg --configure -a

      #Activation de Gmenu de Cairo-dock
      /usr/bin/cairo_dock_gmenu_on.sh

      zenity --error --timeout=$delai_fenetre \
        --text="$install_cancel"
    else
      sudo dpkg --configure -a
      echo "# Install Completed"
    fi

sudo chmod o+r /var/lib/command-not-found/commands.db*

exit 0




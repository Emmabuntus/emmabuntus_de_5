#!/bin/bash

# libreoffice_ecoles.sh --
#
#   This file permits to launch LibreOffice for Schools for the Emmabuntus Distrib.
#
#   Created on 2010-22 by collectif Emmabuntüs (contact@emmabuntus.org).
#
#   It was validate on Debian 12 XFCE/LXQt.
#
#   Home web site : https://emmabuntus.org/
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


################################################################################

if [[ ${USER} == root ]] ; then
dir_user=/root
else
dir_user=/home/${USER}
fi


dir_libo="${dir_user=}/.config/libreoffice_ecoles"
file=user_LO_des_ecoles_portable_mod.zip
dir_libo_dialogbox="user/basic/Standard"
dir_libo_toolbar="user/config/soffice.cfg/modules/swriter/toolbar"

# Vérification si la configuration LiBo pour les écoles est présente pour le root

if ! test -d ${dir_libo}
then
    dir=$(pwd)
    mkdir ${dir_libo}
    mkdir ${dir_libo}/user
    cd ${dir_libo}/user
    cp /opt/LibreOffice_ecoles/${file} .
    unzip ${file}
    rm ${file}
    cd ${dir}
fi

if test -L ${dir_libo}/${dir_libo_dialogbox}
then
    unlink ${dir_libo}/${dir_libo_dialogbox}
fi

if test -L ${dir_libo}/${dir_libo_toolbar}
then
    unlink ${dir_libo}/${dir_libo_toolbar}
fi


if [[ $LANG == fr* ]]
then
    echo "Lancement Libre Office des écoles"
    ln -s ${dir_libo}/${dir_libo_dialogbox}_fr ${dir_libo}/${dir_libo_dialogbox}
    ln -s ${dir_libo}/${dir_libo_toolbar}_fr ${dir_libo}/${dir_libo_toolbar}
else
    echo "Start Libre Office for school"
    ln -s ${dir_libo}/${dir_libo_dialogbox}_en ${dir_libo}/${dir_libo_dialogbox}
    ln -s ${dir_libo}/${dir_libo_toolbar}_en ${dir_libo}/${dir_libo_toolbar}
fi


libreoffice --writer -env:UserInstallation=file://${dir_libo}

#!/bin/bash

###Pour exporter la librairie de gettext.
set -a
source gettext.sh
set +a
export TEXTDOMAIN=emmabuntus
export TEXTDOMAINDIR="/usr/share/locale"
. gettext.sh
emmabuntus=$0


nom_logiciel_affichage="$(eval_gettext 'Computer price')"
description="$(eval_gettext 'Tower + Keyboard/Mouse')"
prix_vente=80
remarque=""
cpu=""
ram=""
dd=""

currency_symbol=$(locale -ck currency_symbol | grep currency_symbol | cut -d \" -f2)

fiche_vente="/tmp/.fiche_vente.txt"
utilisation_fiche=0
dir_install=/opt/price_emmaus

message_validation="$(eval_gettext 'Please enter the configuration of you computer for sale')"
message_fiche="$(eval_gettext 'This sale-card was already entered, do you want to use it ?')"

message_erreur_saisie="
<span color=\"red\">$(eval_gettext 'Wrong description or price !')</span>

$(eval_gettext 'Please re-enter the sale-card data')"

message_info_password="$(eval_gettext 'Please enter the admin password :')"


if test -f $fiche_vente
then

    source ${fiche_vente}

    if  [[ ${cpu} != "" && ${ram} != "" && ${dd} != "" ]]
    then
        message_fiche_complet="
        ${message_fiche}

        <span color=\"blue\">$(eval_gettext 'Description')</span> : ${description}
        <span color=\"blue\">$(eval_gettext 'Sale price')</span> : ${prix_vente} ${currency_symbol}

        <span color=\"blue\">$(eval_gettext 'CPU')</span> : ${cpu}
        <span color=\"blue\">$(eval_gettext 'RAM')</span> : ${ram}
        <span color=\"blue\">$(eval_gettext 'Hard disk drive')</span> : ${dd}

        <span color=\"red\">$(eval_gettext 'Comments')</span> : ${remarque}
        "
    else
        message_fiche_complet="
        ${message_fiche}

        <span color=\"blue\">$(eval_gettext 'Description')</span> : ${description}
        <span color=\"blue\">$(eval_gettext 'Sale price')</span> : ${prix_vente} ${currency_symbol}

        <span color=\"red\">$(eval_gettext 'Comments')</span> : ${remarque}
        "
    fi



    zenity --question --no-wrap --width=700 --text="$message_fiche_complet"


    if [ $? = "0" ]
    then
        utilisation_fiche=1
    else
        utilisation_fiche=0
    fi

    echo "utilisation_fiche = $utilisation_fiche"

fi


if ! test -f ${fiche_vente} || [[ ${utilisation_fiche} == 0 ]]
then


   description=$(zenity --list \
                  --width=750 --height=260 \
                  --text="$message_validation" \
                  --title="$titre" \
                  --separator=":" \
                  --radiolist  --column "$(eval_gettext 'Selection')" --column "$(eval_gettext 'Type of hardware')" \
                  TRUE "$(eval_gettext 'Tower + Screen + Keyboard/Mouse')" \
                  FALSE "$(eval_gettext 'Tower + Keyboard/Mouse')" \
                  FALSE "$(eval_gettext 'Tower')"  \
                  FALSE "$(eval_gettext 'Laptop with Power-Adapter')" \
                  FALSE "$(eval_gettext 'Laptop without Power-Adapter')" \
                  FALSE "$(eval_gettext 'Free comments field')" )

    if ! [ $? = "0" ]
    then
        zenity --error --text="# $nom_logiciel_affichage software installation canceled"

        exit 1
    fi

    if [[ ${description} == "$(eval_gettext 'Free comments field')"  ]]
    then
        description=$(zenity --entry --title="$(eval_gettext 'Computer description')" --text="$(eval_gettext 'Please fill the hardware description in :')" --entry-text "$(eval_gettext 'Data entry area')")
    fi

    prix_vente=$(zenity --entry --title="$(eval_gettext 'Sale price')" --text="$(eval_gettext 'Enter the computer sale price in') ${currency_symbol} :" --entry-text "$prix_vente")

    if ! [ $? = "0" ]
    then
        zenity --error --text="# $nom_logiciel_affichage $(eval_gettext 'software installation canceled')"

        exit 2
    fi

    remarque=$(zenity --entry --title="$(eval_gettext 'Note')" --text="$(eval_gettext 'Please enter a note if the computer has a defect, or leave this field empty and click on \047Validate\047 :')" --entry-text "$remarque")

    if ! [ $? = "0" ]
    then
        zenity --error --text="# $nom_logiciel_affichage $(eval_gettext 'software installation canceled')"

        exit 3
    fi

    if [[ ${prix_vente} == "" || ${description} == ""  ]]
    then
        zenity --error --text="$(eval_gettext 'Error : Wrong description or price !')"

        exit 4
    fi
fi


if [[ ${prix_vente} == "" || ${description} == ""  ]]
then
    zenity --error --title="$(eval_gettext 'Error : Wrong description or price !')" --text="$message_erreur_saisie"
elif  [[ ${utilisation_fiche} == 1 ]]
then
    $dir_install/prix_emmaus.sh ${prix_vente} "${description}" ${fiche_vente} "${remarque}" ${utilisation_fiche}
else
    pkexec $dir_install/prix_emmaus.sh ${prix_vente} "${description}" ${fiche_vente} "${remarque}" "0"
fi



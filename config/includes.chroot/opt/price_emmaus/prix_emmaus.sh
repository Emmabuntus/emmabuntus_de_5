#!/bin/bash

###Pour exporter la librairie de gettext.
set -a
source gettext.sh
set +a
export TEXTDOMAIN=emmabuntus
export TEXTDOMAINDIR="/usr/share/locale"
. gettext.sh
emmabuntus=$0

#var
prix_vente=$1
description=$2
fiche=$3
remarque=$4
affichage=$5

if [[ $LANG == fr* ]]  ; then
    img="prix.jpg"
    img_portable="prix_laptop.jpg"
else
    img="prix_en.jpg"
    img_portable="prix_laptop_en.jpg"
fi

img_display="display.jpg"
info_texte="prix_emmaus.txt"
info_tour="$(eval_gettext 'Tower')"

currency_symbol=$(locale -ck currency_symbol | grep currency_symbol | cut -d \" -f2)

dir_install=/opt/price_emmaus

###Pour exporter la librairie de gettext.
set -a
source gettext.sh
set +a
export TEXTDOMAIN=emmabuntus
export TEXTDOMAINDIR="/usr/share/locale"
. gettext.sh
emmabuntus=$0


if [[ $affichage == 1 ]]
then

    if test -f ${fiche}
    then
    
        source ${fiche}
        
    else
    
        exit 1
    
    fi

    display -quiet  -geometry ${X_WIDTH}x${X_HEIGHT} /tmp/${img_display} &

    exit 0

fi



if [[ ${description} == *${info_tour}* ]]
then
    cp "$dir_install/assets/"$img /tmp/${img_display}
else
    cp "$dir_install/assets/"$img_portable /tmp/${img_display}
fi

#MEM
mem=`free -m | awk {'print $2'} | head -n 2| tail -1 | cut -d'M' -f1 | cut -d'G' -f1`
#echo $mem
if [ "$mem" -lt "600" ]
then
    memory="512 $(eval_gettext 'MB')"
elif [ "$mem" -lt "1000" ]
then
    memory="1 $(eval_gettext 'GB')"
elif [ "$mem" -lt "2000" ]
then
    memory="2 $(eval_gettext 'GB')"
elif [ "$mem" -lt "2000" ]
then
    memory="2 $(eval_gettext 'GB')"
elif [ "$mem" -lt "3000" ]
then
    memory="3 $(eval_gettext 'GB')"
elif [ "$mem" -lt "4000" ]
then
    memory="4 $(eval_gettext 'GB')"
elif [ "$mem" -lt "5000" ]
then
    memory="5 $(eval_gettext 'GB')"
elif [ "$mem" -lt "6000" ]
then
    memory="6 $(eval_gettext 'GB')"
elif [ "$mem" -lt "7000" ]
then
    memory="7 $(eval_gettext 'GB')"
elif [ "$mem" -lt "8000" ]
then
    memory="8 $(eval_gettext 'GB')"
elif [ "$mem" -gt "8000" ]
then
    memory="+8 $(eval_gettext 'GB')"
fi


#Type Memoire
type_memory=`dmidecode  | grep DDR | cut -d":" -f2 | sed 's/ //g' | head -1`

#PROC
proc=`cat /proc/cpuinfo | grep -i "model name" | cut -d":" -f2 | head -1`
model_cpu=`echo $proc | head -1`

proc_freq=`cat /proc/cpuinfo | grep -i "cpu Mhz" | cut -d":" -f2 | head -1 | cut -d'.' -f1`
nb_cores=`cat /proc/cpuinfo  | grep -i "cpu cores" | head -1 | cut -d":" -f2 | sed 's/ //'`

#ESPACE DISQUE
#disk=`df -h`
disk=`fdisk -l | grep "Disk" | cut -d" " -f3 | cut -d"." -f1 | head -1`



# Test retour erreur de détection

if [[ ${model_cpu} == "" ]]
then

    if [[ ${cpu} == "" ]]
    then
        model_cpu=$(zenity --entry --title="$(eval_gettext 'CPU Model')" --text="$(eval_gettext 'Wrong CPU model detected, please enter a new value :')" --entry-text "")
    else
        model_cpu=${cpu}
    fi

fi

if [[ ${memory} == "" ]]
then

    if [[ ${ram} == "" ]]
    then
        memory=$(zenity --entry --title="$(eval_gettext 'RAM capacity')" --text="$(eval_gettext 'Wrong RAM capacity detected, please enter a new value in GB :')" --entry-text "")
        type_memory="$(eval_gettext 'GB')"
    else
        memory=`echo ${ram} | cut -d" " -f1`
        type_memory=`echo ${ram} | cut -d" " -f2`
    fi

fi

if [[ ${disk} == "" ]]
then

    if [[ ${dd} == "" ]]
    then
        disk=$(zenity --entry --title="$(eval_gettext 'HDD capacity')" --text="$(eval_gettext 'Wrong HDD capacity detected, please enter a new value in GB :')" --entry-text "")
    else
        disk=`echo ${dd} | cut -d" " -f1`
    fi

fi

#Stockage des valeurs
echo "# $(eval_gettext 'Machine description and sale price card')" > ${fiche}
echo "description=\"${description}\"" >> ${fiche}
echo "prix_vente=${prix_vente}" >> ${fiche}
echo "remarque=\"${remarque}\"" >> ${fiche}
echo "cpu=\"$model_cpu\"" >> ${fiche}
echo "ram=\"$memory $type_memory\"" >> ${fiche}
echo "dd=\"$disk $(eval_gettext 'GB')\"" >> ${fiche}


#Affichage
echo "# $(eval_gettext 'Machine description and sale price card')" > /tmp/${info_texte}
echo "$(eval_gettext 'Description') : ${description}" >> /tmp/${info_texte}
echo "$(eval_gettext 'CPU') :  $model_cpu" > /tmp/${info_texte}
echo "$(eval_gettext 'RAM') : $memory $type_memory" >> /tmp/${info_texte}
echo "$(eval_gettext 'Hard disk drive') : $disk $(eval_gettext 'GB')" >> /tmp/${info_texte}
echo "$(eval_gettext 'Sale price') : ${prix_vente} ${currency_symbol}" >> /tmp/${info_texte}
echo "" >> /tmp/${info_texte}
echo "$(eval_gettext 'Comments') : ${remarque}" >> /tmp/${info_texte}
echo "" >> /tmp/${info_texte}

cat /tmp/${info_texte}

x_position=30
y_position=60
x_delta=30
pointsize=26
pointsize_large=36

mogrify -fill black -pointsize ${pointsize_large} -annotate +${x_position}+${y_position} "$description : $prix_vente ${currency_symbol}" /tmp/${img_display}

let "y_position=$y_position + x_delta"
mogrify -fill black -pointsize ${pointsize} -annotate +${x_position}+${y_position} " " /tmp/${img_display}

let "y_position=$y_position + x_delta"
mogrify -fill black -pointsize ${pointsize} -annotate +${x_position}+${y_position} "$(eval_gettext 'CPU') : $model_cpu" /tmp/${img_display}

let "y_position=$y_position + x_delta"
mogrify -fill black -pointsize ${pointsize} -annotate +${x_position}+${y_position} "$(eval_gettext 'RAM') : $memory" /tmp/${img_display}

let "y_position=$y_position + x_delta"
mogrify -fill black -pointsize ${pointsize} -annotate +${x_position}+${y_position} "$(eval_gettext 'Hard disk drive') : $disk $(eval_gettext 'GB')" /tmp/${img_display}

if [[ ${remarque} != "" ]]
then
    let "y_position=$y_position + (2 * x_delta)"
    mogrify -fill red -pointsize ${pointsize} -annotate +${x_position}+229 "$(eval_gettext 'Comments') : $remarque" /tmp/${img_display}
fi


#First get the display width and height as shell variables.
eval `xrdb -symbols -screen |  sed -n '/^-D\(WIDTH\|HEIGHT\)/{s/-D/X_/gp;}'`

let "X_WIDTH = ${X_WIDTH}*8/10"
let "X_HEIGHT = ${X_HEIGHT}*8/10"
mogrify -resize ${X_WIDTH}x${X_HEIGHT} /tmp/${img_display}



display -quiet  -geometry ${X_WIDTH}x${X_HEIGHT} /tmp/${img_display} &

chown root:root ${fiche}
chmod a+r ${fiche}
chmod a+r /tmp/${info_texte}
chmod a+r /tmp/${img_display}
